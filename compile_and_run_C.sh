#!/bin/bash
device="atmega64";
device_avrdude="m64";
fn="rgb_led_cube_5x5x5"; # fn - filename
dir_lib="led_cube_lib"
dir_anim="animations"
optim="Os"
dg="Debug"
sum=6;
cnt=1;



# Create Debug directory - just for case
mkdir $dg 2>/dev/null;

# Compile files in directory dir_lib
avr-gcc -Wall -$optim -std=c99 -mmcu=$device  -c *c \
-I ./$dir_lib/ -I ./$dir_anim/ -I ./ &&
avr-gcc -Wall -$optim -std=c99 -mmcu=$device  -c ./$dir_lib/*.c \
-I ./$dir_lib/ -I ./$dir_anim/ -I ./ &&
avr-gcc -Wall -$optim -std=c99 -mmcu=$device  -c ./$dir_anim/*c \
-I ./$dir_lib/ -I ./ &&
#avr-gcc  -Wall -Os -std=c99 -mmcu=$device  -c *c &&

mv *.o $dg/ &&
 echo "<----------------------------------------->" &&
 echo " All Compiled            $cnt/$sum" && let cnt=$((cnt +1)) &&

avr-gcc -Wall -mmcu=$device -o $fn.elf $dg/*.o
mv $fn.elf $dg/ &&
 echo " ELF file generated      $cnt/$sum" && let cnt=$((cnt + 1)) &&	# Calc one step
 								# same as cnt++
# Now make documentation
cd ../../DOC/develop &&
doxygen Doc_EN_RGB_LED_cube_5x5x5.doxyfile >/dev/null 2>/dev/null &&
doxygen Doc_CZ_RGB_LED_cube_5x5x5.doxyfile >/dev/null 2>/dev/null &&
cd ../../SW/AVR/ &&
 echo " Doc. generated          $cnt/$sum" && let cnt=$((cnt + 1)) &&
 								
 								
avr-objdump -S $dg/$fn.elf > $dg/$fn.disasm &&	# Disassembly ELF (just 4 fun)
 echo " Disassembly ELF done    $cnt/$sum" && let cnt++ &&

avr-objcopy -O ihex $dg/$fn.elf $dg/$fn.hex &&	# Make HEX file
cp $dg/$fn.hex ./ &&	# Copy HEX
 echo " HEX file generated      $cnt/$sum" && let cnt++ &&

avrdude -c usbasp -p $device_avrdude -U flash:w:$fn.hex && # upload HEX file
 echo " Upload successful       $cnt/$sum" && let cnt++ # && let cnt++;
#sleep 1s;
#avrdude -c usbasp -p m32 2>/dev/null;	# Restart target chip one more time (because of LCD)

# test for operations result
 if [ $sum -ge $cnt ];	# ge, gt, le, lt, ne, eq
 then
  echo "/-----------------\\";
  echo "| Process failed! |";
  echo "\\-----------------/";
 else
  echo "/------------\\";
  echo "| Process OK |";
  echo "\\------------/";
 fi
