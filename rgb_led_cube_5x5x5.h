/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Complex settings AVR for project
 *
 * In this file is defined speed of CPU, names for pins and PORTs and so on.\n
 * Names for keyboard pins are defined in keyboard_6x5.h file, NOT THERE!
 *
 * Created:  2013/06/08\n
 * Modified: 2015/06/21
*/


// Trick for multiply include this file
#ifndef _settings_library_
#define _settings_library_

//================================| Includes |=================================
#include <avr/io.h>             // Definitions I/O
#include <inttypes.h>           // Data types
#include <avr/interrupt.h>      // Interrupts
#include <avr/pgmspace.h>       // For operations with flash memory

#include "rgb_led_cube_5x5x5_settings.h"        // Global settings
#include <util/delay.h>         // Delays library

#include "bit_operations.h"     // Basic operations with bits and I/O
#include "pwm_rgb.h"            // Driver for PWM
// Some commands can use font 5x5 (simpler usage)
#include "font_5x5.h"
#include "sys_timer.h"          // System clock

//===============================| Strucuters |================================
/**
 * \brief Status and configuration structure for animation process
 *
 */
typedef struct{
  /** Animation is running (1 - running ; 0 - not running) */
  uint8_t anim_run:             1;

  /** Flag is set to 1, when is read command "end of animation". Because\n
   *  this command has as a value "animation repeat", needs to signalizes\n
   *  that value was already read and do not overwrite repeat counter.\n
   */
  uint8_t read_end_of_anim:     1;

  /** If activated, the all command are ignored and just dummy read */
  uint8_t anim_random:          1;

  /** After last animation play first - when is last animation played, then\n
   *  there are two options: stop playing or continue from first animation\n
   *  again.
   */
  uint8_t anim_play_in_loop:    1;

  /** phase of animation waiting process (0~3 - if 3 -> change image) */
  uint8_t anim_cnt:             2;

  /** Flag which indicate if anime scheduler should play animation or not */
  uint8_t anim_scheduler_stop:      1;

  /** Data are stored in FLASH (if 1, else data are stored in SRAM - 0) */
  uint8_t anim_read_FLASH:      1;
}stat_cfg_anime_t;


//===========================| Additional includes |===========================
/* Well, some includes have to be here, because simply use also this library
 * and they depend on some structures, macros and so on
 */


#include "framebuffer.h"        // Framebuffer functions

#include "animation_codes.h"    // Command codes in animations

#include "keyboard_6x5.h"       // Keyboard driver


//============================| Global variables |=============================
extern volatile const uint16_t *p_anime;

extern volatile uint16_t i_fb_data[5][5];

extern volatile stat_cfg_anime_t i_stat_cfg_anime;

extern volatile uint8_t  i_anime_scheduler_animation_id;

extern volatile uint8_t  i_anime_repeat_counter;

//===========================| Function prototypes |===========================
void set_io_to_defaults(void);


void init_animation(void);

void animation_process_word(uint16_t i_tmp,
                            uint8_t *p_framebuffer_cnt,
                            volatile uint8_t *p_do_not_wait);

void animation_scheduler(void);

void check_for_read_end_of_anim(volatile uint8_t *p_anime_repeat_counter);

void move_random_pointer(void);

#endif
