/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Standard font for RGB LED cube 5x5x5
 *
 * Because it is not so easy to draw text pixel by pixel, so this library was\n
 * created. It contains binary representation characters. Characters are 5x5\n
 * pixel (dots).\n
 * Used font at: http://www.dafont.com/5x5-dots.font
 *
 * \code
 * Data format (binary data for framebuffer)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 0 | R | R | R | R | R | G | G | G | G | G | B | B | B | B | B |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 * Instruction format (if MSB is 1, then it is possible change some settings)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 1 | C | C | C | C | C | C | C | V | V | V | V | V | V | V | V |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *  C - command (0~127)
 *  V - value   (0~255)
 *
 * Physical orientation in column:
 *
 *       C0         C1     ...
 *     +-----+   +-----+   ...
 * MSB |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 * LSB |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 * \endcode
 *
 * Created  2013/06/18\n
 * Modified 2013/06/18
 */

#include "font_5x5.h"

const uint16_t font_5x5_A[] PROGMEM = { 0x3DEF,0x5294,0x5294,0x5294,0x3DEF };
const uint16_t font_5x5_B[] PROGMEM = { 0x7FFF,0x56B5,0x56B5,0x56B5,0x294A };
const uint16_t font_5x5_C[] PROGMEM = { 0x39CE,0x4631,0x4631,0x4631,0x4631 };
const uint16_t font_5x5_D[] PROGMEM = { 0x7FFF,0x4631,0x4631,0x4631,0x39CE };
const uint16_t font_5x5_E[] PROGMEM = { 0x7FFF,0x56B5,0x56B5,0x56B5,0x4631 };
const uint16_t font_5x5_F[] PROGMEM = { 0x7FFF,0x5294,0x5294,0x4210,0x4210 };
const uint16_t font_5x5_G[] PROGMEM = { 0x39CE,0x4631,0x56B5,0x56B5,0x5EF7 };
const uint16_t font_5x5_H[] PROGMEM = { 0x7FFF,0x1084,0x1084,0x1084,0x7FFF };
const uint16_t font_5x5_I[] PROGMEM = { 0x0000,0x4631,0x7FFF,0x4631,0x0000 };
const uint16_t font_5x5_J[] PROGMEM = { 0x0842,0x0421,0x4631,0x4631,0x7BDE };
const uint16_t font_5x5_K[] PROGMEM = { 0x7FFF,0x1084,0x1084,0x294A,0x4631 };
const uint16_t font_5x5_L[] PROGMEM = { 0x7FFF,0x0421,0x0421,0x0421,0x0421 };
const uint16_t font_5x5_M[] PROGMEM = { 0x7FFF,0x2108,0x1084,0x2108,0x7FFF };
const uint16_t font_5x5_N[] PROGMEM = { 0x7FFF,0x2108,0x1084,0x0842,0x7FFF };
const uint16_t font_5x5_O[] PROGMEM = { 0x39CE,0x4631,0x4631,0x4631,0x39CE };
const uint16_t font_5x5_P[] PROGMEM = { 0x7FFF,0x5294,0x5294,0x5294,0x2108 };
const uint16_t font_5x5_Q[] PROGMEM = { 0x39CE,0x4631,0x4631,0x3DEF,0x0421 };
const uint16_t font_5x5_R[] PROGMEM = { 0x7FFF,0x5294,0x5294,0x5AD6,0x2529 };
const uint16_t font_5x5_S[] PROGMEM = { 0x2529,0x56B5,0x56B5,0x56B5,0x4A52 };
const uint16_t font_5x5_T[] PROGMEM = { 0x4210,0x4210,0x7FFF,0x4210,0x4210 };
const uint16_t font_5x5_U[] PROGMEM = { 0x7BDE,0x0421,0x0421,0x0421,0x7BDE };
const uint16_t font_5x5_V[] PROGMEM = { 0x6318,0x18C6,0x0421,0x18C6,0x6318 };
const uint16_t font_5x5_W[] PROGMEM = { 0x7FFF,0x0842,0x1084,0x0842,0x7FFF };
const uint16_t font_5x5_X[] PROGMEM = { 0x4631,0x294A,0x1084,0x294A,0x4631 };
const uint16_t font_5x5_Y[] PROGMEM = { 0x6318,0x1084,0x1CE7,0x1084,0x6318 };
const uint16_t font_5x5_Z[] PROGMEM = { 0x4631,0x4E73,0x56B5,0x6739,0x4631 };

const uint16_t font_5x5_a[] PROGMEM = { 0x4A52,0x56B5,0x56B5,0x56B5,0x3DEF };
const uint16_t font_5x5_b[] PROGMEM = { 0x7FFF,0x2529,0x2529,0x2529,0x18C6 };
const uint16_t font_5x5_c[] PROGMEM = { 0x39CE,0x4631,0x4631,0x4631,0x294A };
const uint16_t font_5x5_d[] PROGMEM = { 0x18C6,0x2529,0x2529,0x2529,0x7FFF };
const uint16_t font_5x5_e[] PROGMEM = { 0x39CE,0x56B5,0x56B5,0x56B5,0x35AD };
const uint16_t font_5x5_f[] PROGMEM = { 0x3DEF,0x5294,0x5294,0x4210,0x4210 };
const uint16_t font_5x5_g[] PROGMEM = { 0x2529,0x56B5,0x56B5,0x56B5,0x39CE };
const uint16_t font_5x5_h[] PROGMEM = { 0x7FFF,0x2108,0x2108,0x2108,0x1CE7 };
const uint16_t font_5x5_i[] PROGMEM = { 0x0000,0x0000,0x5EF7,0x0000,0x0000 };
const uint16_t font_5x5_j[] PROGMEM = { 0x0421,0x0421,0x0421,0x0421,0x7BDE };
const uint16_t font_5x5_k[] PROGMEM = { 0x7FFF,0x1084,0x1084,0x294A,0x4631 };
const uint16_t font_5x5_l[] PROGMEM = { 0x7BDE,0x0421,0x0421,0x0421,0x0421 };
const uint16_t font_5x5_m[] PROGMEM = { 0x3DEF,0x4210,0x3DEF,0x4210,0x3DEF };
const uint16_t font_5x5_n[] PROGMEM = { 0x7FFF,0x4210,0x4210,0x4210,0x3DEF };
const uint16_t font_5x5_o[] PROGMEM = { 0x39CE,0x4631,0x4631,0x4631,0x39CE };
const uint16_t font_5x5_p[] PROGMEM = { 0x7FFF,0x4A52,0x4A52,0x4A52,0x318C };
const uint16_t font_5x5_q[] PROGMEM = { 0x318C,0x4A52,0x4A52,0x4A52,0x7FFF };
const uint16_t font_5x5_r[] PROGMEM = { 0x7FFF,0x2108,0x4210,0x4210,0x2108 };
const uint16_t font_5x5_s[] PROGMEM = { 0x2529,0x56B5,0x56B5,0x56B5,0x4A52 };
const uint16_t font_5x5_t[] PROGMEM = { 0x7BDE,0x2529,0x2529,0x0421,0x0842 };
const uint16_t font_5x5_u[] PROGMEM = { 0x7BDE,0x0421,0x0421,0x0842,0x7FFF };
const uint16_t font_5x5_v[] PROGMEM = { 0x6318,0x18C6,0x0421,0x18C6,0x6318 };
const uint16_t font_5x5_w[] PROGMEM = { 0x7BDE,0x0421,0x7BDE,0x0421,0x7BDE };
const uint16_t font_5x5_x[] PROGMEM = { 0x6F7B,0x1084,0x1084,0x1084,0x6F7B };
const uint16_t font_5x5_y[] PROGMEM = { 0x6739,0x14A5,0x14A5,0x14A5,0x7BDE };
const uint16_t font_5x5_z[] PROGMEM = { 0x4631,0x4E73,0x56B5,0x6739,0x4631 };

const uint16_t font_5x5_0[] PROGMEM = { 0x39CE,0x4E73,0x56B5,0x6739,0x39CE };
const uint16_t font_5x5_1[] PROGMEM = { 0x0000,0x2529,0x7FFF,0x0421,0x0000 };
const uint16_t font_5x5_2[] PROGMEM = { 0x4E73,0x56B5,0x56B5,0x56B5,0x2529 };
const uint16_t font_5x5_3[] PROGMEM = { 0x4631,0x4631,0x56B5,0x56B5,0x294A };
const uint16_t font_5x5_4[] PROGMEM = { 0x6318,0x1084,0x1084,0x1084,0x7FFF };
const uint16_t font_5x5_5[] PROGMEM = { 0x77BD,0x56B5,0x56B5,0x56B5,0x4A52 };
const uint16_t font_5x5_6[] PROGMEM = { 0x39CE,0x56B5,0x56B5,0x56B5,0x4A52 };
const uint16_t font_5x5_7[] PROGMEM = { 0x4210,0x4210,0x4E73,0x5294,0x6318 };
const uint16_t font_5x5_8[] PROGMEM = { 0x294A,0x56B5,0x56B5,0x56B5,0x294A };
const uint16_t font_5x5_9[] PROGMEM = { 0x2108,0x5294,0x56B5,0x56B5,0x39CE };

const uint16_t font_5x5_quest[] PROGMEM =
    { 0x0000,0x4210,0x56B5,0x5294,0x2108 };
