/**
 * \file
 *
 * \brief Driver for matrix keyboard 6x5
 *
 * Created:  2013/06/22\n
 * Modified: 2016/08/15
 *
 * \author Martin Stejskal
 *
 */


#ifndef _keyboard_library_
#define _keyboard_library_

//================================| Includes |=================================
#include <avr/io.h>             // Definitions AVR I/O's
#include "bit_operations.h"     // Basic bit operations
#include "sys_timer.h"          // For requests to system time
#include "pwm_rgb.h"            // Changing PWM values


// Local setting (mainly symbolic pin names) and F_CPU (is needed only F_CPU)
#include "../rgb_led_cube_5x5x5.h"
#include <util/delay.h>         // Delays - for reading from keyboard


#include "communication.h"
//============================| global_variables |=============================
/**
 * \brief Framebuffer
 *
 * Direct acces to framebuffer (sometimes is needed and it is easier than\n
 * writing dummy function).
 */
extern volatile uint16_t i_fb_data[5][5];

/**
 * \brief Status and configuration register for process witch plays animations
 */
extern volatile stat_cfg_anime_t i_stat_cfg_anime;

/**
 * \brief Id of actual animation in anime scheduler
 */
extern volatile uint8_t  i_anime_scheduler_animation_id;

/**
 * \brief Counts how many times was animation repeated
 */
extern volatile uint8_t  i_anime_repeat_counter;



//=================================| Macros |==================================
/**
 *
 * \ingroup keyboard Symbolic names for pins
 *
 * \{
 *
 * \code
 * Keyboard layout
 *
 *      |RD0 |RD1 |RD2 |RD3 | RD4|
 * -----+----+----+----+----+----+
 * PWR4 | 04 | 09 | 14 | 24 | 29 |
 * -----+----+----+----+----+----+
 * PWR3 | 03 | 08 | 13 | 23 | 28 |
 * -----+----+----+----+----+----+
 * PWR2 | 02 | 07 | 12 | 22 | 27 |
 * -----+----+----+----+----+----+
 * PWR1 | 01 | 06 | 11 | 21 | 26 |
 * -----+----+----+----+----+----+
 * PWR0 | 00 | 05 | 10 | 20 | 25 |
 *
 * \endcode
 */
#define KBRD_RD0_PORT           F
#define KBRD_RD0_pin            5

#define KBRD_RD1_PORT           F
#define KBRD_RD1_pin            4

#define KBRD_RD2_PORT           F
#define KBRD_RD2_pin            3

#define KBRD_RD3_PORT           F
#define KBRD_RD3_pin            2

#define KBRD_RD4_PORT           F
#define KBRD_RD4_pin            1

#define KBRD_RD5_PORT           F
#define KBRD_RD5_pin            0

#define KBRD_PWR0_PORT          A
#define KBRD_PWR0_pin           2

#define KBRD_PWR1_PORT          A
#define KBRD_PWR1_pin           1

#define KBRD_PWR2_PORT          A
#define KBRD_PWR2_pin           0

#define KBRD_PWR3_PORT          F
#define KBRD_PWR3_pin           7

#define KBRD_PWR4_PORT          F
#define KBRD_PWR4_pin           6
/**
 * \}
 */

/**
 * \brief Define time, when process should scan keyboard
 *
 * Keyboard is not time critical part of system, however is good idea run\n
 * keyboard scans time to time. This number define how many cycles must\n
 * system counter do before function \b keyboard_process will scan keyboard.\n
 * Number 6250 is approximately 300 ms for F_CPU 16 MHz @ sys_timer divider\n
 * 256
 */
#define kbrd_time_to_scan_kbrd          12750UL

/**
 * \brief Define step of changing PWM value in draw mode.
 *
 * This value is not checked, so please use values from 1 to 50. 10 recommended
 */
#define kbrd_draw_pwm_step              10

/**
 * \brief Very short delay
 *
 * Sometimes is need a little, but some, delay to read valid data on input
 */
#define kbrd_micro_delay        __asm("NOP")
//#define kbrd_micro_delay        _delay_us(10)

/**
 * \brief Short delay
 *
 * In some cases is need a little longer delay than kbrd_micro_delay
 */
#define kbrd_short_delay        _delay_us(2)
//#define kbrd_short_delay        _delay_us(22)


/**
 * \brief Main menu options
 *
 * This describes options at main menu (highest level).
 */
typedef enum{
  main_menu     =0,//!< main_menu Currently in main menu
  play_anim     =1,//!< play_anim Sub menu "Play animations"
  draw          =2,//!< draw Sub menu "Draw in cube"

  // slave mode -> controlled by non-keyboard events
  slave         =3,//!< slave Sub menu of "Slave" mode (cube is controlled by PC)

  mm_last_option   //!< mm_last_option Last option in enum. Number is automatic
}kbrd_main_menu_t;

/**
 * \brief Keyboard structure
 *
 * Because handling with keyboard should be easy and it is not so much\n
 * critical, we use structure and bitfield to operate with keyboard pins\b
 * more comfortable.
 *
 * \code
 * Keyboard layout
 *
 *      |RD0 |RD1 |RD2 |RD3 | RD4|
 * -----+----+----+----+----+----+
 * PWR4 | 04 | 09 | 14 | 24 | 29 |
 * -----+----+----+----+----+----+
 * PWR3 | 03 | 08 | 13 | 23 | 28 |
 * -----+----+----+----+----+----+
 * PWR2 | 02 | 07 | 12 | 22 | 27 |
 * -----+----+----+----+----+----+
 * PWR1 | 01 | 06 | 11 | 21 | 26 |
 * -----+----+----+----+----+----+
 * PWR0 | 00 | 05 | 10 | 20 | 25 |
 *
 * \endcode
 */
typedef struct{
  union{
    /* All buttons in one variable. Little endian expected, so BTN0 should be
     * at LSB
     */
    uint32_t i_all_btns;
    struct{
      uint32_t  btn0:     1;      // LSB on Little endian
      uint32_t  btn1:     1;
      uint32_t  btn2:     1;
      uint32_t  btn3:     1;
      uint32_t  btn4:     1;
      uint32_t  btn5:     1;
      uint32_t  btn6:     1;
      uint32_t  btn7:     1;
      uint32_t  btn8:     1;
      uint32_t  btn9:     1;
      uint32_t  btn10:    1;
      uint32_t  btn11:    1;
      uint32_t  btn12:    1;
      uint32_t  btn13:    1;
      uint32_t  btn14:    1;
      uint32_t  btn15:    1;
      uint32_t  btn16:    1;
      uint32_t  btn17:    1;
      uint32_t  btn18:    1;
      uint32_t  btn19:    1;
      uint32_t  btn20:    1;
      uint32_t  btn21:    1;
      uint32_t  btn22:    1;
      uint32_t  btn23:    1;
      uint32_t  btn24:    1;
      uint32_t  btn25:    1;
      uint32_t  btn26:    1;
      uint32_t  btn27:    1;
      uint32_t  btn28:    1;
      uint32_t  btn29:    1;
    }s;
  };
}kbrd_btns_t;



/**
 * \brief Test selected pin if is pressed or not
 *
 * Just read data on pin and add them to i_keyboard_result (If button\n
 * pressed). PWRx must be already set as output and RDx must be as input,\n
 * else this algorithm will not work!\n
 * \note Because "1" is in default integer and we need to operate it in 32bit\n
 * variable, we must retype this number to 32bit long.
 */
#define kbrd_test_for(RDx_column, PWRx_line)   \
  if( (io_read_fast( KBRD_RD##RDx_column##_PORT, KBRD_RD##RDx_column##_pin ))\
       == 0 )\
  {\
    i_keyboard_buttons.i_all_btns |=\
                         (( (uint32_t)1 )<<((RDx_column*5) + PWRx_line));\
  }\


/**
 * \brief Crazy macro for reading whole line on keyboard and write data
 *
 * First set PWRx pin as output and set it to "0"\n
 * Small time delay (chance to discharge parasite capacitors)\n
 * Test for pin RD0, RD1 and so on\n
 * Before set pin as input we need to charge parasite capacitors -> set 1\n
 * And at the end set PWRx again as input with pull-up
 */
#define kbrd_scan_line(X)       \
  io_set_0(       KBRD_PWR##X##_PORT, KBRD_PWR##X##_pin );\
  io_set_dir_out( KBRD_PWR##X##_PORT, KBRD_PWR##X##_pin );\
\
  kbrd_short_delay;\
\
  kbrd_test_for(0, X);\
  kbrd_test_for(1, X);\
  kbrd_test_for(2, X);\
  kbrd_test_for(3, X);\
  kbrd_test_for(4, X);\
  kbrd_test_for(5, X);\
\
  io_set_1(      KBRD_PWR##X##_PORT, KBRD_PWR##X##_pin );\
\
  kbrd_micro_delay;\
  io_set_dir_in( KBRD_PWR##X##_PORT, KBRD_PWR##X##_pin );\

//===========================| Function prototypes |===========================

//==========================| High level functions |===========================
/**
 * \brief Set I/O pin
 *
 * Prepare input and output pins
 */
void init_keyboard_6x5(void);

/**
 * \brief Scan and process data from keyboard
 *
 * Check sys_timer if "there is time" for scanning keyboard buttons. If Yes\n
 * then test if any key is pressed. If no key is pressed, then is called\n
 * return. If any button is pressed, then "jump" to correct option and\n
 * process keyboard data
 */
void keyboard_process(void);

void keyboard_en_dis_slave_mode(uint8_t b_enable);
//===========================| Mid level functions |===========================
/**
 * \brief Jump to main menu (top level)
 *
 * After calling this function, next time when keyboard_process will be\n
 * called user will be in main menu.
 */
void keyboard_jump_to_main_menu(void);

/**
 * \brief Process data from keyboard for "Play animation" mode
 *
 * @param i_keyboard_buttons Data from keyboard (every bit is one button)
 */
void keyboard_process_Play_animation(kbrd_btns_t i_keyboard_buttons);

/**
 * \brief Allow user to browse in menu.
 *
 * @param i_keyboard_buttons Data from keyboard (every bit is one button)
 */
void keyboard_process_Menu(kbrd_btns_t i_keyboard_buttons);

/**
 * \brief Draw your onwn 3D image!
 * @param i_keyboard_buttons Data from keyboard (every bit is one button)
 */
void keyboard_process_draw(kbrd_btns_t i_keyboard_buttons);

//===========================| Low level functions |===========================
/**
 * \brief Scan keyboard and return information about pressed buttons
 *
 * Scan matrix keyboard for pressed buttons. If button pressed, then is\n
 * written "1" to return value on position of button
 *
 * @return Bits of pressed buttons. Because is used 30 buttons, then is used\n
 * 32bit long structure.
 */
kbrd_btns_t keyboard_scan(void);

#endif
