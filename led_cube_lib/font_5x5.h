/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Standard font for RGB LED cube 5x5x5
 *
 * Because it is not so easy to draw text pixel by pixel, so this library was\n
 * created. It contains binary representation characters. Characters are 5x5\n
 * pixel (dots).\n
 * Used font at: http://www.dafont.com/5x5-dots.font
 *
 * \code
 * Data format (binary data for framebuffer)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 0 | R | R | R | R | R | G | G | G | G | G | B | B | B | B | B |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 * Instruction format (if MSB is 1, then it is possible change some settings)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 1 | C | C | C | C | C | C | C | V | V | V | V | V | V | V | V |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *  C - command (0~127)
 *  V - value   (0~255)
 *
 * Physical orientation in column:
 *
 *       C0         C1     ...
 *     +-----+   +-----+   ...
 * MSB |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 * LSB |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 * \endcode
 *
 * Created  2013/06/18\n
 * Modified 2013/06/18
 */

// Trick for multiply include this file
#ifndef _font_5x5_library_
#define _font_5x5_library_


/*-----------------------------------------*
 |               Includes                  |
 *-----------------------------------------*/
#include <inttypes.h>           // Data types
#include <avr/pgmspace.h>       // Font is stored in flash memory

extern const uint16_t font_5x5_A[];
extern const uint16_t font_5x5_B[];
extern const uint16_t font_5x5_C[];
extern const uint16_t font_5x5_D[];
extern const uint16_t font_5x5_E[];
extern const uint16_t font_5x5_F[];
extern const uint16_t font_5x5_G[];
extern const uint16_t font_5x5_H[];
extern const uint16_t font_5x5_I[];
extern const uint16_t font_5x5_J[];
extern const uint16_t font_5x5_K[];
extern const uint16_t font_5x5_L[];
extern const uint16_t font_5x5_M[];
extern const uint16_t font_5x5_N[];
extern const uint16_t font_5x5_O[];
extern const uint16_t font_5x5_P[];
extern const uint16_t font_5x5_Q[];
extern const uint16_t font_5x5_R[];
extern const uint16_t font_5x5_S[];
extern const uint16_t font_5x5_T[];
extern const uint16_t font_5x5_U[];
extern const uint16_t font_5x5_V[];
extern const uint16_t font_5x5_W[];
extern const uint16_t font_5x5_X[];
extern const uint16_t font_5x5_Y[];
extern const uint16_t font_5x5_Z[];

extern const uint16_t font_5x5_a[];
extern const uint16_t font_5x5_b[];
extern const uint16_t font_5x5_c[];
extern const uint16_t font_5x5_d[];
extern const uint16_t font_5x5_e[];
extern const uint16_t font_5x5_f[];
extern const uint16_t font_5x5_g[];
extern const uint16_t font_5x5_h[];
extern const uint16_t font_5x5_i[];
extern const uint16_t font_5x5_j[];
extern const uint16_t font_5x5_k[];
extern const uint16_t font_5x5_l[];
extern const uint16_t font_5x5_m[];
extern const uint16_t font_5x5_n[];
extern const uint16_t font_5x5_o[];
extern const uint16_t font_5x5_p[];
extern const uint16_t font_5x5_q[];
extern const uint16_t font_5x5_r[];
extern const uint16_t font_5x5_s[];
extern const uint16_t font_5x5_t[];
extern const uint16_t font_5x5_u[];
extern const uint16_t font_5x5_v[];
extern const uint16_t font_5x5_w[];
extern const uint16_t font_5x5_x[];
extern const uint16_t font_5x5_y[];
extern const uint16_t font_5x5_z[];

extern const uint16_t font_5x5_0[];
extern const uint16_t font_5x5_1[];
extern const uint16_t font_5x5_2[];
extern const uint16_t font_5x5_3[];
extern const uint16_t font_5x5_4[];
extern const uint16_t font_5x5_5[];
extern const uint16_t font_5x5_6[];
extern const uint16_t font_5x5_7[];
extern const uint16_t font_5x5_8[];
extern const uint16_t font_5x5_9[];

extern const uint16_t font_5x5_quest[];
#endif
