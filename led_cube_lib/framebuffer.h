/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Framebuffer for RGB LED cube 5x5x5
 *
 * Framebuffer display once image on LED cube. As parameter of framebuffer is\n
 * pointer to image in SRAM memory. Framebuffer disable global interrupt\n
 * however enable it when is needed some delay, so in this "time spaces" MCU\n
 * can to other stuff. Also there is called function, witch user can define.\n
 * This function should be defined as variable of macro\n
 * "column_space_function". Recommended is call some process scheduler.\n
 * However it do not have to be defined.
 *
 * Created  2013/05/18\n
 * Modified 2015/07/05
*/


// Trick for multiply include this file
#ifndef _framebuffer_library_
#define _framebuffer_library_

/*-----------------------------------------*
 |               Includes                  |
 *-----------------------------------------*/
#include <avr/io.h>             // Defined I/O
#include <avr/interrupt.h>      // Interrupts
#include <inttypes.h>           // Data types
#include "rgb_led_cube_5x5x5.h"// Symbolic names for pins and other settings
#include <util/delay.h>
#include "bit_operations.h"     // Basic operations with pins

/*
 * Because there is some time between rendering two COLUMNs, in this time space
 * can be called some small user function. Function should no take more than
 * 500us! Example of usage:
 * #include "file_where_is_function.h"
 * #define column_space_function some_user_defined_functon()
 *
 * If user do not want call any function in this time, then DO NOT DEFINE
 * column_space_function !!!
 *
 */
#include "scheduler.h"
/**
 * \brief Function, that is called during COLUMN change
 */
#define column_space_function   scheduler()

/*-----------------------------------------*
 |               Constants                 |
 *-----------------------------------------*/
#ifndef column_space_function
#warning "column_space_function not defined!!! Used dummy NOP instruction!\
Please make sure you do not need do anything when framebuffer is running!\
If you just forget, just type something like this:\n\
#define column_space_function  my_function_between_column_change()"

#define column_space_function  __asm volatile("NOP"::)

#endif

/**
 * \brief Time space (in us) between "two columns"
 *
 * ----- time ----->\n
 * \< COLUMN0 active \> \< time delay \> \< COLUMN1 active \>
 * \< time delay \> \< COLUMN2 active \> ....
 * This value should not exceed 700, else images on LED cube will blink.\n
 * Although it should not be lower than 100. In this case this do not have\n
 * to be enough time for data processing -> all communication with MCU will\n
 * slow down (can even break). Recommended is 500.
 */
#define column_delay    350
/*-----------------------------------------*
 |               Macros                    |
 *-----------------------------------------*/
/**
 * \brief Turn off all LED in WALL(X)
 */
#define framebuffer_wallX_void(X)       \
  i_fb_data[X][0] = i_fb_data[X][1] =\
  i_fb_data[X][2] = i_fb_data[X][3] = i_fb_data[X][4] = 0;


/**
 * \brief Turn on all LED in WALL(X)
 */
#define framebuffer_wallX_ones(X)       \
  i_fb_data[X][0] = i_fb_data[X][1] =\
  i_fb_data[X][2] = i_fb_data[X][3] = i_fb_data[X][4] = 0x7FFF;

/**
 * \brief Copy data from WALL(X) to WALL(Y)
 */
#define framebuffer_copy_wallX_to_wallY(X,Y)    \
  i_fb_data[Y][0] = i_fb_data[X][0];\
  i_fb_data[Y][1] = i_fb_data[X][1];\
  i_fb_data[Y][2] = i_fb_data[X][2];\
  i_fb_data[Y][3] = i_fb_data[X][3];\
  i_fb_data[Y][4] = i_fb_data[X][4];


/**
 * \brief Backup WALL(X) 2D frame to temporary variable
 */
#define framebuffer_backup_wallX(X)     \
  i_tmp_2D_frame[0] = i_fb_data[X][0];\
  i_tmp_2D_frame[1] = i_fb_data[X][1];\
  i_tmp_2D_frame[2] = i_fb_data[X][2];\
  i_tmp_2D_frame[3] = i_fb_data[X][3];\
  i_tmp_2D_frame[4] = i_fb_data[X][4];

/**
 * \brief Restore data from temporary variable to WALL(X)
 */
#define framebuffer_restore_to_wallX(X) \
  i_fb_data[X][0] = i_tmp_2D_frame[0];\
  i_fb_data[X][1] = i_tmp_2D_frame[1];\
  i_fb_data[X][2] = i_tmp_2D_frame[2];\
  i_fb_data[X][3] = i_tmp_2D_frame[3];\
  i_fb_data[X][4] = i_tmp_2D_frame[4];

/**
 * \brief Copy data from COLUMN(X) to COLUMN(Y)
 */
#define framebuffer_copy_columnX_to_columnY(X,Y)        \
  i_fb_data[0][Y] = i_fb_data[0][X];\
  i_fb_data[1][Y] = i_fb_data[1][X];\
  i_fb_data[2][Y] = i_fb_data[2][X];\
  i_fb_data[3][Y] = i_fb_data[3][X];\
  i_fb_data[4][Y] = i_fb_data[4][X];


/**
 * \brief Backup COLUMN(X) in whole 3D frame (5x COLUMN0)
 */
#define framebuffer_backup_columnX(X)   \
  i_tmp_2D_frame[0] = i_fb_data[0][X];\
  i_tmp_2D_frame[1] = i_fb_data[1][X];\
  i_tmp_2D_frame[2] = i_fb_data[2][X];\
  i_tmp_2D_frame[3] = i_fb_data[3][X];\
  i_tmp_2D_frame[4] = i_fb_data[4][X];

/**
 * \brief Restore data from temporary variable to 5x COLUMN(X)
 */
#define framebuffer_restore_to_columnX(X)       \
  i_fb_data[0][X] = i_tmp_2D_frame[0];\
  i_fb_data[1][X] = i_tmp_2D_frame[1];\
  i_fb_data[2][X] = i_tmp_2D_frame[2];\
  i_fb_data[3][X] = i_tmp_2D_frame[3];\
  i_fb_data[4][X] = i_tmp_2D_frame[4];


/**
 * \brief Rotate data in column up
 *
 * 1) Backup actual column\n
 * 2) Rotate data (some bits are not correct)
 * 3) Rotate backup
 * 4) Mask backup
 * 5) Clear backup
 * 6) Clear not valid bits in original
 * 7) Add correct bits
 */
#define framebuffer_rotate_up_wallX_collumnY(X,Y)            \
  i_tmp_2D_frame[Y] = i_fb_data[X][Y];\
  i_fb_data[X][Y]   = i_fb_data[X][Y]<<1;\
  i_tmp_2D_frame[Y] = i_tmp_2D_frame[Y]>>4;\
  i_tmp_2D_frame[Y] = i_tmp_2D_frame[Y] & 0b0000010000100001;\
  i_fb_data[X][Y]   = i_fb_data[X][Y]   & 0b0111101111011110;\
  i_fb_data[X][Y]   = i_fb_data[X][Y]   | i_tmp_2D_frame[Y];


/**
 * \brief Rotate data in column down
 *
 * 1) Backup actual column\n
 * 2) Rotate data (some bits are not correct)
 * 3) Rotate backup
 * 4) Mask backup
 * 5) Clear backup
 * 6) Clear not valid bits in original
 * 7) Add correct bits
 */
#define framebuffer_rotate_down_wallX_collumnY(X,Y)          \
  i_tmp_2D_frame[Y] = i_fb_data[X][Y];\
  i_fb_data[X][Y]   = i_fb_data[X][Y]>>1;\
  i_tmp_2D_frame[Y] = i_tmp_2D_frame[Y]<<4;\
  i_tmp_2D_frame[Y] = i_tmp_2D_frame[Y] & 0b0100001000010000;\
  i_fb_data[X][Y]   = i_fb_data[X][Y]   & 0b0011110111101111;\
  i_fb_data[X][Y]   = i_fb_data[X][Y]   | i_tmp_2D_frame[Y];


/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/
/**
 * \brief Set I/O for framebuffer to defaults and prepare Timer0
 *
 * Set only pins, that framebuffer controls and prepare Timer0
 */
void init_framebuffer(void);

/**
 * \brief Frame buffer use pointer to array with data, witch represents one 3D
 * image
 *
 * This function display data pointed to array \b p_fb_data\b [\b ]\b [\b ] to
 * LED cube. Whole function must run in infinite loop, else just once image
 * will be showed (for very short time). And because of multiplex, LEDs have to
 * been "refreshed"
 *
 * \param p_fb_data Pointer to frame buffer. First index is for "wall",
 * second is for "column". Size of every "column" is 16 bits. MSB bit (highest
 * LED in "column") is always "first". LSB bit (lowest one) is "last".
 *
 * \code
 * Data format (binary data for framebuffer)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 0 | R | R | R | R | R | G | G | G | G | G | B | B | B | B | B |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 * Instruction format (if MSB is 1, then it is possible change some settings)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 1 | C | C | C | C | C | C | C | V | V | V | V | V | V | V | V |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *  C - command (0~127)
 *  V - value   (0~255)
 *
 * Physical orientation in column:
 *
 *       C0         C1     ...
 *     +-----+   +-----+   ...
 * MSB |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 * LSB |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 * \endcode
 */
void framebuffer(volatile uint16_t (*p_fb_data)[5][5]);


/**
 * \brief Add one dot to framebuffer.
 *
 * This is not very efficient way how to draw 3D images, but it can be very\n
 *  helpful in cases that are not time crucial.
 *
 * @param p_fb_data Pointer to framebuffer. To this framebuffer array will be\n
 *                  added "dot".
 * @param i_wall Specify wall, where dot will be.
 * @param i_column Specify column where dot will be.
 * @param i_led Specify LED where dot should appear.
 * @param i_red Red LED on? 0 no, else yes
 * @param i_green Green LED on? 0 no, else yes
 * @param i_blue Blue LED on? 0 no, else yes
 * @return 0 if no problem. If some parameter is incorrect do not add dot and\n
 *           return non zero value
 */
uint8_t framebuffer_add_dot(volatile uint16_t (*p_fb_data)[5][5],
                                uint8_t i_wall,
                                uint8_t i_column,
                                uint8_t i_led,
                                uint8_t i_red,
                                uint8_t i_green,
                                uint8_t i_blue);

/**
 * \brief Just clean all data in framebuffer
 * @param p_fb_data Pointer to frame buffer.
 */
void framebuffer_clean(volatile uint16_t (*p_fb_data)[5][5]);

/**
 * \brief Copy one framebuffer to another.
 *
 * This can be very handy for backuping and restoring
 * @param p_fb_src Source framebuffer (data from here)
 * @param p_fb_dst Destination framebuffer (put there)
 */
void framebuffer_copy(volatile uint16_t (*p_fb_src)[5][5],
                      volatile uint16_t (*p_fb_dst)[5][5]);

/**
 * \brief According parameter set active "wall".
 *
 * If input parameter is out of HW limit, then none "wall" is activated
 *
 * \param i_wall_number Wall number - 0~5
 */
void set_active_wall( uint8_t i_wall_number );


/**
 * \brief According parameter set active "column".
 *
 * If input parameter is out of HW limit, then none "column" is activated
 *
 * \param i_column_number Column number 0~5
 */
void set_active_column( uint8_t i_column_number );


/**
 * \brief Load bits stored in i_data_r to bus with \b red LEDs
 *
 * LSB is represent lowest red diode in column
 *
 * \param i_data_r Binary data for red color
 */
void load_DATA_RED_on_bus(   uint8_t i_data_r );


/**
 * \brief Load bits stored in i_data_g to bus with \b green LEDs
 *
 * LSB is represent lowest green diode in column
 *
 * \param i_data_g Binary data for green color
 */
void load_DATA_GREEN_on_bus( uint8_t i_data_g );



/**
 * \brief Load bits stored in i_data_b to bus with \b blue LEDs
 *
 * LSB is represent lowest blue diode in column
 *
 * \param i_data_b Binary data for blue color
 */
void load_DATA_BLUE_on_bus(  uint8_t i_data_b );


/**
 * \brief Enable interrupt for while
 */
void enable_isr_for_while(void);

#endif
