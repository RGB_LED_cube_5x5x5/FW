/**
 * \file
 *
 * \brief Driver for PWM on ATmega64 for controlling "R_GND", "G_GND" and
 * "B_GND"
 *
 * For advanced effects is PWM nice feature
 *
 * Created:  2013/05/18\n
 * Modified: 2015/06/28
 */


// Trick for multiply include this file
#ifndef _pwm_rgb_library_
#define _pwm_rgb_library_

//================================| Includes |=================================
#include <avr/io.h>             // Definitions AVR I/O's
#include "bit_operations.h"     // Basic bit operations
#include "../rgb_led_cube_5x5x5.h" // Local setting (mainly symbolic pin names)

//=================================| Macros |==================================

/**
 * \brief Set PWM value for red LED's
 *
 * Just paste value to OCR3A register -> set PWM for red LED's
 */
#define pwm_set_r(value_8bit)   OCR3A = value_8bit;


/**
 * \brief Set PWM value for green LED's
 *
 * Just paste value to OCR3B register -> set PWM for green LED's
 */
#define pwm_set_g(value_8bit)   OCR3B = value_8bit;


/**
 * \brief Set PWM value for blue LED's
 *
 * Just paste value to OCR3C register -> set PWM for blue LED's
 */
#define pwm_set_b(value_8bit)   OCR3C = value_8bit;

#define pwm_get_r()             OCR3A
#define pwm_get_g()             OCR3A
#define pwm_get_b()             OCR3A
//===========================| Function prototypes |===========================
/**
 * \brief Prepare HW PWM channels
 *
 * If pwm_support is set to "disabled", then pins are only set to "1" forever
 */
void init_pwm_rgb(void);

#endif
