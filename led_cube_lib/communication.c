/**
 * \file
 *
 * \brief Library for communication with another devices
 *
 * This library should handle communication from UART and I2C.
 *
 * Created: 2015/06/21\n
 * Updated: 2016/08/17
 *
 * @todo: Add timeouts to wait_* states (we can not wait for ever)
 *
 * \version 0.2
 *
 * \author Martin Stejskal
 */

#include "communication.h"

//============================| Global variables |=============================
/**
 * @brief Keep information about actual state through eternity
 */
static comm_states e_state = COMM_WAIT_FOR_INIT;


static uint8_t rx_buffer[COMM_BUFF_SIZE];

//================================| Functions |================================
/**
 * @brief Initialize HW and set up state machine
 */
comm_res communication_init(void)
{
  // Initialize UART and check status
  if( uart_init()  !=  UART_OK ){
    return COMM_FAIL;
  }

  // Limit received characters for string
  uart_set_max_str_lenght(COMM_BUFF_SIZE);

  /* Since UART is initialized, we can send "early message" mainly for debug
   * purpose. Do not care about returned code.
   */
  uart_request_tx_str(COMM_DEV_NAME);

  return COMM_OK;
}



/**
 * @brief Task for communication state machine
 *
 * State machine is for communication PC <---> cube
 */
void communication_task(void)
{
  switch(e_state){
    case COMM_WAIT_FOR_INIT: comm_wait_for_init(); break;
    case COMM_WAIT_FOR_COMMAND: comm_wait_for_command(); break;
    case COMM_FILL_BUFFER: comm_fill_buff(); break;
    /* In case, that we're out of cases (never should happen, but you do not
     * know), default will be "wait for init"
     */
    default: e_state=COMM_WAIT_FOR_INIT;
  }
}




/**
 * @brief Waiting for initialization
 *
 * This function simply waiting for initialization sequence from PC side
 */
void comm_wait_for_init(void){
  /* We need another state machine for this function as well, since we can not
   * afford use blocking functions.
   */
  static comm_w84init_states e_w84i_state = COMM_W84I_1_REQ_RX_DATA;

  /* Since in "switch" is not possible to define new variables, I have to push
   * them here, even although I do not like it...
   */
  uart_res e_uart_status;
  uint8_t i_status;


  switch(e_w84i_state){
    /*************************************************************************/
    // Send request for data receive
    case COMM_W84I_1_REQ_RX_DATA:
      // Check if RX side is ready
      uart_request_rx_str_ready(&i_status);

      // Check if ready
      if(i_status){
        // Ready -> send request and move to next state
        e_uart_status = uart_request_rx_str( (char*)&rx_buffer[0] );

        // If request was successful -> move to another state
        if(e_uart_status == UART_OK){
          e_w84i_state = COMM_W84I_2_W84_DATA;
        }
      }
      break;
    /*************************************************************************/
    // Wait for RX data
    case COMM_W84I_2_W84_DATA:
      // Check if RX side is ready
      uart_request_rx_str_ready(&i_status);

      // Check if ready
      if(i_status){
        // Ready -> Check received string
        if(strcmp((char*)&rx_buffer[0], COMM_INIT_WORD) == 0){
          // Equal -> send name (change state)
          e_w84i_state = COMM_W84I_3_SEND_NAME;
        }
        else
        {
          /* String is different -> error? Let's start from scratch. But at
           * least try to send message to sender, that we can not understand.
           * This time is not checked status, since it is not crucial.
           */
          uart_request_tx_str(CONN_NOT_UNDERSTAND);

          e_w84i_state = COMM_W84I_1_REQ_RX_DATA;
        }
      }
      break;
    /*************************************************************************/
    case COMM_W84I_3_SEND_NAME:
      // Check if TX side is ready.
      uart_request_tx_str_ready(&i_status);
      // If ready, send device name and check status. If fail occur -> re-init
      if(i_status){
        e_uart_status = uart_request_tx_str(COMM_DEV_NAME);
        if(e_uart_status != UART_OK){
          e_w84i_state = COMM_W84I_1_REQ_RX_DATA;
        }
        else{
          e_w84i_state = COMM_W84I_4_REQ_RX_DATA;
        }
      }
      break;
    /*************************************************************************/
    case COMM_W84I_4_REQ_RX_DATA:
      // Check if RX side is ready
      uart_request_rx_str_ready(&i_status);

      // Check if ready
      if(i_status){
        // Ready -> send request and move to next state
        e_uart_status = uart_request_rx_str( (char*)&rx_buffer[0] );

        // If request was successful -> move to another state
        if(e_uart_status == UART_OK){
          e_w84i_state = COMM_W84I_5_W84_DATA;
        }
        else
        {
          // When something get's wrong, get back
          e_w84i_state = COMM_W84I_1_REQ_RX_DATA;
        }
      }
      break;
    /*************************************************************************/
    case COMM_W84I_5_W84_DATA:
      // Check if RX side is ready
      uart_request_rx_str_ready(&i_status);

      // Check if ready
      if(i_status){
        // Ready -> Check received string
        if(strcmp((char*)&rx_buffer[0], CONN_CUBE_DIMENSION_Q) == 0){
          // Equal -> send name (change state)
          e_w84i_state = COMM_W84I_6_SEND_LED_IDX;
        }
        else
        {
          /* String is different -> error? Let's start from scratch. But at
           * least try to send message to sender, that we can not understand.
           * This time is not checked status, since it is not crucial.
           */
          uart_request_tx_str(CONN_NOT_UNDERSTAND);

          e_w84i_state = COMM_W84I_1_REQ_RX_DATA;
        }
      }
      break;
    /*************************************************************************/
    case COMM_W84I_6_SEND_LED_IDX:
      // Check if TX side is ready.
      uart_request_tx_str_ready(&i_status);
      // If ready, send device name and check status. If fail occur -> re-init
      if(i_status){
        e_uart_status = uart_request_tx_str(CONN_CUBE_DIMENSION_A);
        if(e_uart_status == UART_OK){
          // So far so good -> change mode
          e_state = COMM_WAIT_FOR_COMMAND;
        }
        // And next time we need to start from scratch again... it is loop.
        e_w84i_state = COMM_W84I_1_REQ_RX_DATA;
      }
      break;
    /*************************************************************************/
    // Default should not never happen, but ...
    default:
      e_w84i_state = COMM_W84I_1_REQ_RX_DATA;
      break;
  }
}



/**
 * @brief After initialization is done, this function wait and handle commands
 */
void comm_wait_for_command(void)
{
  // State machine
  static comm_w84command_states e_w84c_state = COMM_W84C_1_REQ_RX_DATA;

  /* Since in "switch" is not possible to define new variables, I have to push
   * them here, even although I do not like it...
   */
  uart_res e_uart_status;
  uint8_t i_status;

  /* Our temporary variable for easy tracking of commands (strcomp take too
   * much time)
   */
  static uint8_t cmd_idx = 0;

  switch(e_w84c_state){
    /*************************************************************************/
    // Check if RX side is ready
    case COMM_W84C_1_REQ_RX_DATA:
      // Check if RX side is ready
      uart_request_rx_str_ready(&i_status);

      // Check if ready
      if(i_status){
        // Ready -> send request and move to next state
        e_uart_status = uart_request_rx_str( (char*)&rx_buffer[0] );

        // If request was successful -> move to another state
        if(e_uart_status == UART_OK){
          e_w84c_state = COMM_W84C_2_W84_DATA;
        }
      }
      break;
    /*************************************************************************/
    // Check if we received data
    case COMM_W84C_2_W84_DATA:
      // Check if RX side is ready
      uart_request_rx_str_ready(&i_status);

      // Check if ready
      if(i_status){
        // Ready -> Check received string
        if(strcmp((char*)&rx_buffer[0], CONN_CMD_FILL_BUFF_REQ) == 0){
          // Equal -> send name (change state)
          e_w84c_state = COMM_W84C_3_SEND_OK;
          // This is command number 1 :D
          cmd_idx = 1;
        }
        else
        {
          /* String is different -> error? Let's start from scratch. But at
           * least try to send message to sender, that we can not understand.
           * This time is not checked status, since it is not crucial.
           */
          uart_request_tx_str(CONN_NOT_UNDERSTAND);

          e_w84c_state = COMM_W84C_1_REQ_RX_DATA;
          e_state = COMM_WAIT_FOR_INIT;
        }
      }
      break;
    /*************************************************************************/
    // Response to PC
    case COMM_W84C_3_SEND_OK:
      uart_request_tx_str_ready(&i_status);
      // If ready, send device name and check status. If fail occur -> re-init
      if(i_status){
        e_uart_status = uart_request_tx_str(CONN_OK_RESP);
        if(e_uart_status != UART_OK){
          e_w84c_state = COMM_W84C_1_REQ_RX_DATA;
          e_state = COMM_WAIT_FOR_INIT;
        }
        else{
          e_w84c_state = COMM_W84C_4_EXEC_CMD;
        }
      }
      break;
    /*************************************************************************/
    // Simple execution of command
    case COMM_W84C_4_EXEC_CMD:
      // Get freebuffer free for our purpose
      keyboard_en_dis_slave_mode(1);

      // According to command ID, change main state
      switch(cmd_idx){
        case 1:
          e_w84c_state = COMM_W84C_1_REQ_RX_DATA;
          e_state = COMM_FILL_BUFFER;
          break;
        // Never should happen -> internal error
        default:
          e_uart_status = UART_FAIL;
          while(e_uart_status != UART_OK){
            e_uart_status =
              uart_request_tx_str("Internal when executing commands ERROR!");
          }
          keyboard_en_dis_slave_mode(0);

          e_w84c_state = COMM_W84C_1_REQ_RX_DATA;
          e_state = COMM_WAIT_FOR_INIT;
          break;
      }
      break;
    /*************************************************************************/
    default:
      // This never should happen, but....
      e_w84c_state = COMM_W84C_1_REQ_RX_DATA;
      break;
  }
}

/**
 * @brief When command "Fill buffer" occur, this function became useful
 */
void comm_fill_buff(void)
{
  // State machine
  static comm_fill_buffer_states e_fb_state = COMM_FILL_B_1_REQ_RX_DATA;

  /* Since in "switch" is not possible to define new variables, I have to push
   * them here, even although I do not like it...
   */
  uart_res e_uart_status;
  uint8_t i_status;

  // For calculating wall, column, LED
  uint8_t i_wall  =0;
  uint8_t i_column=0;

  // For access to buffer
  uint8_t *p_i_rx_buff;

  switch(e_fb_state){
    case COMM_FILL_B_1_REQ_RX_DATA:
      // Check if RX side is ready to accept another request
      uart_request_rx_data_ready(&i_status);

      // Check status
      if(i_status){
        // Ready -> send request and move to next state (We wait for whole
        // 3D frame -> 5x5x2B (16b) -> 50 Bytes) + 3 PWM
        e_uart_status = uart_request_rx_data(&rx_buffer[0], 53);

        if(e_uart_status == UART_OK){
          // All right -> move to another state
          e_fb_state = COMM_FILL_B_2_W84_DATA;
        }
      }
      break;
    /*************************************************************************/
    // Request for RX data was send, wait for data
    case COMM_FILL_B_2_W84_DATA:
      uart_request_rx_data_ready(&i_status);

      if(i_status){
        /* Data ready -> check if command have correct code. If not, send "NG"
         * message in another state.
         */
        // RGB information (MSB bit should be zero)
        if((0x80 & rx_buffer[0]) == 0x00){
          // This is correct -> next state send index
          e_fb_state = COMM_FILL_B_3_SEND_ACK;
        }
        // Close request (move to state "Wait for command")
        else if(rx_buffer[0] == CONN_QUIT_CMD){
          e_fb_state = COMM_FILL_B_5_SEND_OK_AND_QUIT;
        }
        else
        {
          // Incorrect command -> next state is send NG response
          e_fb_state = COMM_FILL_B_4_SEND_NG_AND_QUIT;
        }
      }
      break;
    /*************************************************************************/
    // So far so good -> send back ACK
    case COMM_FILL_B_3_SEND_ACK:
      uart_request_tx_str_ready(&i_status);

      // If ready
      if(i_status){
        // Response to PC
        e_uart_status = uart_request_tx_str(CONN_OK_RESP);

        if(e_uart_status == UART_OK){
          // Pointer to RX buffer
          p_i_rx_buff = &rx_buffer[0];


          // Message sent -> fill framebuffer
          for(i_wall=0 ; i_wall<5 ; i_wall++){
            for(i_column=0 ; i_column<5 ; i_column++){
              // Simply copy byte per byte to framebuffer (MSB first)
              i_fb_data[i_wall][i_column] = ((*p_i_rx_buff)    <<8 ) +
                                             (*(p_i_rx_buff +1)        );
              p_i_rx_buff = p_i_rx_buff +2;
            }
          }

          pwm_set_r(* (p_i_rx_buff++) )
          pwm_set_g(* (p_i_rx_buff++) )
          pwm_set_b(* (p_i_rx_buff++) )


          // Get ready for another batch
          e_fb_state = COMM_FILL_B_1_REQ_RX_DATA;
        }
        else{
          // Something went wrong -> back to init sequence
          keyboard_en_dis_slave_mode(0);

          e_fb_state = COMM_FILL_B_1_REQ_RX_DATA;
          e_state = COMM_WAIT_FOR_INIT;
        }
      }
      break;
    /*************************************************************************/
    // We got unknown Byte -> send at least NG message
    case COMM_FILL_B_4_SEND_NG_AND_QUIT:
      // Check if TX side is ready.
      uart_request_tx_str_ready(&i_status);
      // If ready, send device name and check status. If fail occur -> re-init
      if(i_status){
        e_uart_status = uart_request_tx_str(CONN_NG_RESP);
        if(e_uart_status == UART_OK){
          // Message sent. Release framebuffer
          keyboard_en_dis_slave_mode(0);

          e_fb_state = COMM_FILL_B_1_REQ_RX_DATA;
          e_state = COMM_WAIT_FOR_INIT;
        }
        // Else stay in this state and try to send message
      }
      break;
    /*************************************************************************/
    // Close command received -> change state back to "Wait for command"
    case COMM_FILL_B_5_SEND_OK_AND_QUIT:
      // Check if TX side is ready.
      uart_request_tx_str_ready(&i_status);
      // If ready, send device name and check status. If fail occur -> re-init
      if(i_status){
        e_uart_status = uart_request_tx_str(CONN_OK_RESP);
        if(e_uart_status == UART_OK){
          // Message sent. Release framebuffer
          keyboard_en_dis_slave_mode(0);

          e_fb_state = COMM_FILL_B_1_REQ_RX_DATA;
          e_state = COMM_WAIT_FOR_COMMAND;
        }
        // Else stay in this state and try to send message
      }
      break;
    /*************************************************************************/
    default:
      // Never should happen, but...
      e_fb_state = COMM_FILL_B_1_REQ_RX_DATA;
      break;
  }
}


/**
 * \brief Send string over UART interface.
 *
 * This is mainly for sending debug messages, because it actually wait until\n
 * UART is ready.
 *
 * @param c_str Pointer to string array
 */
void print_str(char *p_c_str)
{
  uart_res e_status = UART_FAIL;
  uint8_t i_status = 0;

  while(e_status != UART_OK)
  {
    ///\todo Add some smart system of warning or something like this
    e_status = uart_request_tx_str(p_c_str);
    switch(e_status)
    {
    case UART_OK:
      // If OK, nothing to do. While condition solve this
      break;
    case UART_FAIL:
      // If this fails, we can not do anzthing from here. Just give it up
      return;
      break;
    case UART_TIMEOUT:
      // Good reason to give it up, because we waited too long without effect
      return;
      break;
    case UART_INVALID_PARAM:
      // How is this possible? WTF?
      // Just start panic
      return;
      break;
    case UART_BUSY:
      // OK, just one more round
      break;
    }
  }

  // And wait until string is send
  while(i_status == 0){
    uart_request_tx_str_ready(&i_status);
  }

}
