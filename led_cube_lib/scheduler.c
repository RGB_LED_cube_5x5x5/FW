/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Simple process scheduler
 *
 * Scheduler can be used in cases, that is need to run periodic many process.\n
 * Called function MUST BE type void function(void) !!!.Scheduler do not take\n
 * any arguments and return values!
 *
 * Created:  2013/06/08\n
 * Modified: 2015/06/24
 */

#include "scheduler.h"

/*-----------------------------------------*
 |             Global variables            |
 *-----------------------------------------*/



/*-----------------------------------------*
 |                Functions                |
 *-----------------------------------------*/
void scheduler(void)
{
  /**
   * \brief Variable for scheduler()
   *
   * Define witch process will be called this time. Initial value is 0, however\n
   * it do not have to be defined anyway
   */
  static uint8_t i_process_id = 0;


  // Switch according actual process_id
  switch(i_process_id)
  {
#ifdef scheduler_task_0
  case 0: scheduler_task_0 ; break;
#endif
#ifdef scheduler_task_1
  case 1: scheduler_task_1 ; break;
#endif
#ifdef scheduler_task_2
  case 2: scheduler_task_2 ; break;
#endif
#ifdef scheduler_task_3
  case 3: scheduler_task_3 ; break;
#endif
#ifdef scheduler_task_4
  case 4: scheduler_task_4 ; break;
#endif
#ifdef scheduler_task_5
  case 5: scheduler_task_5 ; break;
#endif
#ifdef scheduler_task_6
  case 6: scheduler_task_6 ; break;
#endif
#ifdef scheduler_task_7
  case 7: scheduler_task_7 ; break;
#endif
#ifdef scheduler_task_8
  case 8: scheduler_task_8 ; break;
#endif
#ifdef scheduler_task_9
  case 9: scheduler_task_9 ; break;
#endif
#ifdef scheduler_task_10
  case 10: scheduler_task_10 ; break;
#endif
#ifdef scheduler_task_11
  case 11: scheduler_task_11 ; break;
#endif
#ifdef scheduler_task_12
  case 12: scheduler_task_12 ; break;
#endif
#ifdef scheduler_task_13
  case 13: scheduler_task_13 ; break;
#endif
#ifdef scheduler_task_14
  case 14: scheduler_task_14 ; break;
#endif
#ifdef scheduler_task_15
  case 15: scheduler_task_15 ; break;
#endif
#ifdef scheduler_task_16
  case 16: scheduler_task_16 ; break;
#endif
#ifdef scheduler_task_17
  case 17: scheduler_task_17 ; break;
#endif
#ifdef scheduler_task_18
  case 18: scheduler_task_18 ; break;
#endif
#ifdef scheduler_task_19
  case 19: scheduler_task_19 ; break;
#endif
#ifdef scheduler_task_20
#error "Sorry. For so many processes is not scheduler prepared yet. Please\
write code manually"
#endif



  // If not defined -> start again from begin -> set i_process_id and exit
  default: i_process_id = 0; return;
  }

  // Next time will be called next function
  i_process_id++;
}


