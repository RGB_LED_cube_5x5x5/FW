/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Library for system clock (used timer)
 *
 * Sometimes is needed reference timer for some functions. For example if\n
 * there is keyboard scan. This is not necessary to run every ms, but every\n
 * 100 ms. Delay functions are not much effective, because when waiting MCU\n
 * can not do anything else.
 *
 * Created:  2013/08/14\n
 * Modified: 2013/08/14
 */


#ifndef _sys_timer_library_
#define _sys_timer_library_

/*-----------------------------------------*
 |               Includes                  |
 *-----------------------------------------*/
#include <avr/io.h>     // Input/Output - also defined
/*-----------------------------------------*
 |                  Macros                 |
 *-----------------------------------------*/
/**
 * \brief Symbolic name for system clock register
 *
 * Instead of using "direct" name for register, where is counting time, there\n
 * is symbolic name for case of change this register. User should use\n
 * sys_timer variable instead of use direct name register.
 */
#define sys_timer       TCNT1

/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/
/**
 * \brief Initialize system clock, witch can be used for functions as ref time
 *
 * Set 16bit timer1 as system clock. Some functions do not need run every ms,\n
 * but for example every 300 ms. This timer overflows approximately every\n
 * 1.049 s (F_CPU = 16 MHz ; prescaller = 256)
 */
void init_sys_timer(void);





#endif
