/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Macros and functions for bit operations in C
 *
 * Some operations are not "easy to write" so there is "easy to use". If it is
 * not necessary, then macros \b simple do not use
 *
 *
*/

// Trick for multiply include this file
#ifndef _bit_operations_library_
#define _bit_operations_library_

/*-----------------------------------------*
 |               Includes                  |
 *-----------------------------------------*/
#include <avr/io.h>

/*-----------------------------------------*
 |                  Macros                 |
 *-----------------------------------------*/
/**
 * \brief Set bit in variable.
 *
 * Set defined bit in variable or register.\n
 * Example:\n
 *  set_bit_simple(DDRB,PB4);  // Set PB4 as output\n
 *  set_bit_simple( data, 5);  // Set 5th bit to '1' in variable "data"\n
 * Following code will \b NOT \b work!!!:\n
 * #define LED_PORT DDRB\n
 * #define LED_pin  4\n
 *  set_bit_simple(LED_port, LED_pin);
 *
 * \param var Input variable.
 * \param bit_num Define witch bit will be rewritten. For 8 bit numbers use 0~7
 * \return Result is saved to input variable
 *
 */
#define set_bit_simple(var,bit_num)     var = (var) | (1<<bit_num)

/**
 * \brief Set bit in variable.
 *
 * Set defined bit in variable or register.\n
 * Example:\n
 *  set_bit(DDRB,PB4);  // Set PB4 as output\n
 *  set_bit( data, 5);  // Set 5th bit to '1' in variable "data"\n
 * Following code will work too:\n
 * #define LED_PORT DDRB\n
 * #define LED_pin  4\n
 *  set_bit(LED_port, LED_pin);
 *
 * \param var Input variable.
 * \param bit_num Define witch bit will be rewritten. For 8 bit numbers use 0~7
 * \return Result is saved to input variable
 *
 */
#define set_bit(var,bit_num)    set_bit_simple(var, bit_num)

/**
 * \brief Clear bit in variable.
 *
 * Clear defined bit in variable or register.\n
 * Example:\n
 *  clr_bit_simple(DDRB,PB4);  // Set PB4 as input\n
 *  clr_bit_simple( data, 4);  // Set 4th bit to '0' in variable "data"\n
 * Following code will \b NOT \b work!!!:\n
 * define LED_PORT DDRB\n
 * define LED_pin  4\n
 *  clr_bit_simple(LED_port, LED_pin);
 *
 * \param var Input variable.
 * \param bit_num Define witch bit will be rewritten. For 8 bit numbers use 0~7
 * \return Result is saved to input variable
 *
 */
#define clr_bit_simple(var,bit_num)     var = (var) & (~(1<<bit_num))


/**
 * \brief Clear bit in variable.
 *
 * Clear defined bit in variable or register.\n
 * Example:\n
 *  clr_bit_simple(DDRB,PB4);  // Set PB4 as input\n
 *  clr_bit_simple( data, 4);  // Set 4th bit to '0' in variable "data"\n
 * Following code will work too:\n
 * #define LED_PORT DDRB\n
 * #define LED_pin  4\n
 *  clr_bit(LED_port, LED_pin);
 *
 * \param var Input variable.
 * \param bit_num Define witch bit will be rewritten. For 8 bit numbers use 0~7
 * \return Result is saved to input variable
 *
 */
#define clr_bit(var,bit_num)    var = (var) & (~(1<<bit_num))



/**
 * \brief Set pin direction as output.
 *
 * Pin is defined by PORT and pin number. So macro take this two parameters\n
 * and create one line command. Advantage is, that only port post-fix is\n
 * given, so it can be easy combined with \b io_set_1 \n
 * Example:\n
 *  io_set_dir_out_simple(B,3);        // Produce code: DDRB = DDRB | (1<<3);\n
 * Following code will \b NOT \b work!!!:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_set_dir_out_simple(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: B -> DDRB
 * \param pin Pin number. Example 3 -> PB3 (PB3 can be given too)
 */
#define io_set_dir_out_simple(X,pin)    DDR##X = DDR##X | (1<< pin)
/**
 * \brief Set pin direction as output.
 *
 * Pin is defined by PORT and pin number. So macro take this two parameters\n
 * and create one line command. Advantage is, that only port post-fix is\n
 * given, so it can be easy combined with \b io_set_1 \n
 * Example:\n
 *  io_set_dir_out(B,3);        // Produce code: DDRB = DDRB | (1<<3);\n
 * Following code will work too:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_set_dir_out(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: B -> DDRB
 * \param pin Pin number. Example 3 -> PB3 (PB3 can be given too)
 */
#define io_set_dir_out(X,pin)           io_set_dir_out_simple(X,pin)


/**
 * \brief Set pin direction as input
 *
 * Pin is defined by PORT and pin number. So macro take this two parameters\n
 * and create command. Advantage is, that only port post-fix is given, so it\n
 * can be easy combined with \b io_read . Also clear PIN bit (for case, that\n
 * pin was set as output before).\n
 * Example:\n
 *  io_set_dir_in_simple(C,0);         // Produce code:\n
 *  //  DDRC = DDRC & (~ (1<<0) ) ; PINC = PINC & (~ (1<<0) );\n
 * Following code will \b NOT \b work!!!:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_set_dir_in_simple(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: C -> DDRC + PINC
 * \param pin Pin number. Example 0 -> PC0 (PC0 can be given too)
 */
#define io_set_dir_in_simple(X,pin)     DDR##X = DDR##X & (~(1 << pin));\
                                        PIN##X = PIN##X & (~(1 << pin))

/**
 * \brief Set pin direction as input
 *
 * Pin is defined by PORT and pin number. So macro take this two parameters\n
 * and create command. Advantage is, that only port post-fix is given, so it\n
 * can be easy combined with \b io_read . Also clear PIN bit (for case, that\n
 * pin was set as output before).\n
 * Example:\n
 *  io_set_dir_in(C,0);         // Produce code:\n
 *  //  DDRC = DDRC & (~ (1<<0) ) ; PINC = PINC & (~ (1<<0) );\n
 * Following code will work too:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_set_dir_in(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: C -> DDRC + PINC
 * \param pin Pin number. Example 0 -> PC0 (PC0 can be given too)
 */
#define io_set_dir_in(X,pin)    io_set_dir_in_simple(X,pin)


/**
 * \brief Set pin level (in PORTx register) to '1'
 *
 * Pin is defined by PORT and pin number. So macro take this two parameters\n
 * and create one line command. Advantage is, that only port post-fix is\n
 * given, so it can be easy combined with \b io_set_dir_out . Pin must be set\n
 * as output before! This macro just set PORTx register!\n
 * Example:\n
 *  io_set_1_simple(A,7);      // Produce code: PORTA = PORTA | (1<<7);\n
 * Following code will \b NOT \b work!!!:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_set_1_simple(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: A -> PORTA
 * \param pin Pin number. Example 7 -> PA7 (PA7 can be given too)
 */
#define io_set_1_simple(X,pin)          PORT##X = PORT##X | (1 << pin )


/**
 * \brief Set pin level (in PORTx register) to '1'
 *
 * Pin is defined by PORT and pin number. So macro take this two parameters\n
 * and create one line command. Advantage is, that only port post-fix is\n
 * given, so it can be easy combined with \b io_set_dir_out . Pin must be set\n
 * as output before! This macro just set PORTx register!\n
 * Example:\n
 *  io_set_1(A,7);      // Produce code: PORTA = PORTA | (1<<7);\n
 * Following code will work too:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_set_1(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: A -> PORTA
 * \param pin Pin number. Example 7 -> PA7 (PA7 can be given too)
 */
#define io_set_1(X,pin)         io_set_1_simple(X,pin)

/**
 * \brief Set pin level (in PORTx register) to '1' (HIGH)
 *
 * Same as macro \b io_set_1 .\n
 * Example:\n
 *  io_set_1(A,7);      // Produce code: PORTA = PORTA | (1<<7);\n
 * Following code will work too:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_set_H(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: A -> PORTA
 * \param pin Pin number. Example 7 -> PA7 (PA7 can be given too)
 */
// Same as "io_set_1" macro
#define io_set_H(X,pin)         io_set_1(X,pin)


/**
 * \brief Set pin level (in PORTx register) to '0'
 *
 * Pin is defined by PORT and pin number. So macro take this two parameters\n
 * and create one line command. Advantage is, that only port post-fix is\n
 * given, so it can be easy combined with \b io_set_dir_out . Pin must be set\n
 * as output before! This macro just set PORTx register!\n
 * Example:\n
 *  io_set_0_simple(D,2);      // Produce code: PORTD = PORTD & (~ (1<<2) );\n
 * Following code will \b NOT \b work!!!:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_set_0_simple(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: D -> PORTD
 * \param pin Pin number. Example 2 -> PD2 (PD2 can be given too)
 */
#define io_set_0_simple(X,pin)          PORT##X = PORT##X & (~(1 << pin))


/**
 * \brief Set pin level (in PORTx register) to '0'
 *
 * Pin is defined by PORT and pin number. So macro take this two parameters\n
 * and create one line command. Advantage is, that only port post-fix is\n
 * given, so it can be easy combined with \b io_set_dir_out . Pin must be set\n
 * as output before! This macro just set PORTx register!\n
 * Example:\n
 *  io_set_0(D,2);      // Produce code: PORTD = PORTD & (~ (1<<2) );\n
 * Following code will work too:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_set_0(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: D -> PORTD
 * \param pin Pin number. Example 2 -> PD2 (PD2 can be given too)
 */
#define io_set_0(X,pin)         io_set_0_simple(X,pin)

/**
 * \brief Set pin level (in PORTx register) to '0' (LOW)
 *
 * Same as macro \b io_set_0 .\n
 * Example:\n
 *  io_set_0(D,2);      // Produce code: PORTD = PORTD & (~ (1<<2) );\n
 * Following code will work too:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_set_L(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: D -> PORTD
 * \param pin Pin number. Example 2 -> PD2 (PD2 can be given too)
 */
#define io_set_L(X,pin)         io_set_0(X,pin)


/**
 * \brief Read pin level (in PINx register).
 *
 * Pin is defined by PORT and pin number. So macro take this two parameters\n
 * and create one line command. Advantage is, that only port post-fix is\n
 * given, so it can be easy combined with \b io_set_dir_in . Pin must be set\n
 * as input before! This macro just read PINx register and shift result!\n
 * Example:\n
 *  io_read_simple(D,6);       // Produce code: ( ( PIND & (1<<6) )>>6 )\n
 * Following code will \b NOT \b work!!!:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_read_simple(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: D -> PIND
 * \param pin Pin number. Example 6 -> PD6 (PD6 can be given too)
 * \return \b 1 if on pin is logic 1 (HIGH), otherwise return \b 0
 */
#define io_read_simple(X,pin)           ( PIN##X & (1 << pin) )>>pin


/**
 * \brief Read pin level (in PINx register).
 *
 * Pin is defined by PORT and pin number. So macro take this two parameters\n
 * and create one line command. Advantage is, that only port post-fix is\n
 * given, so it can be easy combined with \b io_set_dir_in . Pin must be set\n
 * as input before! This macro just read PINx register and shift result!\n
 * Example:\n
 *  io_read(D,6);       // Produce code: ( ( PIND & (1<<6) )>>6 )\n
 * Following code will work too:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_read(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: D -> PIND
 * \param pin Pin number. Example 6 -> PD6 (PD6 can be given too)
 * \return \b 1 if on pin is logic 1 (HIGH), otherwise return \b 0
 */
#define io_read(X,pin)          io_read_simple(X,pin)



/**
 * \brief Read pin level (in PORTx register) a little bit faster, but...
 *
 * ... beware of return value! Pin is defined by PORT and pin number. So\n
 * macro take this two parameters and create one line command. Advantage is,\n
 * that only port post-fix is given, so it can be easy combined with\n
 * \b io_set_dir_in . Pin must be set as input before! This macro just read\n
 * PINx register and NOT shift result!\n
 * Example:\n
 *  io_read_fast_simple(A,5);  // Produce code: PINA & (1<<5);\n
 * Following code will \b NOT \b work!!!:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_read_fast_simple(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: A -> PORTA
 * \param pin Pin number. Example 5 -> PA5 (PA5 can be given too)
 * \return \b 0 if on pin is logic 0 (LOW), otherwise return \b NON \b ZERO \n
 * value\n
 */
#define io_read_fast_simple(X,pin)     PIN##X & (1 << pin)



/**
 * \brief Read pin level (in PORTx register) a little bit faster, but...
 *
 * ... beware of return value! Pin is defined by PORT and pin number. So\n
 * macro take this two parameters and create one line command. Advantage is,\n
 * that only port post-fix is given, so it can be easy combined with\n
 * \b io_set_dir_in . Pin must be set as input before! This macro just read\n
 * PINx register and NOT shift result!\n
 * Example:\n
 *  io_read_fast(A,5);  // Produce code: PINA & (1<<5);\n
 * Following code will work too:\n
 * #define LED_PORT B\n
 * #define LED_pin  3\n
 *  io_read_fast(LED_PORT, LED_pin);
 *
 * \param X Port name. Example: A -> PORTA
 * \param pin Pin number. Example 5 -> PA5 (PA5 can be given too)
 * \return \b 0 if on pin is logic 0 (LOW), otherwise return \b NON \b ZERO \n
 * value\n
 */
#define io_read_fast(X,pin)     io_read_fast_simple(X,pin)

/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/


/*-----------------------------------------*
 |               Definitions               |
 *-----------------------------------------*/
/**
 * \defgroup direction_group Direction definition
 *
 * Define direction for function set_pins()
 *
 */

/**
 * \ingroup direction_group
 *
 * \{
 *
 */
#define dir_out         1
#define dir_in          0
/**
 * \}
 */




#endif
