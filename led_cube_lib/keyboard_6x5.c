/**
 * \file
 *
 * \brief Driver for matrix keyboard 6x5
 *
 * Created:  2013/06/22\n
 * Modified: 2016/08/15
 *
 * \author Martin Stejskal
 */


#include "keyboard_6x5.h"


/**
 * \brief Keep status information about actual "selection" in main menu
 *
 * Unfortunately this should be global variable, because different functions\n
 * need access to this variable.
 */
static kbrd_main_menu_t e_main_m_option=play_anim;

/**
 * \brief In submenu "Play menu" this flag decide if animation should be\n
 * played in loop
 *
 * Need to be global, because different functions need to know status
 */
static uint8_t i_play_anim_in_loop=0;


//===========================| Non user functions |============================
/**
 * \brief Load defined LED's color and change it
 *
 * Changing color is done automatically in order specified in code.
 *
 * @param i_led Define which LED will be changed
 * @param p_res_column This column will be changed. It is source and target\n
 *                     column
 * @return 0 if no problem detected. 1 if i_led value is not correct.\n
 *         2 if internal error occurs (probably badly written code, sorry...)
 *
 */
uint8_t kbrd_draw_change_one_led_state(const uint8_t  i_led,
                                       volatile uint16_t *p_res_column);


//================================| Functions |================================
void init_keyboard_6x5(void)
{
  // Keyboard - KBRD_RDx - all input and enabled pull-up
  io_set_dir_in( KBRD_RD0_PORT, KBRD_RD0_pin );
  io_set_1( KBRD_RD0_PORT, KBRD_RD0_pin );      // Activate pull-up
  io_set_dir_in( KBRD_RD1_PORT, KBRD_RD1_pin );
  io_set_1( KBRD_RD1_PORT, KBRD_RD1_pin );      // Activate pull-up
  io_set_dir_in( KBRD_RD2_PORT, KBRD_RD2_pin );
  io_set_1( KBRD_RD2_PORT, KBRD_RD2_pin );      // Activate pull-up
  io_set_dir_in( KBRD_RD3_PORT, KBRD_RD3_pin );
  io_set_1( KBRD_RD3_PORT, KBRD_RD3_pin );      // Activate pull-up
  io_set_dir_in( KBRD_RD4_PORT, KBRD_RD4_pin );
  io_set_1( KBRD_RD4_PORT, KBRD_RD4_pin );      // Activate pull-up
  io_set_dir_in( KBRD_RD5_PORT, KBRD_RD5_pin );
  io_set_1( KBRD_RD5_PORT, KBRD_RD5_pin );      // Activate pull-up

  // Keyboard - KBRD_PWRx - all as Hi-Z -> set as input with pull-up
  io_set_dir_in( KBRD_PWR0_PORT, KBRD_PWR0_pin );
  io_set_1( KBRD_PWR0_PORT, KBRD_PWR0_pin);
  io_set_dir_in( KBRD_PWR1_PORT, KBRD_PWR1_pin );
  io_set_1( KBRD_PWR1_PORT, KBRD_PWR1_pin);
  io_set_dir_in( KBRD_PWR2_PORT, KBRD_PWR2_pin );
  io_set_1( KBRD_PWR2_PORT, KBRD_PWR2_pin);
  io_set_dir_in( KBRD_PWR3_PORT, KBRD_PWR3_pin );
  io_set_1( KBRD_PWR3_PORT, KBRD_PWR3_pin);
  io_set_dir_in( KBRD_PWR4_PORT, KBRD_PWR4_pin );
  io_set_1( KBRD_PWR4_PORT, KBRD_PWR4_pin);

  // And set menu to "Play animation"
  e_main_m_option=play_anim;
}






/*---------------------------------------------------------------------------*/



void keyboard_process(void)
{

  // To this variable is written last "time" when this function was called
  static uint16_t i_last_time_kbd_scan;
  static uint16_t i_last_time_proc_func;

  /* When user press button, next button can be pressed after some time.
   * If user press button, this variable will be set as non-zero -> next time
   * will wait some time
   */
  static uint8_t  i_read_from_kbd_now;

  // Store informations about pressed buttons. Need to be set to 0 right now
  kbrd_btns_t i_keyboard_buttons;
  i_keyboard_buttons.i_all_btns = 0x00000000;


  /* Slave mode is kind a special. If "slave" mode is enabled, then keyboard
   * will be temporary disabled until mode will be changed. There are prepared
   * functions, that handle entering and leaving "slave" mode.
   */
  if(e_main_m_option == slave){
    return;
  }

  /* Test if "there is right time" to read from keyboard. Test if difference
   * between actual time and last time is bigger than kbrd_time_to_scan_kbrd
   * (this constant define time in cycles for sys_timer). Or scan keyboard if
   * on the "last time" (when this process was called) user do not press any
   * button
   */
  if( (sys_timer - i_last_time_kbd_scan)> kbrd_time_to_scan_kbrd ||
      (i_read_from_kbd_now != 0))
  {/* If difference is bigger or no pressed button last time -> scan from
    * keyboard
    */
    // Now we need to write back "last time" - write actual system time
    i_last_time_kbd_scan = sys_timer;

    // Scan data from keyboard
    i_keyboard_buttons = keyboard_scan();

    // Test if none button is pressed
    if( i_keyboard_buttons.i_all_btns == 0 )
    {
      // User did not press any button. Next time check keyboard without delay
      i_read_from_kbd_now = 1;
    }
    else
    {// Some button was pressed
      /* User pressed any button -> set i_read_from_kbd_now -> next time there
       * will be small delay
       */
      i_read_from_kbd_now = 0;
    }
  }

  /* Time to time also run "keyboard_process_*" functions. Point is that
   * actually these small subtasks do not need to be run so often, so we can
   * save some CPU time. But also it is good idea to keep reaction time fast,
   * so that is reason why condition is little bit different. Also we need to
   * do processing if some button is pressed
   */
  if(((sys_timer - i_last_time_proc_func)> (kbrd_time_to_scan_kbrd/4)) ||
     (i_keyboard_buttons.i_all_btns != 0)
    )
  {
    // Write actual time
    i_last_time_proc_func = sys_timer;

    // Jump to correct menu option / sub option
    switch(e_main_m_option)
    {
    case main_menu:
      keyboard_process_Menu(i_keyboard_buttons);
      break;
    case play_anim:
      keyboard_process_Play_animation(i_keyboard_buttons);
      break;
    case draw:
      keyboard_process_draw(i_keyboard_buttons);
      break;
    // If there is unknown case, just jump to main menu.
    default: e_main_m_option = main_menu;
    }
  }







/*
    // Write data
    for(uint8_t i=0 ; i<5 ; i++)
    {
      i_fb_data[0][i] = (i_keyboard_buttons >> ((uint32_t)(i+1)*(uint32_t)5)) & 0b00011111;
    }
*/

}


/**
 * @brief Allow 3rd functions to enable/disable slave mode
 *
 * @param b_enable Non-zero value imply enabling slave mode. Zero value imply \
 *                 disabling slave mode
 */
void keyboard_en_dis_slave_mode(uint8_t b_enable)
{
  // Backup variables
  static uint8_t i_pwm_backup[3];
  volatile static uint16_t i_fb_backup[5][5];
  volatile static stat_cfg_anime_t i_stat_cfg_anime_bkp;
  static kbrd_main_menu_t e_main_m_option_bkp;


  if(b_enable){
    // Enable slave mode

    // Backup last option and switch mode
    e_main_m_option_bkp = e_main_m_option;
    e_main_m_option=slave;

    // Backup PWM values
    i_pwm_backup[0] = pwm_get_r();
    i_pwm_backup[1] = pwm_get_g();
    i_pwm_backup[2] = pwm_get_b();

    // Backup framebuffer
    framebuffer_copy(&i_fb_data, &i_fb_backup);

    // Backup i_stat_cfg_anime and stop playing animation (no matter what)
    i_stat_cfg_anime_bkp = i_stat_cfg_anime;
    i_stat_cfg_anime.anim_scheduler_stop = 1;

    // Clean framebuffer
    framebuffer_clean(&i_fb_data);

    // Now framebuffer is free
  }
  else
  {
    // Disable slave mode

    // Restore all variables
    pwm_set_r(i_pwm_backup[0]);
    pwm_set_g(i_pwm_backup[1]);
    pwm_set_b(i_pwm_backup[2]);

    framebuffer_copy(&i_fb_backup, &i_fb_data);

    i_stat_cfg_anime = i_stat_cfg_anime_bkp;

    e_main_m_option = e_main_m_option_bkp;
  }

}


//===========================| Mid level functions |===========================
/*---------------------------------------------------------------------------*/

inline void keyboard_jump_to_main_menu(void)
{
  e_main_m_option=main_menu;
}
/*---------------------------------------------------------------------------*/
void keyboard_process_Menu(kbrd_btns_t i_keyboard_buttons)
{
  static uint8_t i_first_time = 1;
  static uint8_t i_pwm_backup[3];

  // Need to keep information about user's selection until user did not confirm
  static uint8_t i_selected_option;

  /* If this procedure is run for "first time" (when jump to menu), then
   * it is good idea to backup some things
   */
  if(i_first_time != 0)
  {
    i_pwm_backup[0] = pwm_get_r();
    i_pwm_backup[1] = pwm_get_g();
    i_pwm_backup[2] = pwm_get_b();

    framebuffer_clean(&i_fb_data);

    // Set PWM to 100%
    pwm_set_r(0xFF);
    pwm_set_g(0xFF);
    pwm_set_b(0xFF);

    // Now we are at main menu
    i_selected_option = main_menu;

    // No more for first time
    i_first_time = 0;
  }

  // According to actual menu selection, show "selected" option as LED
  // Calculate wall
  uint8_t i_wall  =0;
  uint8_t i_column=0;
  uint8_t i_led   =0;
  uint8_t i_tmp = i_selected_option;

  while(i_tmp >= 25)
  {
    i_tmp = i_tmp - 25;
    i_wall++;
  }
  // OK, we found wall, let's find column
  while(i_tmp >= 5)
  {
    i_tmp = i_tmp - 5;
    i_column++;
  }
  // OK, and now LED, which is actually simple
  i_led = i_tmp;

  // Add stylish dot
  framebuffer_add_dot(&i_fb_data, i_wall, i_column, i_led, 0, 1, 1);


  // Test BTN26 (next item in menu)
  if(i_keyboard_buttons.s.btn26)
  {// If pressed
    // Move to next option (if makes sense)
    if(i_selected_option < (mm_last_option-1))
    {
      i_selected_option++;
      framebuffer_clean(&i_fb_data);
    }
    else
    {
      // Make dot red
      framebuffer_add_dot(&i_fb_data, i_wall, i_column, i_led, 1, 0, 0);
    }
  }


  // Test BTN25 (previous item in menu)
  if(i_keyboard_buttons.s.btn25)
  {// If pressed
    // Move to previous option (if makes sense)
    if(i_selected_option >= 1)
    {
      i_selected_option--;
      framebuffer_clean(&i_fb_data);
    }
    else
    {
      // Make dot red
      framebuffer_add_dot(&i_fb_data, i_wall, i_column, i_led, 1, 0, 0);
    }
  }

  // Test BTN28 (set actual settings)
  if(i_keyboard_buttons.s.btn28)
  {
    // Change menu according to user
    e_main_m_option = i_selected_option;
  }

  // ESC button - Play animation no matter what
  if(i_keyboard_buttons.s.btn29)
  {
    e_main_m_option = play_anim;
  }

  // Common routine for BTN28 and BTN29 (when we have to switch mode)
  if(i_keyboard_buttons.s.btn28 ||
     i_keyboard_buttons.s.btn29)
  {
    // Restore PWM values
    pwm_set_r(i_pwm_backup[0]);
    pwm_set_g(i_pwm_backup[1]);
    pwm_set_b(i_pwm_backup[2]);

    /* Leaving this mode. Next time when comming back it will be for
     * "first time"
     */
    i_first_time = 1;
  }
}

/*---------------------------------------------------------------------------*/

void keyboard_process_Play_animation(kbrd_btns_t i_keyboard_buttons)
{
  volatile static uint16_t i_fb_backup[5][5];
  static uint8_t i_pwm_red   = 0xFF;
  static uint8_t i_pwm_green = 0xFF;
  static uint8_t i_pwm_blue  = 0xFF;

  static uint8_t  i_first_time = 1;

  // If jump to this mode
  if(i_first_time == 2)
  {
    // Restore framebuffer and PWM
    framebuffer_copy(&i_fb_backup, &i_fb_data);
    pwm_set_r(i_pwm_red);
    pwm_set_g(i_pwm_green);
    pwm_set_b(i_pwm_blue);

    i_first_time = 0;
  }
  else if(i_first_time == 1)
  {
    // Very first time - do not load backup of FB
    i_first_time = 0;
    pwm_set_r(i_pwm_red);
    pwm_set_g(i_pwm_green);
    pwm_set_b(i_pwm_blue);
  }

  // We want to run animation
  i_stat_cfg_anime.anim_scheduler_stop = 0;

  // Test for BTN25 (next animation)
  if(i_keyboard_buttons.s.btn25)
  {// If pressed button BTN25
    // Animation stop
    i_stat_cfg_anime.anim_run = 0;

    // Set flag, witch says, that we already read last command
    i_stat_cfg_anime.read_end_of_anim = 1;

    /* And set repeat counter to 0 -> instead of repeating actual animation
     * scheduler will run next animation
     */
    i_anime_repeat_counter = 0;

    // And clear flag for repeating animation (just for case)
    i_play_anim_in_loop=0;
  }



  // Test for BTN26 (previous animation)
  if(i_keyboard_buttons.s.btn26)
  {// If pressed BTN26
    // Animation stop
    i_stat_cfg_anime.anim_run = 0;

    // Set flag, witch says, that we already read last command
    i_stat_cfg_anime.read_end_of_anim = 1;

    // Set animation ID to previous
    i_anime_scheduler_animation_id = i_anime_scheduler_animation_id-2;

    /* And set repeat counter to 0 -> instead of repeating actual animation
     * scheduler will run next animation (well, that is reason why we decreased
     * 2 by i_anime_scheduler_animation_id
     */
    i_anime_repeat_counter = 0;

    // And clear flag for repeating animation (just for case)
    i_play_anim_in_loop = 0;
  }



  // Test for BTN27 (infinite loop)
  if(i_keyboard_buttons.s.btn27)
  {// If pressed BTN27
    if( i_play_anim_in_loop == 0 )
    {// If 0 -> not pressed before
      // Set flag
      i_play_anim_in_loop = 1;

      // Set flag, that says "We already played it all. Just repeat..."
      i_stat_cfg_anime.read_end_of_anim = 1;
    }
    else
    {// Pressed again -> user wants to continue -> play animation to the end
      // Clear flag
      i_play_anim_in_loop = 0;

      // Counter to 0
      i_anime_repeat_counter = 0;
    }
  }

  // Test for infinite loop flag
  if( i_play_anim_in_loop != 0 )
  {// If infinite loop flag is set -> Set "a lots of repeat"
    i_anime_repeat_counter = 0xFF;
  }




  // Test for BTN28 (reset - start from anim0)
  if(i_keyboard_buttons.s.btn28)
  {// If button pressed
    // Set pointer to anime0
    p_anime = anime0;

    // Just for case set "anime run" flag
    i_stat_cfg_anime.anim_run = 1;

    // Clear flag for repeating animation (just for case)
    i_play_anim_in_loop = 0;

    // Set animation scheduler to begin
    i_anime_scheduler_animation_id = 0;
  }




  // Test for BTN29 (jump to MENU)
  if(i_keyboard_buttons.s.btn29)
  {
    // OK, stop animation, backup and clear framebuffer, set PWM
    i_stat_cfg_anime.anim_scheduler_stop = 1;

    // Next time nearly like a new
    i_first_time = 2;

    // Save framebuffer and PWM
    framebuffer_copy(&i_fb_data, &i_fb_backup);
    i_pwm_red   = pwm_get_r();
    i_pwm_green = pwm_get_g();
    i_pwm_blue  = pwm_get_b();

    keyboard_jump_to_main_menu();
  }
}


/*---------------------------------------------------------------------------*/

uint8_t kbrd_draw_change_one_led_state(const uint8_t  i_led,
                                       volatile uint16_t *p_res_column)
{
  // Check input variable and if wrong, show error
  if(i_led >= 5)
  {
    return 1;
  }
  uint8_t i_red;
  uint8_t i_green;
  uint8_t i_blue;

  // Get RGB values from column data word
  i_red   = (((*p_res_column) & (1<<(10 + i_led)))>>10);
  i_green = (((*p_res_column) & (1<<( 5 + i_led)))>>5);
  i_blue  = (((*p_res_column) & (1<<( 0 + i_led)))>>0);

  // Calculate actual status

  // state "none"
  if((i_red   == 0) &&
     (i_green == 0) &&
     (i_blue  == 0))
  {
    // Change to "red"
    i_red   = 1;
    i_green = 0;
    i_blue  = 0;
  }
  // State "red"
  else if((i_red   != 0) &&
          (i_green == 0) &&
          (i_blue  == 0))
  {
    // Change to "green"
    i_red   = 0;
    i_green = 1;
    i_blue  = 0;
  }
  // State "green"
  else if((i_red   == 0) &&
          (i_green != 0) &&
          (i_blue  == 0))
  {
    // Change to "blue"
    i_red   = 0;
    i_green = 0;
    i_blue  = 1;
  }
  // State "blue"
  else if((i_red   == 0) &&
          (i_green == 0) &&
          (i_blue  != 0))
  {
    // Change to "red_green"
    i_red   = 1;
    i_green = 1;
    i_blue  = 0;
  }
  // State "red_green"
  else if((i_red   != 0) &&
          (i_green != 0) &&
          (i_blue  == 0))
  {
    // Change to "red_blue"
    i_red   = 1;
    i_green = 0;
    i_blue  = 1;
  }
  // State "red_blue"
  else if((i_red   != 0) &&
          (i_green == 0) &&
          (i_blue  != 0))
  {
    // Change to "green_blue"
    i_red   = 0;
    i_green = 1;
    i_blue  = 1;
  }
  // State "green_blue"
  else if((i_red   == 0) &&
          (i_green != 0) &&
          (i_blue  != 0))
  {
    // Change to "red_green_blue"
    i_red   = 1;
    i_green = 1;
    i_blue  = 1;
  }
  // State "red_green_blue"
  else if((i_red   != 0) &&
          (i_green != 0) &&
          (i_blue  != 0))
  {
    // Change to "none" (first state)
    i_red   = 0;
    i_green = 0;
    i_blue  = 0;
  }
  else // Unknown combination
  {
    // Never should happen - ERROR
    return 2;
  }

  // OK, now write values back
  if(i_red)
  {
    // We want to light on red LED
    (*p_res_column) |= (1<<(10 + i_led));
  }
  else
  {
    // We want to light off red LED
    (*p_res_column) &= ~(1<<(10 + i_led));
  }

  if(i_green)
  {
    (*p_res_column) |= (1<<(5 + i_led));
  }
  else
  {
    (*p_res_column) &= ~(1<<(5 + i_led));
  }

  if(i_blue)
  {
    (*p_res_column) |= (1<<(0 + i_led));
  }
  else
  {
    (*p_res_column) &= ~(1<<(0 + i_led));
  }
  // No problem detected
  return 0;
}

/*---------------------------------------------------------------------------*/

void keyboard_process_draw(kbrd_btns_t i_keyboard_buttons)
{
  // Keep actual "picture" in memory even if jump out from this mode
  volatile static uint16_t i_fb_backup[5][5];

  // Keep information about set PWM
  static uint8_t i_pwm_r = 0xFF;
  static uint8_t i_pwm_g = 0xFF;
  static uint8_t i_pwm_b = 0xFF;

  // If jump to this mode some initalization procedures have to be done
  static uint8_t i_first_time = 1;

  // Keep information about actual wall
  static uint8_t i_wall = 0;


  if(i_first_time)
  {
    // Restore framebuffer and PWM
    framebuffer_copy(&i_fb_backup, &i_fb_data);
    pwm_set_r(i_pwm_r);
    pwm_set_g(i_pwm_g);
    pwm_set_b(i_pwm_b);

    i_first_time = 0;
  }

  // Column 0 ; LED 0
  if(i_keyboard_buttons.s.btn0)
  {
    kbrd_draw_change_one_led_state(0, &i_fb_data[i_wall][0]);
  }
  // Column 0 ; LED 1
  if(i_keyboard_buttons.s.btn1)
  {
    kbrd_draw_change_one_led_state(1, &i_fb_data[i_wall][0]);
  }
  // Column 0 ; LED 2
  if(i_keyboard_buttons.s.btn2)
  {
    kbrd_draw_change_one_led_state(2, &i_fb_data[i_wall][0]);
  }
  // Column 0 ; LED 3
  if(i_keyboard_buttons.s.btn3)
  {
    kbrd_draw_change_one_led_state(3, &i_fb_data[i_wall][0]);
  }
  // Column 0 ; LED 4
  if(i_keyboard_buttons.s.btn4)
  {
    kbrd_draw_change_one_led_state(4, &i_fb_data[i_wall][0]);
  }
  // Column 1 ; LED 0
  if(i_keyboard_buttons.s.btn5)
  {
    kbrd_draw_change_one_led_state(0, &i_fb_data[i_wall][1]);
  }
  // Column 1 ; LED 1
  if(i_keyboard_buttons.s.btn6)
  {
    kbrd_draw_change_one_led_state(1, &i_fb_data[i_wall][1]);
  }
  // Column 1 ; LED 2
  if(i_keyboard_buttons.s.btn7)
  {
    kbrd_draw_change_one_led_state(2, &i_fb_data[i_wall][1]);
  }
  // Column 1 ; LED 3
  if(i_keyboard_buttons.s.btn8)
  {
    kbrd_draw_change_one_led_state(3, &i_fb_data[i_wall][1]);
  }
  // Column 1 ; LED 4
  if(i_keyboard_buttons.s.btn9)
  {
    kbrd_draw_change_one_led_state(4, &i_fb_data[i_wall][1]);
  }
  // Column 2 ; LED 0
  if(i_keyboard_buttons.s.btn10)
  {
    kbrd_draw_change_one_led_state(0, &i_fb_data[i_wall][2]);
  }
  // Column 2 ; LED 1
  if(i_keyboard_buttons.s.btn11)
  {
    kbrd_draw_change_one_led_state(1, &i_fb_data[i_wall][2]);
  }
  // Column 2 ; LED 2
  if(i_keyboard_buttons.s.btn12)
  {
    kbrd_draw_change_one_led_state(2, &i_fb_data[i_wall][2]);
  }
  // Column 2 ; LED 3
  if(i_keyboard_buttons.s.btn13)
  {
    kbrd_draw_change_one_led_state(3, &i_fb_data[i_wall][2]);
  }
  // Column 2 ; LED 4
  if(i_keyboard_buttons.s.btn14)
  {
    kbrd_draw_change_one_led_state(4, &i_fb_data[i_wall][2]);
  }
  // Column 3 ; LED 0
  if(i_keyboard_buttons.s.btn15)
  {
    kbrd_draw_change_one_led_state(0, &i_fb_data[i_wall][3]);
  }
  // Column 3 ; LED 1
  if(i_keyboard_buttons.s.btn16)
  {
    kbrd_draw_change_one_led_state(1, &i_fb_data[i_wall][3]);
  }
  // Column 3 ; LED 2
  if(i_keyboard_buttons.s.btn17)
  {
    kbrd_draw_change_one_led_state(2, &i_fb_data[i_wall][3]);
  }
  // Column 3 ; LED 3
  if(i_keyboard_buttons.s.btn18)
  {
    kbrd_draw_change_one_led_state(3, &i_fb_data[i_wall][3]);
  }
  // Column 3 ; LED 4
  if(i_keyboard_buttons.s.btn19)
  {
    kbrd_draw_change_one_led_state(4, &i_fb_data[i_wall][3]);
  }
  // Column 4 ; LED 0
  if(i_keyboard_buttons.s.btn20)
  {
    kbrd_draw_change_one_led_state(0, &i_fb_data[i_wall][4]);
  }
  // Column 4 ; LED 1
  if(i_keyboard_buttons.s.btn21)
  {
    kbrd_draw_change_one_led_state(1, &i_fb_data[i_wall][4]);
  }
  // Column 4 ; LED 2
  if(i_keyboard_buttons.s.btn22)
  {
    kbrd_draw_change_one_led_state(2, &i_fb_data[i_wall][4]);
  }
  // Column 4 ; LED 3
  if(i_keyboard_buttons.s.btn23)
  {
    kbrd_draw_change_one_led_state(3, &i_fb_data[i_wall][4]);
  }
  // Column 4 ; LED 4
  if(i_keyboard_buttons.s.btn24)
  {
    kbrd_draw_change_one_led_state(4, &i_fb_data[i_wall][4]);
  }

  // PWM R change
  if(i_keyboard_buttons.s.btn27)
  {
    i_pwm_r = i_pwm_r + kbrd_draw_pwm_step;
    pwm_set_r(i_pwm_r);
  }
  // PWM G change
  if(i_keyboard_buttons.s.btn26)
  {
    i_pwm_g = i_pwm_g + kbrd_draw_pwm_step;
    pwm_set_g(i_pwm_g);
  }
  // PWM B change
  if(i_keyboard_buttons.s.btn25)
  {
    i_pwm_b = i_pwm_b + kbrd_draw_pwm_step;
    pwm_set_b(i_pwm_b);
  }

  // Next wall button
  if(i_keyboard_buttons.s.btn28)
  {
    i_wall++;
    if(i_wall >= 5) i_wall = 0;
  }

  // Go to menu
  if(i_keyboard_buttons.s.btn29)
  {
    // Save framebuffer and PWM values
    framebuffer_copy(&i_fb_data, &i_fb_backup);

    i_pwm_r = pwm_get_r();
    i_pwm_g = pwm_get_g();
    i_pwm_b = pwm_get_b();

    // Next time like a new ;)
    i_first_time = 1;

    keyboard_jump_to_main_menu();
  }
}

//===========================| Low level functions |===========================
/*---------------------------------------------------------------------------*/



kbrd_btns_t keyboard_scan(void)
{
  // Reset i_keyboard_buttons to zero (no pressed button yet)
  kbrd_btns_t i_keyboard_buttons;
  i_keyboard_buttons.i_all_btns = 0;

  // Scan line 0 - data are written to i_keyboard_result (it is big macro)
  kbrd_scan_line(0);

  // Scan line 1
  kbrd_scan_line(1);

  // Line 2
  kbrd_scan_line(2);

  // Line 3
  kbrd_scan_line(3);

  // Line 4
  kbrd_scan_line(4);

  return i_keyboard_buttons;
}





/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/




























