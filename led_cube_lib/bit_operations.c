/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Macros and functions for bit operations in C
 *
 * Some operations are not "easy to write" so there is "easy to use"
 *
*/
#include "bit_operations.h"

