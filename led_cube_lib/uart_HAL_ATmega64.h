/**
 * \file
 *
 * \brief HAL of UART for ATmega64
 *
 * Created:  2015/06/07\n
 * Modified: 2015/06/07
 *
 * \author Martin Stejskal
 *
 * \todo Add support for UART0 (now only UART1)
 */

#ifndef _UART_HAL_ATmega64_library_
#define _UART_HAL_ATmega64_library_
//================================| Includes |=================================
#include <inttypes.h>

// Simply, effective and flexible operations with PINs
#include "bit_operations.h"

// Get F_CPU value
#include "../rgb_led_cube_5x5x5_settings.h"

//=================================| Defines |=================================
/**
 * \brief Baudrate of UART module.
 *
 * Default value should be 115200
 */
#define UART_HAL_BAUDRATE       115200
//=================================| Macros |==================================

#define UBRR_val        ((F_CPU/(8*UART_HAL_BAUDRATE))-1)

//===============================| Structures |================================

/**
 * \brief Register image
 */
typedef struct{
  union{
    struct{
      uint8_t MPCMn :1;     // LSB (Little endian)
      uint8_t U2Xn  :1;
      uint8_t UPEn  :1;
      uint8_t DORn  :1;
      uint8_t FEn   :1;
      uint8_t UDREn :1;
      uint8_t TXCn  :1;
      uint8_t RXCn  :1;     // MSB (Little endian)
    }s_UCSRnA;
    uint8_t UCSRnA;
  };

  union{
    struct{
      uint8_t TXB8n       :1;
      uint8_t RXB8n       :1;
      uint8_t UCSZn2      :1;
      uint8_t TXENn       :1;
      uint8_t RXENn       :1;
      uint8_t UDRIEn      :1;
      uint8_t TXICEn      :1;
      uint8_t RXICEn      :1;
    }s_UCSRnB;
    uint8_t UCSRnB;
  };

  union{
    struct{
      uint8_t UCPOLn      :1;
      uint8_t UCSZn       :2;
      uint8_t USBSn       :1;
      uint8_t UPMn        :2;
      uint8_t UMSELn      :1;
    }s_UCSRnC;
    uint8_t UCSRnC;
  };

  union{
    uint16_t UBRRn;
    struct{
      uint8_t UBRRnL;
      uint8_t UBRRnH;
    }s_UBRRn;
  };

}uart_HAL_t;

/**
 *
 * \brief Error codes for UART HAL layer
 *
 */
typedef enum{
  UART_HAL_OK = 0,
  UART_HAL_FAIL = 1,
  UART_HAL_TX_NOT_COMPLETE = 2,
  UART_HAL_RX_NOT_COMPLETE = 3,
  UART_HAL_RX_DATA_OVERRUN = 4,
  UART_HAL_RX_PARITY_ERR = 5,
}uart_HAL_res;


//===========================| Function prototypes |===========================

/**
 * \brief Function that prepare UART hardware
 * @return UART_HAL_OK (0) if everything is OK
 */
uart_HAL_res uart_HAL_init(void);

/**
 * \brief Recieve 1 Byte from UART
 * @param p_i_rx_Byte Pointer to memory, where data will be written
 * @return UART_HAL_OK (0) if everything is OK
 */
uart_HAL_res uart_HAL_rx_Byte(uint8_t *p_i_rx_Byte);

/**
 * \brief Send 1 Byte over UART
 * @param i_tx_Byte Data Byte which will be send
 * @return UART_HAL_OK (0) if everything is OK
 */
uart_HAL_res uart_HAL_tx_Byte(uint8_t i_tx_Byte);

#endif
