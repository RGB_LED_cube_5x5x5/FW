/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Library for system clock (used timer)
 *
 * Sometimes is needed reference timer for some functions. For example if\n
 * there is keyboard scan. This is not necessary to run every ms, but every\n
 * 100 ms. Delay functions are not much effective, because when waiting MCU\n
 * can not do anything else.
 *
 * Created:  2013/08/14\n
 * Modified: 2013/08/14
 */


#include "sys_timer.h"



void init_sys_timer(void)
{
  // We will use 16 bit timer1 -> Normal timer operation
  TCCR1A =
      (0<<COM1A1)|(0<<COM1A0)|
      (0<<COM1B1)|(0<<COM1B0)|
      (0<<COM1C1)|(0<<COM1C0)|
      (0<<WGM11) |(0<<WGM10) ;

  // Set prescaller to 256 -> overflow approximately every 1.049 s
  TCCR1B =
      (0<<WGM13) |(0<<WGM12) |
      (1<<CS12)  |(0<<CS11)  |(0<<CS10);

  // Do not force compare
  TCCR1C =
      (0<<FOC1A) |(0<<FOC1B) |(0<<FOC1C);

  // Turn off all interrupts from timer 1
  TIMSK = TIMSK & ~( (1<<TICIE1)|(1<<OCIE1A)|(1<<OCIE1B)|(1<<TOIE1) );
}
