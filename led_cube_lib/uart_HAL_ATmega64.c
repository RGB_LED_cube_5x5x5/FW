/**
 * \file
 *
 * \brief HAL of UART for ATmega64
 *
 * Created:  2015/06/07\n
 * Modified: 2015/06/23
 *
 * \author Martin Stejskal
 */

#include "uart_HAL_ATmega64.h"

//===========================| Internal functions |============================
/**
 * \brief Just read registers and copy them to structure
 * @return Image of UART registers in structure
 */
inline uart_HAL_t uart_HAL_read_registers(void)
{
  uart_HAL_t s_reg_img;

  s_reg_img.UCSRnA = UCSR1A;
  s_reg_img.UCSRnB = UCSR1B;
  s_reg_img.UCSRnC = UCSR1C;
  s_reg_img.s_UBRRn.UBRRnL = UBRR1L;
  s_reg_img.s_UBRRn.UBRRnH = UBRR1H;

  return s_reg_img;
}

/**
 * \brief Data in register image structure write to registers
 * @param s_reg_img Image of UART registers in structure
 */
inline void uart_HAL_write_registers(uart_HAL_t s_reg_img)
{
  UCSR1A = s_reg_img.UCSRnA;
  UCSR1B = s_reg_img.UCSRnB;
  UCSR1C = s_reg_img.UCSRnC;
  UBRR1L = s_reg_img.s_UBRRn.UBRRnL;
  UBRR1H = s_reg_img.s_UBRRn.UBRRnH;
}

/**
 * \brief Create register structure with default values
 * @return Register image structure for UART
 */
inline uart_HAL_t uart_HAL_get_default_reg_img(void)
{
  uart_HAL_t s_reg_img;
  s_reg_img.UCSRnA = 0;
  s_reg_img.UCSRnB = 0;
  s_reg_img.UCSRnC = 0;
  s_reg_img.UBRRn = 0;

  return s_reg_img;
}
//================================| Functions |================================

uart_HAL_res uart_HAL_init(void)
{
  // Get "clear" settings
  uart_HAL_t s_reg_img = uart_HAL_get_default_reg_img();

  // Reconfigure UART
  // Double transmission speed
  s_reg_img.s_UCSRnA.U2Xn = 1;

  // Enable RX module
  s_reg_img.s_UCSRnB.RXENn = 1;
  // Enable TX module
  s_reg_img.s_UCSRnB.TXENn = 1;

  // We want to use 8 bit communication
  s_reg_img.s_UCSRnB.UCSZn2 = 0;
  s_reg_img.s_UCSRnC.UCSZn  = 0b11;

  // Set prescaller (already calculated by preprocessor)
  s_reg_img.UBRRn = UBRR_val;

  // Write data to register
  uart_HAL_write_registers(s_reg_img);

  return UART_HAL_OK;
}

/*---------------------------------------------------------------------------*/

inline uart_HAL_res uart_HAL_rx_Byte(uint8_t *p_i_rx_Byte)
{
  // Status variable - set default value
  uart_HAL_res e_status = UART_HAL_OK;

  // Read registers
  uart_HAL_t s_reg_img = uart_HAL_read_registers();

  // Check if receive is complete
  if(s_reg_img.s_UCSRnA.RXCn == 0)
  {
    // Still not complete
    return UART_HAL_RX_NOT_COMPLETE;
  }

  // Check for parity error
  if(s_reg_img.s_UCSRnA.UPEn != 0)
  {
    // Read dummy data
    uint8_t i_dummy = UDR1;
    i_dummy++;  // To avoid warning from compiler
    return UART_HAL_RX_PARITY_ERR;
  }

  // Check if overrun occurs
  if(s_reg_img.s_UCSRnA.DORn != 0)
  {
    // Well overrun occurs, but we are able to read data...
    e_status = UART_HAL_RX_DATA_OVERRUN;
  }

  // It looks like, there is no problem.
  *p_i_rx_Byte = UDR1;
  // Check for another errors
  return e_status;
}

/*---------------------------------------------------------------------------*/

inline uart_HAL_res uart_HAL_tx_Byte(uint8_t i_tx_Byte)
{
  // Read registers
  uart_HAL_t s_reg_img = uart_HAL_read_registers();

  // Check if TX register is ready
  if(s_reg_img.s_UCSRnA.UDREn == 0)
  {
    // Module is busy
    return UART_HAL_TX_NOT_COMPLETE;
  }
  else
  {
    // TX module is ready to go
    UDR1 = i_tx_Byte;
    return UART_HAL_OK;
  }
  // This never, ever should happen
  return UART_HAL_FAIL;
}




