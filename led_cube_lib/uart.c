/**
 * \file
 *
 * \brief Driver for UART
 *
 * This driver is optimized for LED cube project and it is NOT universal\n
 * driver for AVR! It is strictly written for ATmega64!\n
 * Please keep this on mind and when you decide to change anything be sure\n
 * you also changed it here.
 *
 * Created: 2015/06/02\n
 * Updated: 2016/08/15
 *
 * \author Martin Stejskal
 */

#include "uart.h"


//============================| Global variables |=============================
/**
 * @brief Keep useful informations for RX/TX tasks
 */
static uart_tasks_t tasks_s;

static uint16_t i_max_str_len = UART_DEFALT_STR_RX_BUFFER_SIZE;
//================================| Functions |================================

//===========================| Non user functions |============================
static void uart_tx_data_subtask(void)
{
  uart_res e_status;

  // Check actual state
  if(tasks_s.tx_data.state == UART_WAITING_FOR_REQUEST)
  {
    // Waiting for request, or job is done - nothing to do here
    return;
  }
  // Else we are doing processing
  /* Try to send one Byte. If not successful, then just try it next time.
   * Usually failure is caused by "busy" flag of HW.
   */
  e_status = uart_tx_Byte(*tasks_s.tx_data.p_i_data);
  if(e_status == UART_OK)
  {
    // Success - increase pointer, decrease Byte counter
    tasks_s.tx_data.p_i_data++;
    tasks_s.tx_data.i_data_length--;
    // Check if we run out of data
    if(tasks_s.tx_data.i_data_length == 0)
    {
      // OK, we are out of data. All send. Set status
      tasks_s.tx_data.state = UART_WAITING_FOR_REQUEST;
    }
  }
}

static void uart_tx_str_subtask(void)
{
  uart_res e_status;

  // Check actual state
  if(tasks_s.tx_str.state == UART_WAITING_FOR_REQUEST)
  {
    // Waiting for request or job is done.
    return;
  }

  // Else let's do something
  e_status = uart_tx_Byte(*tasks_s.tx_str.p_i_data);
  if(e_status == UART_OK)
  {
    // Success, check for NULL character (last one)
    if(*tasks_s.tx_str.p_i_data == 0x00)
    {
      // Last Byte received. Job is done
      tasks_s.tx_str.state = UART_WAITING_FOR_REQUEST;
      return;
    }
    // Else just increase pointer and receive next character
    tasks_s.tx_str.p_i_data++;
  }
}


static void uart_rx_data_subtask(void)
{
  uart_res e_status;

  // Check actual state
  if(tasks_s.rx_data.state == UART_WAITING_FOR_REQUEST)
  {
    // Waiting for request, or job is done - nothing to do here
    return;
  }
  // Else we are doing processing
  /* Try to send one Byte. If not successful, then just try it next time.
   * Usually failure is caused by "busy" flag of HW.
   */
  e_status = uart_rx_Byte(tasks_s.rx_data.p_i_data);
  if(e_status == UART_OK)
  {
    // Success - increase pointer, decrease Byte counter
    tasks_s.rx_data.p_i_data++;
    tasks_s.rx_data.i_data_length--;
    // Check if we run out of data
    if(tasks_s.rx_data.i_data_length == 0)
    {
      // OK, we are out of data. All send. Set status
      tasks_s.rx_data.state = UART_WAITING_FOR_REQUEST;
    }
  }
}

static void uart_rx_str_subtask(void)
{
  uart_res e_status;

  // Check actual state
  if(tasks_s.rx_str.state == UART_WAITING_FOR_REQUEST)
  {
    // Waiting for request or job is done.
    return;
  }
  // Else let's do something
  e_status = uart_rx_Byte(tasks_s.rx_str.p_i_data);
  if(e_status == UART_OK)
  {
    // Success, check for NULL character (last one)
    if(*tasks_s.rx_str.p_i_data == 0x00)
    {
      // Last Byte received. Job is done. Reset counter as well
      tasks_s.rx_str.i_data_length = 0;
      tasks_s.rx_str.state = UART_WAITING_FOR_REQUEST;
      return;
    }
    // Else just increase pointer and receive next character
    tasks_s.rx_str.p_i_data++;
    tasks_s.rx_str.i_data_length++;

    // Check data length
    if(tasks_s.rx_str.i_data_length >= i_max_str_len){
      /* Well, we got problem. To avoid re-writing buffer, let's move pointer
       * back to ground zero
       */
      tasks_s.rx_str.p_i_data = tasks_s.rx_str.p_i_data -
                                tasks_s.rx_str.i_data_length;

      // And start counting from zero
      tasks_s.rx_str.i_data_length = 0;
    }
  }
}
//==========================| High level functions |===========================
/**
 * \brief Initialize HW
 * @return UART_OK (0) if no problem detected
 */
uart_res uart_init(void)
{
  // Status variable
  uart_res e_status;

  // Clear task variable
  tasks_s.rx_data.i_data_length = 0;
  tasks_s.rx_data.p_i_data = 0;
  tasks_s.rx_data.state = UART_WAITING_FOR_REQUEST;
  // And just copy whole structures
  tasks_s.rx_str  = tasks_s.rx_data;
  tasks_s.tx_data = tasks_s.rx_data;
  tasks_s.tx_str  = tasks_s.rx_data;

  // Initialize HW
  e_status = uart_HAL_init();

  return e_status;
}

/**
 * \brief Task function
 *
 * In some cases it is needed to run multiple tasks. So just periodically\n
 * call this function and use uart_request_* functions. All processing will\n
 * be done automatically.\n
 * Or just use low level functions. It is up to you.
 */
void uart_task(void)
{
  /* This code have to be there because optimizer is crazy and if there are
   * only functions, then just wipe this function. Do not understand why, but
   * with this trick with counter it works
   */
  static uint8_t i_cnt = 0;
  i_cnt++;

  // Just do many small tasks
  uart_tx_data_subtask();
  uart_tx_str_subtask();
  uart_rx_data_subtask();
  uart_rx_str_subtask();
}

//=========================| Middle level functions |==========================
/**
 * \brief By this function you can check if driver is ready for next task
 *
 * Test if is possible to call uart_request_rx_data() again
 * @param p_i_status Address where result will be written. 0 means: not ready\n
 *        yet, 1 means: you can call uart_request_rx_data()
 * @return UART_OK (0) if no problem detected.
 */
inline uart_res uart_request_tx_data_ready(uint8_t *p_i_status)
{
  // Just for case, that someone will call this function in loop to avoid
  // system freeze
  uart_tx_str_subtask();
  uart_tx_data_subtask();

  if((tasks_s.tx_str.state  == UART_WAITING_FOR_REQUEST) &&
     (tasks_s.tx_data.state == UART_WAITING_FOR_REQUEST))
    *p_i_status = 1;
  else
    *p_i_status = 0;

  return UART_OK;
}

/**
 * \brief By this function you can check if driver is ready for next task
 *
 * Test if is possible to call uart_request_tx_data() again
 * @param p_i_status Address where result will be written. 0 means: not ready\n
 *        yet, 1 means: you can call uart_request_tx_data()
 * @return UART_OK (0) if no problem detected.
 */
inline uart_res uart_request_rx_data_ready(uint8_t *p_i_status)
{
  // Just for case, that someone will call this function in loop to avoid
  // system freeze
  uart_rx_str_subtask();
  uart_rx_data_subtask();

  if((tasks_s.rx_str.state  == UART_WAITING_FOR_REQUEST) &&
     (tasks_s.rx_data.state == UART_WAITING_FOR_REQUEST))
    *p_i_status = 1;
  else
    *p_i_status = 0;

  return UART_OK;
}


/**
 * \brief By this function you can check if driver is ready for next task
 *
 * Test if is possible to call uart_request_tx_str() again
 * @param p_i_status Address where result will be written. 0 means: not ready\n
 *        yet, 1 means: you can call uart_request_tx_str()
 * @return UART_OK (0) if no problem detected.
 */
inline uart_res uart_request_tx_str_ready(uint8_t *p_i_status)
{
  // Just for case, that someone will call this function in loop to avoid
  // system freeze
  uart_tx_str_subtask();
  uart_tx_data_subtask();

  if((tasks_s.tx_str.state  == UART_WAITING_FOR_REQUEST) &&
     (tasks_s.tx_data.state == UART_WAITING_FOR_REQUEST))
    *p_i_status = 1;
  else
    *p_i_status = 0;

  return UART_OK;
}

/**
 * \brief By this function you can check if driver is ready for next task
 *
 * Test if is possible to call uart_request_rx_str() again
 * @param p_i_status Address where result will be written. 0 means: not ready\n
 *        yet, 1 means: you can call uart_request_rx_str()
 * @return UART_OK (0) if no problem detected.
 */
inline uart_res uart_request_rx_str_ready(uint8_t *p_i_status)
{
  // Just for case, that someone will call this function in loop to avoid
  // system freeze
  uart_rx_str_subtask();
  uart_rx_data_subtask();

  if((tasks_s.rx_str.state  == UART_WAITING_FOR_REQUEST) &&
     (tasks_s.rx_data.state == UART_WAITING_FOR_REQUEST))
    *p_i_status = 1;
  else
    *p_i_status = 0;

  return UART_OK;
}

/**
 * \brief Send request for sending data.
 *
 * This is only request. Processing will be done during calling uart_task()\n
 * function.
 * @param p_i_data Pointer to data array.
 * @param i_length Number of Bytes which you want to send. 0 is invalid value
 * @return UART_OK (0) if no problem detected. UART_FAIL (1) when currently\n
 *         doing this task. UART_INVALID_PARAM (3) when invalid parameter\n
 *         given.
 */
uart_res uart_request_tx_data(uint8_t *p_i_data, uint16_t i_length)
{
  // Just give parameters to structure if not in progress
  // Get status
  uint8_t i_ready;
  uart_request_tx_data_ready(&i_ready);

  // Just give parameters to structure if not in progress
  if(i_ready == 0)
  {
    return UART_BUSY;
  }

  if(i_length == 0)
  {
    return UART_INVALID_PARAM;
  }

  tasks_s.tx_data.p_i_data = p_i_data;
  tasks_s.tx_data.i_data_length = i_length;
  // And from now we are working on it
  tasks_s.tx_data.state = UART_DOING;

  return UART_OK;
}

/**
 * \brief Send request for receiving data
 *
 * This is only request. Processing will be done during calling uart_task()\n
 * function
 * @param p_i_data Pointer to memory where received data will be written
 * @param i_length Number of Byter which should be received. 0 is invalid value
 * @return UART_OK (0) if no problem detected. UART_FAIL (1) when currently\n
 *         doing this task. UART_INVALID_PARAM (3) when invalid parameter\n
 *         given.
 */
uart_res uart_request_rx_data(uint8_t *p_i_data, uint16_t i_length)
{
  // Just give parameters to structure if not in progress
  // Get status
  uint8_t i_ready;
  uart_request_rx_data_ready(&i_ready);

  // Just give parameters to structure if not in progress
  if(i_ready == 0)
  {
    // Currently working on it. Hold on
    return UART_BUSY;
  }

  if(i_length == 0)
  {
    return UART_INVALID_PARAM;
  }

  tasks_s.rx_data.p_i_data = p_i_data;
  tasks_s.rx_data.i_data_length = i_length;
  // And from now we are working on it
  tasks_s.rx_data.state = UART_DOING;

  return UART_OK;
}




/**
 * \brief Send request for sending string.
 *
 * This is only request. Processing will be done during calling uart_task()\n
 * function.
 * @param p_c_data Pointer to string array.
 * @return UART_OK (0) if no problem detected. UART_FAIL (1) when currently\n
 *         doing this task.
 */
uart_res uart_request_tx_str(char *p_c_data)
{
  // Get status
  uint8_t i_ready;
  uart_request_tx_str_ready(&i_ready);

  // Just give parameters to structure if not in progress
  if(i_ready == 0)
  {
    // Not ready so far
    return UART_BUSY;
  }

  tasks_s.tx_str.p_i_data = (uint8_t*)p_c_data;
  // And from now we are working on it
  tasks_s.tx_str.state = UART_DOING;

  return UART_OK;
}

/**
 * \brief Send request for receiving string
 *
 * This is only request. Processing will be done during calling uart_task()\n
 * function
 * @param p_c_data Pointer to memory where received string will be written
 * @return UART_OK (0) if no problem detected. UART_FAIL (1) when currently\n
 *         doing this task.
 */
uart_res uart_request_rx_str(char *p_c_data)
{
  // Just give parameters to structure if not in progress
  // Get status
  uint8_t i_ready;
  uart_request_rx_str_ready(&i_ready);

  // Just give parameters to structure if not in progress
  if(i_ready == 0)
  {
    // Currently working on it. Hold on
    return UART_BUSY;
  }

  tasks_s.rx_str.p_i_data = (uint8_t*)p_c_data;

  // And from now we are working on it
  tasks_s.rx_str.state = UART_DOING;

  return UART_OK;
}

//===========================| Low level functions |===========================
inline uart_res uart_set_max_str_lenght(uint16_t i_max_len){
  i_max_str_len = i_max_len;
  return UART_OK;
}

/**
 * \brief Primitive function for sending string
 *
 * Send data until NULL character is found. This is blocking function, so\n
 * beware!
 * @param p_c_txt Pointer to first character of string
 * @return UART_OK (0) if no problem detected
 */
uart_res uart_tx_str(char *p_c_txt)
{
  // Timeout counter
  uint32_t i_timeout_cnt=0;

  // Send data until 0x00 (NULL) is found
  while(*p_c_txt != 0x00)
  {
    // Send Byte by Byte, but we have to check if data was really send or not
    if(uart_tx_Byte(*p_c_txt) == 0)
    {
      // If error code is 0 -> OK -> let's move to next character
      p_c_txt++;
      i_timeout_cnt = 0;
    }
    else
    {
      // Error detected. Just increase counter and check value
      i_timeout_cnt++;
      if(i_timeout_cnt > UART_TIMEOUT_CYCLES)
      {
        return UART_TIMEOUT;
      }
    }
  }

  return UART_OK;
}

/**
 * \brief Just send one Byte
 * @param i_data This Byte will be send
 * @return UART_OK (0) if no problem detected
 */
inline uart_res uart_tx_Byte(uint8_t i_data)
{
  // Check if there is any problem
  if(uart_HAL_tx_Byte(i_data))
  {
    // If some error occurs
    return UART_FAIL;
  }

  return UART_OK;
}

/**
 * \brief Just receive one Byte
 * @param p_i_data Address where recieved Byte will be written
 * @return UART_OK (0) if no problem detected
 */
inline uart_res uart_rx_Byte(uint8_t *p_i_data)
{
  // Check if there is any problem
  if(uart_HAL_rx_Byte(p_i_data))
  {
    // If some error occurs
    return UART_FAIL;
  }

  return UART_OK;
}
