/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Framebuffer for RGB LED cube 5x5x5
 *
 * Framebuffer display once image on LED cube. As parameter of framebuffer is\n
 * pointer to image in SRAM memory. Framebuffer disable global interrupt\n
 * however enable it when is needed some delay, so in this "time spaces" MCU\n
 * can to other stuff.
 *
 * Created  2013/05/18\n
 * Modified 2015/07/05
*/

#include "framebuffer.h"

/*-----------------------------------------*
 |             Global variables            |
 *-----------------------------------------*/
// Usually needed because of interrupt functions
/**
 * \brief Inform framebuffer, that is time to change active column
 */
volatile uint8_t i_column_delay_time_out = 0;

/**
 * \brief Measure time between change of two columns
 *
 * When data for LEDs are loaded on bus, they can (should) stay there\n
 * for some time. When time column_delay pass, then in this interrupt is set\n
 * variable i_column_delay_time_out to 1 -> signalize, that is time to\n
 * change active column
 */
ISR(TIMER0_COMP_vect)
{
  i_column_delay_time_out = 1;
}


void init_framebuffer(void)
{
  // Data - Red color (output, set to 0)
  io_set_dir_out( D_R0_PORT, D_R0_pin );
  io_set_0(       D_R0_PORT, D_R0_pin );
  io_set_dir_out( D_R1_PORT, D_R1_pin );
  io_set_0(       D_R1_PORT, D_R1_pin );
  io_set_dir_out( D_R2_PORT, D_R2_pin );
  io_set_0(       D_R2_PORT, D_R2_pin );
  io_set_dir_out( D_R3_PORT, D_R3_pin );
  io_set_0(       D_R3_PORT, D_R3_pin );
  io_set_dir_out( D_R4_PORT, D_R4_pin );
  io_set_0(       D_R4_PORT, D_R4_pin );

  // Data - Green color (output, set to 0)
  io_set_dir_out( D_G0_PORT, D_G0_pin );
  io_set_0(       D_G0_PORT, D_G0_pin );
  io_set_dir_out( D_G1_PORT, D_G1_pin );
  io_set_0(       D_G1_PORT, D_G1_pin );
  io_set_dir_out( D_G2_PORT, D_G2_pin );
  io_set_0(       D_G2_PORT, D_G2_pin );
  io_set_dir_out( D_G3_PORT, D_G3_pin );
  io_set_0(       D_G3_PORT, D_G3_pin );
  io_set_dir_out( D_G4_PORT, D_G4_pin );
  io_set_0(       D_G4_PORT, D_G4_pin );

  // Data - Blue color (output, set to 0)
  io_set_dir_out( D_B0_PORT, D_B0_pin );
  io_set_0(       D_B0_PORT, D_B0_pin );
  io_set_dir_out( D_B1_PORT, D_B1_pin );
  io_set_0(       D_B1_PORT, D_B1_pin );
  io_set_dir_out( D_B2_PORT, D_B2_pin );
  io_set_0(       D_B2_PORT, D_B2_pin );
  io_set_dir_out( D_B3_PORT, D_B3_pin );
  io_set_0(       D_B3_PORT, D_B3_pin );
  io_set_dir_out( D_B4_PORT, D_B4_pin );
  io_set_0(       D_B4_PORT, D_B4_pin );


  // Wall - all output, set to 0
  io_set_dir_out( WALL0_PORT, WALL0_pin );
  io_set_0(       WALL0_PORT, WALL0_pin );
  io_set_dir_out( WALL1_PORT, WALL1_pin );
  io_set_0(       WALL1_PORT, WALL1_pin );
  io_set_dir_out( WALL2_PORT, WALL2_pin );
  io_set_0(       WALL2_PORT, WALL2_pin );
  io_set_dir_out( WALL3_PORT, WALL3_pin );
  io_set_0(       WALL3_PORT, WALL3_pin );
  io_set_dir_out( WALL4_PORT, WALL4_pin );
  io_set_0(       WALL4_PORT, WALL4_pin );

  // Column - all output, set to "0"
  io_set_dir_out( COLUMN0_PORT, COLUMN0_pin );
  io_set_0(       COLUMN0_PORT, COLUMN0_pin );
  io_set_dir_out( COLUMN1_PORT, COLUMN1_pin );
  io_set_0(       COLUMN1_PORT, COLUMN1_pin );
  io_set_dir_out( COLUMN2_PORT, COLUMN2_pin );
  io_set_0(       COLUMN2_PORT, COLUMN2_pin );
  io_set_dir_out( COLUMN3_PORT, COLUMN3_pin );
  io_set_0(       COLUMN3_PORT, COLUMN3_pin );
  io_set_dir_out( COLUMN4_PORT, COLUMN4_pin );
  io_set_0(       COLUMN4_PORT, COLUMN4_pin );


  /* OK, now prepare Timer0
   * CTC mode
   * Normal pin operation
   * 64x prescaler -> 4us/cycle
   */
  TCCR0 = (1<<WGM01)|(0<<WGM00)|
          (0<<COM01)|(0<<COM00)|
          (1<<CS02) |(0<<CS01) |(0<<CS00);
  // Calculate OCR0 -> 4us/cycle -> *1/4 -> in 1us
  OCR0 = (column_delay * 0.25);

  // Interrupt vectors - when TCNT0 == OCR0
  TIMSK |= (1<<OCIE0);
}



/*---------------------------------------------------------------------------*/

void framebuffer(volatile uint16_t (*p_fb_data)[5][5])
{
  uint8_t i_data_r;     // For temporary "red color"
  uint8_t i_data_g;     // For temporary "green color"
  uint8_t i_data_b;     // For temporary "blue color"

  for(uint8_t i_wall=0 ; i_wall<5 ; i_wall++)
  {
    // Before change set all LEDs to off
    ///\todo optimalize for speed
    load_DATA_RED_on_bus(0);
    load_DATA_GREEN_on_bus(0);
    load_DATA_BLUE_on_bus(0);

    // Set active "wall"
    set_active_wall( i_wall );

    for(uint8_t i_column=0 ; i_column<5 ; i_column++)
    {
      // Get data for red color
      i_data_r = ((*p_fb_data)[i_wall][i_column] >> 10) & (0b00011111);

      // Get data for green color
      i_data_g = ((*p_fb_data)[i_wall][i_column] >> 5)  & (0b00011111);

      // Get data for blue color
      i_data_b = ((*p_fb_data)[i_wall][i_column] >> 0)  & (0b00011111);

      // Before change set all LEDs to off
      load_DATA_RED_on_bus(0);
      load_DATA_GREEN_on_bus(0);
      load_DATA_BLUE_on_bus(0);

      // Set active "column"
      set_active_column( i_column );

      // And show data
      load_DATA_RED_on_bus( i_data_r );
      load_DATA_GREEN_on_bus( i_data_g );
      load_DATA_BLUE_on_bus( i_data_b );

      // Clear i_column_delay_time_out - still not timed out :)
      i_column_delay_time_out = 0;

      TCNT0 = 0;        // Reset timer

      /* Now there will be some time space between two "COLUMNS" -> call some
       * functions
       */
      while( i_column_delay_time_out == 0 )
      {
        column_space_function;
        enable_isr_for_while();
      }
    }
  }
}

/*---------------------------------------------------------------------------*/
uint8_t framebuffer_add_dot(volatile uint16_t (*p_fb_data)[5][5],
                                uint8_t i_wall,
                                uint8_t i_column,
                                uint8_t i_led,
                                uint8_t i_red,
                                uint8_t i_green,
                                uint8_t i_blue)
{
  // Check input parameters
  if((i_wall >= 5) ||
     (i_column >= 5) ||
     (i_led >= 5)
    )
  {
    // Wrong input parameter. No fun today...
    return 1;
  }
  // Else parameters are OK. Let's add dot
  uint16_t i_bin_data = 0;
  // Basically we only need to fill correctly 16 bit value in framebuffer.
  if(i_red)
  {
    // We want red LED on. 1 shift by 10 + LED value
    i_bin_data |= 1<<(10 + i_led);
  }
  if(i_green)
  {
    i_bin_data |= 1<<(5 + i_led);
  }
  if(i_blue)
  {
    i_bin_data |= 1<<(0 + i_led);
  }

  // OK, now just add these bits to FB. But first delete previous dot (if any)
  (*p_fb_data)[i_wall][i_column] &= ~((1<<(10+i_led)) | (1<<(5+i_led)) |
                                    (1<<(0 +i_led))
                                   );

  (*p_fb_data)[i_wall][i_column] |= i_bin_data;

  return 0;
}

/*---------------------------------------------------------------------------*/
inline void framebuffer_clean(volatile uint16_t (*p_fb_data)[5][5])
{
  uint8_t i;
  uint8_t j;

  for(i=0 ; i<5 ; i++)
  {
    for(j=0 ; j<5 ; j++)
    {
      // Belive or not, but collums are VERY importnant ;)
      (*p_fb_data)[i][j]=0;
    }
  }
}

inline void framebuffer_copy(volatile uint16_t (*p_fb_src)[5][5],
                             volatile uint16_t (*p_fb_dst)[5][5])
{
  uint8_t i, j;

  for(i=0 ; i<5 ; i++)
  {
    for(j=0 ; j<5 ; j++)
    {
      (*p_fb_dst)[i][j] = (*p_fb_src)[i][j];
    }
  }
}

/*---------------------------------------------------------------------------*/

inline void set_active_wall( uint8_t i_wall_number )
{
  switch( i_wall_number )
  {
    case 0:     // "Wall" is activated by low level
      io_set_L( WALL0_PORT, WALL0_pin );        // Enable WALL0
      io_set_H( WALL1_PORT, WALL1_pin );        // Disable WALL1
      io_set_H( WALL2_PORT, WALL2_pin );        // Disable WALL2
      io_set_H( WALL3_PORT, WALL3_pin );        // Disable WALL3
      io_set_H( WALL4_PORT, WALL4_pin );        // Disable WALL4
      break;
    case 1:     // "Wall" is activated by low level
      io_set_H( WALL0_PORT, WALL0_pin );        // Disable WALL0
      io_set_L( WALL1_PORT, WALL1_pin );        // Enable WALL1
      io_set_H( WALL2_PORT, WALL2_pin );        // Disable WALL2
      io_set_H( WALL3_PORT, WALL3_pin );        // Disable WALL3
      io_set_H( WALL4_PORT, WALL4_pin );        // Disable WALL4
      break;
    case 2:     // "Wall" is activated by low level
      io_set_H( WALL0_PORT, WALL0_pin );        // Disable WALL0
      io_set_H( WALL1_PORT, WALL1_pin );        // Disable WALL1
      io_set_L( WALL2_PORT, WALL2_pin );        // Enable WALL2
      io_set_H( WALL3_PORT, WALL3_pin );        // Disable WALL3
      io_set_H( WALL4_PORT, WALL4_pin );        // Disable WALL4
      break;
    case 3:     // "Wall" is activated by low level
      io_set_H( WALL0_PORT, WALL0_pin );        // Disable WALL0
      io_set_H( WALL1_PORT, WALL1_pin );        // Disable WALL1
      io_set_H( WALL2_PORT, WALL2_pin );        // Disable WALL2
      io_set_L( WALL3_PORT, WALL3_pin );        // Enable WALL3
      io_set_H( WALL4_PORT, WALL4_pin );        // Disable WALL4
      break;
    case 4:     // "Wall" is activated by low level
      io_set_H( WALL0_PORT, WALL0_pin );        // Disable WALL0
      io_set_H( WALL1_PORT, WALL1_pin );        // Disable WALL1
      io_set_H( WALL2_PORT, WALL2_pin );        // Disable WALL2
      io_set_H( WALL3_PORT, WALL3_pin );        // Disable WALL3
      io_set_L( WALL4_PORT, WALL4_pin );        // Enable WALL4
      break;
    default :    // Disable all
      io_set_H( WALL0_PORT, WALL0_pin );        // Disable WALL0
      io_set_H( WALL1_PORT, WALL1_pin );        // Disable WALL1
      io_set_H( WALL2_PORT, WALL2_pin );        // Disable WALL2
      io_set_H( WALL3_PORT, WALL3_pin );        // Disable WALL3
      io_set_H( WALL4_PORT, WALL4_pin );        // Disable WALL4
  }
}


/*---------------------------------------------------------------------------*/


inline void set_active_column( uint8_t i_column_number )
{
  switch( i_column_number )
  {
  case 0:       // "Column" is activated by low level
    io_set_L( COLUMN0_PORT, COLUMN0_pin );      // Enable COLUMN0
    io_set_H( COLUMN1_PORT, COLUMN1_pin );      // Disable COLUMN1
    io_set_H( COLUMN2_PORT, COLUMN2_pin );      // Disable COLUMN2
    io_set_H( COLUMN3_PORT, COLUMN3_pin );      // Disable COLUMN3
    io_set_H( COLUMN4_PORT, COLUMN4_pin );      // Disable COLUMN4
    break;
  case 1:       // "Column" is activated by low level
    io_set_H( COLUMN0_PORT, COLUMN0_pin );      // Disable COLUMN0
    io_set_L( COLUMN1_PORT, COLUMN1_pin );      // Enable COLUMN1
    io_set_H( COLUMN2_PORT, COLUMN2_pin );      // Disable COLUMN2
    io_set_H( COLUMN3_PORT, COLUMN3_pin );      // Disable COLUMN3
    io_set_H( COLUMN4_PORT, COLUMN4_pin );      // Disable COLUMN4
    break;
  case 2:       // "Column" is activated by low level
    io_set_H( COLUMN0_PORT, COLUMN0_pin );      // Disable COLUMN0
    io_set_H( COLUMN1_PORT, COLUMN1_pin );      // Disable COLUMN1
    io_set_L( COLUMN2_PORT, COLUMN2_pin );      // Enable COLUMN2
    io_set_H( COLUMN3_PORT, COLUMN3_pin );      // Disable COLUMN3
    io_set_H( COLUMN4_PORT, COLUMN4_pin );      // Disable COLUMN4
    break;
  case 3:       // "Column" is activated by low level
    io_set_H( COLUMN0_PORT, COLUMN0_pin );      // Disable COLUMN0
    io_set_H( COLUMN1_PORT, COLUMN1_pin );      // Disable COLUMN1
    io_set_H( COLUMN2_PORT, COLUMN2_pin );      // Disable COLUMN2
    io_set_L( COLUMN3_PORT, COLUMN3_pin );      // Enable COLUMN3
    io_set_H( COLUMN4_PORT, COLUMN4_pin );      // Disable COLUMN4
    break;
  case 4:       // "Column" is activated by low level
    io_set_H( COLUMN0_PORT, COLUMN0_pin );      // Disable COLUMN0
    io_set_H( COLUMN1_PORT, COLUMN1_pin );      // Disable COLUMN1
    io_set_H( COLUMN2_PORT, COLUMN2_pin );      // Disable COLUMN2
    io_set_H( COLUMN3_PORT, COLUMN3_pin );      // Disable COLUMN3
    io_set_L( COLUMN4_PORT, COLUMN4_pin );      // Enable COLUMN4
    break;
  default:       // "Column" is activated by low level
    io_set_H( COLUMN0_PORT, COLUMN0_pin );      // Disable COLUMN0
    io_set_H( COLUMN1_PORT, COLUMN1_pin );      // Disable COLUMN1
    io_set_H( COLUMN2_PORT, COLUMN2_pin );      // Disable COLUMN2
    io_set_H( COLUMN3_PORT, COLUMN3_pin );      // Disable COLUMN3
    io_set_H( COLUMN4_PORT, COLUMN4_pin );      // Disable COLUMN4
  }
}

/*---------------------------------------------------------------------------*/



inline void load_DATA_RED_on_bus( uint8_t i_data_r )
{
  // D_R0 - test D_R0 bit in i_data_r
  if ( (i_data_r & 0b00000001) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_R0_PORT, D_R0_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_R0_PORT, D_R0_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_R1 - test D_R1 bit in i_data_r
  if ( (i_data_r & 0b00000010) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_R1_PORT, D_R1_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_R1_PORT, D_R1_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_R2 - test D_R2 bit in i_data_r
  if ( (i_data_r & 0b00000100) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_R2_PORT, D_R2_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_R2_PORT, D_R2_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_R3 - test D_R3 bit in i_data_r
  if ( (i_data_r & 0b00001000) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_R3_PORT, D_R3_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_R3_PORT, D_R3_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_R4 - test D_R4 bit in i_data_r
  if ( (i_data_r & 0b00010000) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_R4_PORT, D_R4_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_R4_PORT, D_R4_pin );
  }
}


/*---------------------------------------------------------------------------*/


inline void load_DATA_GREEN_on_bus( uint8_t i_data_g )
{
  // D_G0 - test D_G0 bit in i_data_g
  if ( (i_data_g & 0b00000001) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_G0_PORT, D_G0_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_G0_PORT, D_G0_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_G1 - test D_G1 bit in i_data_g
  if ( (i_data_g & 0b00000010) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_G1_PORT, D_G1_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_G1_PORT, D_G1_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_G2 - test D_G2 bit in i_data_g
  if ( (i_data_g & 0b00000100) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_G2_PORT, D_G2_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_G2_PORT, D_G2_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_G3 - test D_G3 bit in i_data_g
  if ( (i_data_g & 0b00001000) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_G3_PORT, D_G3_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_G3_PORT, D_G3_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_G4 - test D_G4 bit in i_data_g
  if ( (i_data_g & 0b00010000) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_G4_PORT, D_G4_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_G4_PORT, D_G4_pin );
  }
}


/*---------------------------------------------------------------------------*/


inline void load_DATA_BLUE_on_bus( uint8_t i_data_b )
{
  // D_B0 - test D_B0 bit in i_data_r
  if ( (i_data_b & 0b00000001) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_B0_PORT, D_B0_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_B0_PORT, D_B0_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_B1 - test D_B1 bit in i_data_b
  if ( (i_data_b & 0b00000010) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_B1_PORT, D_B1_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_B1_PORT, D_B1_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_B2 - test D_B2 bit in i_data_b
  if ( (i_data_b & 0b00000100) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_B2_PORT, D_B2_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_B2_PORT, D_B2_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_B3 - test D_B3 bit in i_data_b
  if ( (i_data_b & 0b00001000) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_B3_PORT, D_B3_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_B3_PORT, D_B3_pin );
  }
/*---------------------------------------------------------------------------*/
  // D_B4 - test D_B4 bit in i_data_b
  if ( (i_data_b & 0b00010000) == 0 )
  {     // If 0 -> set 0
    io_set_0( D_B4_PORT, D_B4_pin );
  }
  else
  {     // Else set 1
    io_set_1( D_B4_PORT, D_B4_pin );
  }
}

/*---------------------------------------------------------------------------*/

inline void enable_isr_for_while(void)
{
  // Enable interrupts, do some dummy operations, disable interrupts
  sei();
  __asm("NOP");
  __asm("NOP");
  __asm("NOP");
  __asm("NOP");
  __asm("NOP");
  cli();
}
