/**
 * \file
 *
 * \brief Library for communication with another devices
 *
 * This library should handle communication from UART and I2C.
 *
 * Created: 2015/06/21\n
 * Updated: 2016/08/17
 *
 * \version 0.2
 *
 * \author Martin Stejskal
 */

#ifndef _communication_library_
#define _communication_library_
//================================| Includes |=================================
#include "uart.h"

/* Keyboard library allow switching modes -> need switch to get private
 * access to framebuffer
 */
#include "keyboard_6x5.h"

/* Need access to PWM */
#include "pwm_rgb.h"

// Working with string
#include <string.h>

//=================================| Defines |=================================
/**
 * @brief Device name
 *
 * This string will be used for introducing device through UART. Since name
 * is also on PCB, it is very good idea to use same naming -> easy recognition
 */
#define COMM_DEV_NAME  "RGB_LED_cube_5x5x5-00\n"

/**
 * @brief Size of buffer for UART RX data
 *
 * It need to hold at least size of framebuffer (5*5* 16b -> 400b -> 50B ) plus
 * PWM information (3 Bytes) -> at LEAST 53 Bytes
 */
#define COMM_BUFF_SIZE  60

/**
 * @brief Word that start initialize sequence
 *
 * PC (probably) have to send brief greetings, so LED cube will start
 * responding
 */
#define COMM_INIT_WORD  "Hello\n"


/**
 * @brief String which is used to get cube dimension
 */
#define CONN_CUBE_DIMENSION_Q "Dimension?\n"

/**
 * @brief Cube's response to CONN_CUBE_DIMENSION_Q
 */
#define CONN_CUBE_DIMENSION_A "5\n"


/**
 * @brief Optional message when got unexpected string
 */
#define CONN_NOT_UNDERSTAND "\nSorry. I do not understand\n"


/**
 * @brief Command that execute "fill buffer" function
 */
#define CONN_CMD_FILL_BUFF_REQ  "Fill buffer\n"

/**
 * @brief Positive response
 */
#define CONN_OK_RESP "OK\n"

/**
 * @brief Negative response
 */
#define CONN_NG_RESP "NG\n"


/**
 * @brief sequence of bits, that imply that current command is done
 *
 * After receiving of this sequence (at specified moments), command should
 * be aborted and state machine should move to state "COMM_WAIT_FOR_COMMAND"
 */
#define CONN_QUIT_CMD   0b11111111
//===============================| Structures |================================

//===============================| Structures |================================

/**
 * \brief List of error codes
 */
typedef enum{
  COMM_OK = 0,  //!< COMM_OK No problem
  COMM_FAIL = 1,//!< COMM_FAIL General error
  COMM_TIMEOUT = 2,//!< COMM_TIMEOUT Timeout occurred
  COMM_INVALID_PARAM = 3,//!< COMM_INVALID_PARAM Invalid parameter
  COMM_BUSY = 4, //!< COMM_BUSY Can not complete request
}comm_res;


/**
 * @brief State machine for communication
 *
 * Here are all basic states, which state machine uses
 */
typedef enum{
  COMM_WAIT_FOR_INIT,
  COMM_WAIT_FOR_COMMAND,
  COMM_FILL_BUFFER,
}comm_states;


/**
 * @brief Even "Wait for init" function need own states
 */
typedef enum{
  COMM_W84I_1_REQ_RX_DATA,
  COMM_W84I_2_W84_DATA,
  COMM_W84I_3_SEND_NAME,
  COMM_W84I_4_REQ_RX_DATA,
  COMM_W84I_5_W84_DATA,
  COMM_W84I_6_SEND_LED_IDX
}comm_w84init_states;

/**
 * @brief State machine for processing commands
 */
typedef enum{
  COMM_W84C_1_REQ_RX_DATA,//!< COMM_W84C_1_REQ_RX_DATA
  COMM_W84C_2_W84_DATA,   //!< COMM_W84C_2_W84_DATA
  COMM_W84C_3_SEND_OK,    //!< COMM_W84C_3_SEND_OK
  COMM_W84C_4_EXEC_CMD,
}comm_w84command_states;


/**
 * @brief States for "Fill buffer" command
 */
typedef enum{
  COMM_FILL_B_1_REQ_RX_DATA,//!< COMM_FILL_B_1_REQ_RX_DATA
  COMM_FILL_B_2_W84_DATA,   //!< COMM_FILL_B_2_W84_DATA
  COMM_FILL_B_3_SEND_ACK,   //!< COMM_FILL_B_3_SEND_IDX
  COMM_FILL_B_4_SEND_NG_AND_QUIT,
  COMM_FILL_B_5_SEND_OK_AND_QUIT,
}comm_fill_buffer_states;
//================================| Functions |================================
comm_res communication_init(void);

void communication_task(void);

void comm_wait_for_init(void);

void comm_wait_for_command(void);

void comm_fill_buff(void);

void print_str(char *p_c_str);

#endif
