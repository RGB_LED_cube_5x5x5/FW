/**
 * \file
 *
 * \brief Driver for UART
 *
 * This driver is optimized for LED cube project and it is NOT universal\n
 * driver for AVR! It is strictly written for ATmega64!\n
 * Please keep this on mind and when you decide to change anything be sure\n
 * you also changed it here.
 *
 * Created: 2015/06/02\n
 * Updated: 2016/08/15
 *
 * \author Martin Stejskal
 *
 */

#ifndef _uart_library_
#define _uart_library_
//================================| Includes |=================================
#include <inttypes.h>
// For simple working with sprintf
#include <stdio.h>
// Hardware Abstract Layer for UART
#include "uart_HAL_ATmega64.h"

//=================================| Defines |=================================
#define UART_TIMEOUT_CYCLES     10000UL

/**
 * @brief UART driver check string length and prevent overwriting RAM
 *
 * You know it, users... When dumb user will send "too long" string, it can
 * cause rewriting RAM which you may not wanted to rewrite... So here is
 * predefined max size of string which UART will be handle in memory. It does
 * not allocate any additional memory. It is simply check pointer value.
 * This is default value for preventing random RAM rewriting. Anyway user is
 * free to use uart_res uart_set_max_str_lenght() to set wanted value.
 */
#define UART_DEFALT_STR_RX_BUFFER_SIZE  10
//===============================| Structures |================================

/**
 * \brief List of error codes
 */
typedef enum{
  UART_OK = 0,  //!< UART_OK No problem
  UART_FAIL = 1,//!< UART_FAIL General error
  UART_TIMEOUT = 2,//!< UART_TIMEOUT Timeout occurred
  UART_INVALID_PARAM = 3,//!< UART_INVALID_PARAM Invalid parameter
  UART_BUSY = 4, //!< UART_BUSY Can not complete request
}uart_res;


typedef struct{
  enum{
    UART_WAITING_FOR_REQUEST,
    UART_DOING,
  }state;
  uint8_t *p_i_data;
  uint16_t i_data_length;
}uart_one_task_t;


typedef struct{
  uart_one_task_t rx_data;
  uart_one_task_t tx_data;
  uart_one_task_t rx_str;
  uart_one_task_t tx_str;
}uart_tasks_t;
//===========================| Function prototypes |===========================
//==========================| High level functions |===========================
uart_res uart_init(void);

void uart_task(void);
//=========================| Middle level functions |==========================
uart_res uart_request_tx_data_ready(uint8_t *p_i_status);
uart_res uart_request_rx_data_ready(uint8_t *p_i_status);
uart_res uart_request_tx_str_ready(uint8_t *p_i_status);
uart_res uart_request_rx_str_ready(uint8_t *p_i_status);

uart_res uart_request_tx_data(uint8_t *p_i_data, uint16_t i_length);
uart_res uart_request_rx_data(uint8_t *p_i_data, uint16_t i_length);
uart_res uart_request_tx_str(char *p_c_data);
uart_res uart_request_rx_str(char *p_c_data);
//===========================| Low level functions |===========================
uart_res uart_set_max_str_lenght(uint16_t i_max_len);

uart_res uart_tx_str(char *p_c_txt);

uart_res uart_tx_Byte(uint8_t i_data);

uart_res uart_rx_Byte(uint8_t *p_i_data);
#endif
