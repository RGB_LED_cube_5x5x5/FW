/**
 * \file
 *
 * \brief Driver for PWM on ATmega64 for controlling "R_GND", "G_GND" and
 * "B_GND"
 *
 * For advanced effects is PWM nice feature
 *
 * Created:  2013/05/18\n
 * Modified: 2015/05/31
 */

#include "pwm_rgb.h"


void init_pwm_rgb(void)
{
  /* Only set pins to output and set them to 1. No matter if support is enabled
   * or disabled. Always set as output...
   */

  // Red color
  io_set_dir_out( PWM_R_PORT, PWM_R_pin );
  io_set_1(       PWM_R_PORT, PWM_R_pin );

  // Green color
  io_set_dir_out( PWM_G_PORT, PWM_G_pin );
  io_set_1(       PWM_G_PORT, PWM_G_pin );

  // Blue color
  io_set_dir_out( PWM_B_PORT, PWM_B_pin );
  io_set_1(       PWM_B_PORT, PWM_B_pin );

  // OK, add configure some stuff, if required...
#if pwm_support == 1

  // Set outputs to non-inverting 8bit fast PWM
  TCCR3A = (1<<COM3A1)|(0<<COM3A0)|\
           (1<<COM3B1)|(0<<COM3B0)|\
           (1<<COM3C1)|(0<<COM3C0)|\
           (0<<WGM31) |(1<<WGM30);

  // 8bit fast PWM, prescaller 1
  TCCR3B = (0<<WGM33) |(0<<WGM32)|\
           (0<<CS32)  |(0<<CS31) |(1<<CS30);

  // And set default values to OCR3x registers (on)
  OCR3A = 255;  // PWM value for red color -> 100%
  OCR3B = 255;  // PWM value for green color -> 100%
  OCR3C = 255;  // PWM value for blue color -> 100%
#endif
}
