/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Simple process scheduler
 *
 * Scheduler can be used in cases, that is need to run periodic many process.\n
 * Called function MUST BE type void function(void) !!!.Scheduler do not take\n
 * any arguments and return values!
 *
 * Created:  2013/06/08\n
 * Modified: 2015/06/21
 */

// Trick for multiply include this file
#ifndef _scheduler_library_
#define _scheduler_library_

/*-----------------------------------------*
 |               Includes                  |
 *-----------------------------------------*/
#include <inttypes.h>   // Needed by scheduler()

/* ADD LIBRARIES, WHERE USER FUNCTIONS ARE DEFINED
 * Example:
 * #include "all_my_cool_functions.h"
 */
#include "../rgb_led_cube_5x5x5.h"
#include "keyboard_6x5.h"
#include "uart.h"
#include "communication.h"

/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/
/* DEFINE USER FUNCTIONS HERE
 * Example:
 * #define scheduler_task_0     do_fft();
 * #define scheduler_task_1     some_data_processing();
 * #define scheduler_task_2     show_on_LCD();
 */
#define scheduler_task_0        animation_scheduler()

#define scheduler_task_1        keyboard_process()

// UART have special task -> need little high priority
#define scheduler_task_2        uart_task()

#define scheduler_task_3        communication_task()


// ----------------------------------- //
/**
 * \brief Scheduler algorithm
 *
 * Every time when is called this function is called next function in\n
 * schedule. Thanks to this function is possible run "many" functions only\n
 * when there is enough time.
 */
void scheduler(void);



#endif
