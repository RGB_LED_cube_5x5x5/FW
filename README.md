# ==<| RGB LED cube 5x5x5 |>==
## About
 * Source codes for RGB LED cube 5x5x5

## Description
 * animations - folder with user defined animations
 * Debug - temporary folder when compile
 * led_cube_lib - non-standard libraries and settings
 * compile_and_run_C.sh - Linux script for compile and upload to AVR though
   USBasp
 * makefile - makefile for this project :)
 * rgb_led_cube_5x5x5.c - main program
 * rgb_led_cube_5x5x5.hex - HEX file for AVR
 * set_fuses.sh - set appropiate fuses through USBasp programmer


## Download
 * For downloading, please use GIT command with "--recursive" parameter

```bash
   $ git clone --recursive git@gitlab.com:RGB_LED_cube_5x5x5/FW.git
```

## Help and support
 * If you have questions, or if you want to contribute on this project, just
   let me know [martin.stej at gmail dot com]