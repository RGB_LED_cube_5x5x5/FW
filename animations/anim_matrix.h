/**
 * \file
 *
 * \brief Animation: matrix
 * For RGB LED cube 5x5x5
 *
 * \author Anime maker version 1.0 beta
 *
 * Anime maker by: Martin Stejskal (martin.stej@gmail.com)
 *
 */

#ifndef _anim_matrix_library_
#define _anim_matrix_library_

/*-----------------------------------------*
 |               Includes                  |
 *-----------------------------------------*/
#include <avr/pgmspace.h>       // For operations with flash memory
#include <inttypes.h>           // Data types

#include "animation_codes.h"    // For more "human read" animation code

extern const uint16_t bin_anim_matrix[];

#endif
