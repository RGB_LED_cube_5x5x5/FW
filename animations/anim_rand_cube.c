/**
 * \file
 *
 * \brief Animation: rand_cube
 * For RGB LED cube 5x5x5
 *
 * \author Anime maker version 1.0 beta
 *
 * Anime maker by: Martin Stejskal (martin.stej@gmail.com)
 *
 */

#include "anim_rand_cube.h"

/* Data format (binary data for framebuffer)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 0 | R | R | R | R | R | G | G | G | G | G | B | B | B | B | B |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 * Instruction format (if MSB is 1, then it is possible change some settings)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 1 | C | C | C | C | C | C | C | V | V | V | V | V | V | V | V |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *  C - command (0~127)
 *  V - value   (0~255)
 *
 */
const uint16_t bin_anim_rand_cube[] PROGMEM =
  {  // Serial stream of data
    // Command start of animation + value (slowdown factor)
    cmd_anim_start + 1,
    //Data + other commands (body)
    // Frame 0
    0x7FFF,0x4631,0x4631,0x4631,0x7FFF,
    0x4631,0x0000,0x0000,0x0000,0x4631,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x7FFF,0x4631,0x4631,0x4631,0x7FFF,
    

    // Frame 1
    cmd_rand_all_pwm + 255,
    

    // Frame 257
    cmd_rand_all_pwm + 255,
    

    // Frame 513
    cmd_rand_all_pwm + 255,
    

    // Frame 769
    
    // End of animation + option
    cmd_end_of_anim + 0
  };