/**
 * \file
 *
 * \brief Animation: arrow
 * For RGB LED cube 5x5x5
 *
 * \author Anime maker version 1.0 beta
 *
 * Anime maker by: Martin Stejskal (martin.stej@gmail.com)
 *
 */

#include "anim_arrow.h"

/* Data format (binary data for framebuffer)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 0 | R | R | R | R | R | G | G | G | G | G | B | B | B | B | B |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 * Instruction format (if MSB is 1, then it is possible change some settings)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 1 | C | C | C | C | C | C | C | V | V | V | V | V | V | V | V |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *  C - command (0~127)
 *  V - value   (0~255)
 *
 */
const uint16_t bin_anim_arrow[] PROGMEM =
  {  // Serial stream of data
    // Command start of animation + value (slowdown factor)
    cmd_anim_start + 2,
    //Data + other commands (body)
    // Frame 0
    0x1084,0x1084,0x39CE,0x1084,0x0000,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    

    // Frame 1
    0x0000,0x1084,0x1084,0x39CE,0x1084,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    

    // Frame 2
    0x0000,0x0000,0x1084,0x1084,0x39CE,
    0x0000,0x0000,0x0000,0x0000,0x1084,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    

    // Frame 3
    0x0000,0x0000,0x0000,0x1084,0x1084,
    0x0000,0x0000,0x0000,0x0000,0x39CE,
    0x0000,0x0000,0x0000,0x0000,0x1084,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    

    // Frame 4
    0x0000,0x0000,0x0000,0x0000,0x1084,
    cmd_param_2D_frame + 1,
    0x0000,0x0000,0x0000,0x0000,0x39CE,
    0x0000,0x0000,0x0000,0x0000,0x1084,
    cmd_param_2D_frame + 0,
    

    // Frame 5
    cmd_param_2D_frame + 0,
    0x0000,0x0000,0x0000,0x0000,0x1084,
    cmd_param_2D_frame + 1,
    0x0000,0x0000,0x0000,0x0000,0x39CE,
    0x0000,0x0000,0x0000,0x0000,0x1084,
    

    // Frame 6
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    0x0000,0x0000,0x0000,0x0000,0x1084,
    cmd_param_2D_frame + 1,
    0x0000,0x0000,0x0000,0x1084,0x39CE,
    

    // Frame 7
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    0x0000,0x0000,0x0000,0x0000,0x1084,
    0x0000,0x0000,0x1084,0x39CE,0x1084,
    

    // Frame 8
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    0x0000,0x1084,0x39CE,0x1084,0x1084,
    

    // Frame 9
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    0x1084,0x39CE,0x1084,0x1084,0x0000,
    

    // Frame 10
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    0x1084,0x0000,0x0000,0x0000,0x0000,
    0x39CE,0x1084,0x1084,0x0000,0x0000,
    

    // Frame 11
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    0x1084,0x0000,0x0000,0x0000,0x0000,
    0x39CE,0x0000,0x0000,0x0000,0x0000,
    0x1084,0x1084,0x0000,0x0000,0x0000,
    

    // Frame 12
    cmd_param_2D_frame + 0,
    0x1084,0x0000,0x0000,0x0000,0x0000,
    0x39CE,0x0000,0x0000,0x0000,0x0000,
    0x1084,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + 1,
    

    // Frame 13
    0x1084,0x0000,0x0000,0x0000,0x0000,
    0x39CE,0x0000,0x0000,0x0000,0x0000,
    0x1084,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + 1,
    cmd_param_2D_frame + 0,
    

    // Frame 14
    0x39CE,0x1084,0x0000,0x0000,0x0000,
    0x1084,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + 1,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    

    // Frame 15
    0x1084,0x39CE,0x1084,0x0000,0x0000,
    0x1084,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    cmd_param_2D_frame + 0,
    

    // Frame 16
        // End of animation + option
    cmd_end_of_anim + 15
  };
