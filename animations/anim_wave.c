/**
 * \file
 *
 * \brief Animation: wave
 * For RGB LED cube 5x5x5
 *
 * \author Anime maker version 1.0 beta
 *
 * Anime maker by: Martin Stejskal (martin.stej@gmail.com)
 *
 */

#include "anim_wave.h"

/* Data format (binary data for framebuffer)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 0 | R | R | R | R | R | G | G | G | G | G | B | B | B | B | B |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 * Instruction format (if MSB is 1, then it is possible change some settings)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 1 | C | C | C | C | C | C | C | V | V | V | V | V | V | V | V |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *  C - command (0~127)
 *  V - value   (0~255)
 *
 */
const uint16_t bin_anim_wave[] PROGMEM =
  {  // Serial stream of data
    // Command start of animation + value (slowdown factor)
    cmd_anim_start + 3,
    //Data + other commands (body)
    // Frame 0
    cmd_set_pwm_r + 0,
    cmd_set_pwm_b + 180,
    0x0421,0x0421,0x0421,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 1
    0x0842,0x0421,0x0421,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 2
    0x1084,0x0842,0x0421,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 3
    0x2108,0x1084,0x0842,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 4
    0x1084,0x2108,0x1084,0x0842,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 5
    0x0842,0x1084,0x2108,0x1084,0x0842,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 6
    0x0421,0x0842,0x1084,0x2108,0x1084,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 7
    0x0421,0x0421,0x0842,0x1084,0x2108,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 8
    0x0421,0x0421,0x0421,0x0842,0x1084,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 9
    0x0421,0x0421,0x0421,0x0421,0x0842,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 10
    0x0421,0x0421,0x0421,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 11
    cmd_param_3D_frame + param_3D_frame_same,
    

    // Frame 12
    cmd_param_3D_frame + param_3D_frame_same,
    

    // Frame 13
    cmd_param_3D_frame + param_3D_frame_same,
    

    // Frame 14
    0x0421,0x0421,0x0421,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x0842,0x0842,0x0842,0x0842,0x0842,
    

    // Frame 15
    0x0421,0x0421,0x0421,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x0842,0x0842,0x0842,0x0842,0x0842,
    0x1084,0x1084,0x1084,0x1084,0x1084,
    

    // Frame 16
    0x0421,0x0421,0x0421,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    0x0842,0x0842,0x0842,0x0842,0x0842,
    0x1084,0x1084,0x1084,0x1084,0x1084,
    0x2108,0x2108,0x2108,0x2108,0x2108,
    

    // Frame 17
    0x0421,0x0421,0x0421,0x0421,0x0421,
    0x0842,0x0842,0x0842,0x0842,0x0842,
    0x1084,0x1084,0x1084,0x1084,0x1084,
    0x2108,0x2108,0x2108,0x2108,0x2108,
    0x1084,0x1084,0x1084,0x1084,0x1084,
    

    // Frame 18
    0x0842,0x0842,0x0842,0x0842,0x0842,
    0x1084,0x1084,0x1084,0x1084,0x1084,
    0x2108,0x2108,0x2108,0x2108,0x2108,
    0x1084,0x1084,0x1084,0x1084,0x1084,
    0x0842,0x0842,0x0842,0x0842,0x0842,
    

    // Frame 19
    0x1084,0x1084,0x1084,0x1084,0x1084,
    0x2108,0x2108,0x2108,0x2108,0x2108,
    0x1084,0x1084,0x1084,0x1084,0x1084,
    0x0842,0x0842,0x0842,0x0842,0x0842,
    0x0421,0x0421,0x0421,0x0421,0x0421,
    

    // Frame 20
    0x2108,0x2108,0x2108,0x2108,0x2108,
    0x1084,0x1084,0x1084,0x1084,0x1084,
    0x0842,0x0842,0x0842,0x0842,0x0842,
    0x0421,0x0421,0x0421,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 21
    0x1084,0x1084,0x1084,0x1084,0x1084,
    0x0842,0x0842,0x0842,0x0842,0x0842,
    0x0421,0x0421,0x0421,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 22
    0x0842,0x0842,0x0842,0x0842,0x0842,
    0x0421,0x0421,0x0421,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    

    // Frame 23
    0x0421,0x0421,0x0421,0x0421,0x0421,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    


    // Frame 24
    cmd_param_3D_frame + param_3D_frame_same,
    

    // Frame 25
    cmd_param_3D_frame + param_3D_frame_same,
    

    // Frame 26
    cmd_param_3D_frame + param_3D_frame_same,


    // Frame 27
    
    // End of animation + option
    cmd_end_of_anim + 6
  };
