/**
 * \file
 *
 * \brief Animation: matrix
 * For RGB LED cube 5x5x5
 *
 * \author Anime maker version 1.0 beta
 *
 * Anime maker by: Martin Stejskal (martin.stej@gmail.com)
 *
 */

#include "anim_matrix.h"

/* Data format (binary data for framebuffer)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 0 | R | R | R | R | R | G | G | G | G | G | B | B | B | B | B |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 * Instruction format (if MSB is 1, then it is possible change some settings)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 1 | C | C | C | C | C | C | C | V | V | V | V | V | V | V | V |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *  C - command (0~127)
 *  V - value   (0~255)
 *
 */
const uint16_t bin_anim_matrix[] PROGMEM =
  {  // Serial stream of data
    // Command start of animation + value (slowdown factor)
    cmd_anim_start + 4,
    //Data + other commands (body)
    // Frame 0
    0x0000,0x0200,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 1
    0x0000,0x0300,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 2
    0x0000,0x0380,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0200,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 3
    0x0000,0x01C0,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0300,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 4
    0x0000,0x00E0,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0380,0x0000,
    0x0200,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 5
    0x0000,0x0060,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x01C0,0x0000,
    0x0300,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 6
    0x0000,0x0020,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x00E0,0x0000,
    0x0180,0x0000,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0200,0x0000,0x0000,
    

    // Frame 7
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0060,0x0000,
    0x00C0,0x0000,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0100,0x0000,0x0000,
    

    // Frame 8
    0x0000,0x0200,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0020,0x0000,
    0x0060,0x0000,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0080,0x0000,0x0000,
    

    // Frame 9
    0x0000,0x0300,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0200,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0020,0x0000,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0040,0x0000,0x0000,
    

    // Frame 10
    0x0000,0x0380,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0300,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0020,0x0000,0x0000,
    

    // Frame 11
    0x0000,0x03C0,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0380,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0200,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 12
    0x0000,0x01E0,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x01C0,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0300,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 13
    0x0000,0x00E0,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x00E0,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0380,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 14
    0x0000,0x0060,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0060,0x0000,
    0x0000,0x0000,0x0200,0x0000,0x0000,
    0x01C0,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 15
    0x0000,0x0020,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0020,0x0000,
    0x0000,0x0000,0x0300,0x0000,0x0000,
    0x00E0,0x0000,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0000,0x0200,
    

    // Frame 16
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0180,0x0000,0x0000,
    0x0060,0x0000,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0000,0x0300,
    

    // Frame 17
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x00C0,0x0000,0x0000,
    0x0020,0x0000,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0000,0x0380,
    

    // Frame 18
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0060,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0000,0x03C0,
    

    // Frame 19
    0x0000,0x0200,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0020,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0000,0x03E0,
    

    // Frame 20
    0x0000,0x0300,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0000,0x03E0,
    

    // Frame 21
    0x0000,0x0180,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0000,0x01E0,
    

    // Frame 22
    0x0000,0x00C0,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0200,0x0000,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0000,0x00E0,
    

    // Frame 23
    0x0000,0x0060,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0300,0x0000,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0000,0x0060,
    

    // Frame 24
    0x0000,0x0020,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0380,0x0000,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0000,0x0020,
    

    // Frame 25
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0200,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x01C0,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 26
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0300,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x00E0,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 27
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0380,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0060,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 28
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x01C0,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0020,0x0000,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 29
    0x0000,0x0200,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x00E0,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 30
    0x0000,0x0300,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0060,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 31
    0x0000,0x0380,0x0000,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0020,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 32
    0x0000,0x01C0,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0000,0x0200,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 33
    0x0000,0x00E0,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0000,0x0300,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 34
    0x0000,0x0060,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0000,0x0380,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 35
    0x0000,0x0020,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0000,0x01C0,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 36
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0000,0x00E0,
    0x0000,0x0200,0x0000,0x0000,0x0000,
    

    // Frame 37
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0000,0x0060,
    0x0000,0x0300,0x0000,0x0000,0x0000,
    

    // Frame 38
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0000,0x0000,0x0000,0x0020,
    0x0000,0x0180,0x0000,0x0000,0x0000,
    

    // Frame 39
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x00C0,0x0000,0x0000,0x0000,
    

    // Frame 40
    0x0000,0x0000,0x0000,0x0000,0x0200,
    0x0000,0x0200,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0060,0x0000,0x0000,0x0000,
    

    // Frame 41
    0x0000,0x0000,0x0000,0x0000,0x0300,
    0x0000,0x0300,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0020,0x0000,0x0000,0x0000,
    

    // Frame 42
    0x0000,0x0000,0x0000,0x0000,0x0180,
    0x0000,0x0380,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 43
    0x0000,0x0000,0x0000,0x0000,0x00C0,
    0x0000,0x01C0,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 44
    0x0000,0x0000,0x0000,0x0000,0x0060,
    0x0000,0x00E0,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 45
    0x0000,0x0000,0x0000,0x0000,0x0020,
    0x0000,0x0060,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 46
    cmd_param_2D_frame + param_2D_frame_void,
    0x0000,0x0020,0x0000,0x0000,0x0000,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    cmd_param_2D_frame + param_2D_frame_void,
    

    // Frame 47
    cmd_param_3D_frame + param_3D_frame_void,
    

    // Frame 48
    
    // End of animation + option
    cmd_end_of_anim + 5
  };