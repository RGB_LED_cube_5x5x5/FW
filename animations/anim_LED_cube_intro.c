/**
 * \file
 *
 * \brief Animation: LED_cube_intro
 * For RGB LED cube 5x5x5
 *
 * \author Anime maker version 1.0 beta
 *
 * Anime maker by: Martin Stejskal (martin.stej@gmail.com)
 *
 */

#include "anim_LED_cube_intro.h"

/* Data format (binary data for framebuffer)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 0 | R | R | R | R | R | G | G | G | G | G | B | B | B | B | B |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 * Instruction format (if MSB is 1, then it is possible change some settings)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 1 | C | C | C | C | C | C | C | V | V | V | V | V | V | V | V |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *  C - command (0~127)
 *  V - value   (0~255)
 *
 */
const uint16_t bin_anim_LED_cube_intro[] PROGMEM =
  {  // Serial stream of data
    // Command start of animation + value (slowdown factor)
    cmd_anim_start + 4,
    //Data + other commands (body)
    // Frame 0
    cmd_draw_character + 76,
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 1
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 2
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 3
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 4
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 5
    cmd_draw_character + 69,
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 6
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 7
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 8
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 9
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 10
    cmd_draw_character + 68,
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 11
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 12
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 13
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,
    

    // Frame 14
    cmd_rotate_fb + param_rotate_fb_forward,
    cmd_end_of_3D_frame + 0,


        // Frame 15
    cmd_set_pwm_r + 1,
    cmd_set_pwm_g + 1,
    cmd_set_pwm_b + 1,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    
        // Frame 15
    cmd_set_pwm_r + 5,
    cmd_set_pwm_g + 5,
    cmd_set_pwm_b + 5,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    
    // Frame 15
    cmd_set_pwm_r + 10,
    cmd_set_pwm_g + 10,
    cmd_set_pwm_b + 10,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    
    
    // Frame 15
    cmd_set_pwm_r + 20,
    cmd_set_pwm_g + 20,
    cmd_set_pwm_b + 20,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,    

    // Frame 15
    cmd_set_pwm_r + 30,
    cmd_set_pwm_g + 30,
    cmd_set_pwm_b + 30,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 16
    cmd_set_pwm_r + 40,
    cmd_set_pwm_g + 40,
    cmd_set_pwm_b + 40,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 17
    cmd_set_pwm_r + 50,
    cmd_set_pwm_g + 50,
    cmd_set_pwm_b + 50,
    cmd_set_pwm_r + 60,
    cmd_set_pwm_g + 60,
    cmd_set_pwm_b + 60,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 18
    cmd_set_pwm_r + 70,
    cmd_set_pwm_g + 70,
    cmd_set_pwm_b + 70,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 19
    cmd_set_pwm_r + 80,
    cmd_set_pwm_g + 80,
    cmd_set_pwm_b + 80,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 20
    cmd_set_pwm_r + 90,
    cmd_set_pwm_g + 90,
    cmd_set_pwm_b + 90,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 21
    cmd_set_pwm_r + 100,
    cmd_set_pwm_g + 100,
    cmd_set_pwm_b + 100,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 22
    cmd_set_pwm_r + 110,
    cmd_set_pwm_g + 110,
    cmd_set_pwm_b + 110,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 23
    cmd_set_pwm_r + 120,
    cmd_set_pwm_g + 120,
    cmd_set_pwm_b + 120,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 24
    cmd_set_pwm_r + 130,
    cmd_set_pwm_g + 130,
    cmd_set_pwm_b + 130,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 25
    cmd_set_pwm_r + 140,
    cmd_set_pwm_g + 140,
    cmd_set_pwm_b + 140,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 26
    cmd_set_pwm_r + 150,
    cmd_set_pwm_g + 150,
    cmd_set_pwm_b + 150,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 27
    cmd_set_pwm_r + 160,
    cmd_set_pwm_g + 160,
    cmd_set_pwm_b + 160,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 28
    cmd_set_pwm_r + 170,
    cmd_set_pwm_g + 170,
    cmd_set_pwm_b + 170,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 29
    cmd_set_pwm_r + 180,
    cmd_set_pwm_g + 180,
    cmd_set_pwm_b + 180,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 30
    cmd_set_pwm_r + 190,
    cmd_set_pwm_g + 190,
    cmd_set_pwm_b + 190,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 31
    cmd_set_pwm_r + 200,
    cmd_set_pwm_g + 200,
    cmd_set_pwm_b + 200,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 32
    cmd_set_pwm_r + 210,
    cmd_set_pwm_g + 210,
    cmd_set_pwm_b + 210,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 33
    cmd_set_pwm_r + 220,
    cmd_set_pwm_g + 220,
    cmd_set_pwm_b + 220,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 34
    cmd_set_pwm_r + 230,
    cmd_set_pwm_g + 230,
    cmd_set_pwm_b + 230,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 35
    cmd_set_pwm_r + 240,
    cmd_set_pwm_g + 240,
    cmd_set_pwm_b + 240,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 36
    cmd_set_pwm_r + 255,
    cmd_set_pwm_g + 255,
    cmd_set_pwm_b + 255,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 37
    cmd_param_3D_frame + param_3D_frame_same,
    

    // Frame 38
    cmd_param_3D_frame + param_3D_frame_same,
    

    // Frame 39
    cmd_param_3D_frame + param_3D_frame_same,
    

    // Frame 40
    cmd_set_pwm_r + 240,
    cmd_set_pwm_g + 240,
    cmd_set_pwm_b + 240,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 41
    cmd_set_pwm_r + 230,
    cmd_set_pwm_g + 230,
    cmd_set_pwm_b + 230,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 42
    cmd_set_pwm_r + 220,
    cmd_set_pwm_g + 220,
    cmd_set_pwm_b + 220,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 43
    cmd_set_pwm_r + 210,
    cmd_set_pwm_g + 210,
    cmd_set_pwm_b + 210,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 44
    cmd_set_pwm_r + 200,
    cmd_set_pwm_g + 200,
    cmd_set_pwm_b + 200,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 45
    cmd_set_pwm_r + 190,
    cmd_set_pwm_g + 190,
    cmd_set_pwm_b + 190,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 46
    cmd_set_pwm_r + 180,
    cmd_set_pwm_g + 180,
    cmd_set_pwm_b + 180,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 47
    cmd_set_pwm_r + 170,
    cmd_set_pwm_g + 170,
    cmd_set_pwm_b + 170,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 48
    cmd_set_pwm_r + 160,
    cmd_set_pwm_g + 160,
    cmd_set_pwm_b + 160,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 49
    cmd_set_pwm_r + 150,
    cmd_set_pwm_g + 150,
    cmd_set_pwm_b + 150,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 50
    cmd_set_pwm_r + 140,
    cmd_set_pwm_g + 140,
    cmd_set_pwm_b + 140,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 51
    cmd_set_pwm_r + 130,
    cmd_set_pwm_g + 130,
    cmd_set_pwm_b + 130,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 52
    cmd_set_pwm_r + 120,
    cmd_set_pwm_g + 120,
    cmd_set_pwm_b + 120,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 53
    cmd_set_pwm_r + 110,
    cmd_set_pwm_g + 110,
    cmd_set_pwm_b + 110,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 54
    cmd_set_pwm_r + 100,
    cmd_set_pwm_g + 100,
    cmd_set_pwm_b + 100,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 55
    cmd_set_pwm_r + 90,
    cmd_set_pwm_g + 90,
    cmd_set_pwm_b + 90,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 56
    cmd_set_pwm_r + 80,
    cmd_set_pwm_g + 80,
    cmd_set_pwm_b + 80,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 57
    cmd_set_pwm_r + 70,
    cmd_set_pwm_g + 70,
    cmd_set_pwm_b + 70,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 58
    cmd_set_pwm_r + 60,
    cmd_set_pwm_g + 60,
    cmd_set_pwm_b + 60,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 59
    cmd_set_pwm_r + 50,
    cmd_set_pwm_g + 50,
    cmd_set_pwm_b + 50,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 60
    cmd_set_pwm_r + 40,
    cmd_set_pwm_g + 40,
    cmd_set_pwm_b + 40,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 61
    cmd_set_pwm_r + 30,
    cmd_set_pwm_g + 30,
    cmd_set_pwm_b + 30,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 62
    cmd_set_pwm_r + 20,
    cmd_set_pwm_g + 20,
    cmd_set_pwm_b + 20,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 63
    cmd_set_pwm_r + 10,
    cmd_set_pwm_g + 10,
    cmd_set_pwm_b + 10,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 64
    cmd_set_pwm_r + 6,
    cmd_set_pwm_g + 6,
    cmd_set_pwm_b + 6,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 65
    cmd_set_pwm_r + 4,
    cmd_set_pwm_g + 4,
    cmd_set_pwm_b + 4,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 66
    cmd_set_pwm_r + 2,
    cmd_set_pwm_g + 2,
    cmd_set_pwm_b + 2,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 67
    cmd_set_pwm_r + 1,
    cmd_set_pwm_g + 1,
    cmd_set_pwm_b + 1,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 68
    cmd_set_pwm_r + 0,
    cmd_set_pwm_g + 0,
    cmd_set_pwm_b + 0,
    0x03E0,0x0220,0x0220,0x0220,0x03E0,
    0x4400,0x0000,0x0000,0x0000,0x4400,
    cmd_param_2D_frame + param_2D_frame_same,
    cmd_param_2D_frame + param_2D_frame_same,
    0x001F,0x0011,0x0011,0x0011,0x001F,
    

    // Frame 69
    cmd_param_3D_frame + param_3D_frame_void,
    

    // Frame 70
    cmd_param_3D_frame + param_3D_frame_void,
    

    // Frame 71
    cmd_param_3D_frame + param_3D_frame_void,
    

    // Frame 72
    cmd_param_3D_frame + param_3D_frame_void,
    

    // Frame 73
    cmd_param_3D_frame + param_3D_frame_void,
    

    // Frame 74
    cmd_param_3D_frame + param_3D_frame_void,
    

    // Frame 75
    
    // End of animation + option
    cmd_end_of_anim + 0
  };