/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Main file for RGB LED cube 5x5x5
 *
 * Created:  2013/05/16\n
 * Modified: 2016/08/17
 *
 * \version 0.5
*/

// Function prototypes, settings, symbolic names, F_CPU, and so on
#include "rgb_led_cube_5x5x5.h"


//============================| Global variables |=============================

// Usually needed because of interrupt functions
/**
 * \brief Framebuffer for 1 image, that is displayed
 *
 * Define frame buffer - buffer size: 16bits -> 3 colors x 5 led in\n
 * column -> 5 columns in one wall -> 5x16bits -> 5 walls -> 5x5x16bits\n
 * First index is for "wall" (0~4) and second is for "column" (0~4).\n
 * Must be global and volatile, because it is used also by interrupts.\n
 * [WALL][COLUMN]
 */
volatile uint16_t i_fb_data[5][5] =
// Fill with demo data - nothing - no LED on
{
        {0x0000, 0x0000, 0x0000, 0x0000, 0x0000},
        {0x0000, 0x0000, 0x0000, 0x0000, 0x0000},
        {0x0000, 0x0000, 0x0000, 0x0000, 0x0000},
        {0x0000, 0x0000, 0x0000, 0x0000, 0x0000},
        {0x0000, 0x0000, 0x0000, 0x0000, 0x0000}
};


/**
 * \brief Status and configuration structure for animation process
 */
volatile stat_cfg_anime_t i_stat_cfg_anime;

/**
 * \brief Pointer for animation process
 *
 * To this pointer is stored SRAM or FLASH address, where are data stored.\n
 * 16 bit pointer should be enough.
 */
volatile const uint16_t *p_anime = 0;

/**
 * \brief Slowdown factor for animation
 *
 * Every 40 ms is possible load new frame -> 25 fps. However, sometimes it is\n
 * too short time, so it is possible make animation slower. If is set to 1,\n
 * then every 40 ms is load new frame. If is set to 2, then every 80 ms is\n
 * loaded new frame and so on. This value can be changed any time during\n
 * animation (Should be part of animation code). So do not change it here.
 *
 */
volatile uint8_t  anime_slowdown_factor = 1;

/**
 * \brief Counter for slowdown factor
 *
 * Internal variable. Value is increased every 40 ms and set to 1 when\n
 * anime_slowdown_factor == anime_slowdown_counter. Should not be changed by\n
 * user.
 */
volatile uint8_t  anime_slowdown_counter = 1;

/**
 * \brief Counter for whole animations
 *
 * When is one animation played to the end, then value is increased by 1.
 */
volatile uint8_t  i_anime_scheduler_animation_id = 0;

/**
 * \brief Counter for random animation
 *
 * If enabled random function, there must be some counter, witch tell when\n
 * random animations should end
 */
volatile uint8_t  i_random_counter = 0;

/**
 * \brief Pointer to "random" address
 *
 * When we need read random data, it is easy to point somewhere in SRAM -> get
 * pseudo random data
 */
volatile uint16_t *p_random_addr = (uint16_t*)move_random_pointer;

/**
 * \brief Counts how many times was animation repeated
 *
 * When is read last command, then is need to find out if animation should be\n
 * repeated or not. For this is there this variable
 */
volatile uint8_t  i_anime_repeat_counter;


//===============================| Interrupts |================================

/**
 * \brief Function for animation process.
 *
 * Approximately every 10 ms is called this interrupt. Every 40 ms (every\n
 * fourth time) this function increase pointer value and read another image.\n
 * If animation is at the end, then is wait for a "new pointer" and show last\n
 * image in buffer. Used timer2, because interrupt priority is one of the\n
 * first.
 */
ISR(TIMER2_COMP_vect)
{
  /* Sometimes is needed that delay should be minimum. For example when
   * animation is repeated, then is needed minimal delay between last and first
   * frame.
   */
  volatile static uint8_t i_do_not_wait;

  /* Let's find out in witch time phase function is. If in phase 3, then it is
   * time to play some animation.
   */

  // Test if run out 40 ms - exception only do_not_wait
  if( (i_stat_cfg_anime.anim_cnt == 3) || (i_do_not_wait != 0) )
  {
    // Set phase to begin (always if i_phase >= 3)
    i_stat_cfg_anime.anim_cnt = 0;

    // Check slowdown factor - exception only do_not_wait
    if( (anime_slowdown_counter >= anime_slowdown_factor) || \
        (i_do_not_wait != 0) )
    {
      // OK, first reset slowdown counter to 1
      anime_slowdown_counter = 1;


      // Check animation status - Run/Stop
      if( i_stat_cfg_anime.anim_run != 0 )
      {// Animation run
        // Variable for temporary data
        uint16_t i_tmp = 0;

        // Counter for bytes loaded to framebuffer
        uint8_t i_framebuffer_cnt = 0;


        /* OK, if status is "run", then is needed to know from witch memory
         * data will be loaded. Options are flash and SRAM. So let's find
         * out...
         */
        if( i_stat_cfg_anime.anim_read_FLASH != 0)
        { // Data are stored in flash memory
          // OK, so fill whole framebuffer
          while( i_framebuffer_cnt < 25 )
          {
            i_tmp = pgm_read_word( p_anime );     // Load word (16 bits)
            p_anime++;                            // Increment pointer by 1

            /* And process loaded data. If data were written to framebuffer,
             * then increase "i_framebuffer_cnt" by 1
             */
            animation_process_word( i_tmp, &i_framebuffer_cnt, &i_do_not_wait );
          }
        }
        else//if( i_stat_cfg_anime.anim_read_FLASH != 0)
        { // Else data are stored in SRAM
          // OK, so fill whole framebuffer
          while( i_framebuffer_cnt < 25)
          {
            i_tmp = *p_anime;                     // Load word (16 bits)
            p_anime++;                            // Increase pointer py 1

            /* And process loaded data. If data were written to framebuffer,
             * then increase "i_framebuffer_cnt" by 1
             */
            animation_process_word( i_tmp,& i_framebuffer_cnt, &i_do_not_wait );
          }
        }
      }//if( i_stat_cfg_anime.anim_run != 0 )
      else// Else animation Stop -> nothing to do
      {
        //print_str("A stop\n");
      }
    }
    else
    {// If anime_slowdown_counter != anime_slowdown_factor
      anime_slowdown_counter++;
    }
  }
  else
  {// if( (i_stat_cfg_anime.anim_cnt == 3) || (i_do_not_wait != 0) )
    // -> increase value and wait for another "round"
    i_stat_cfg_anime.anim_cnt++;
  }
}// ISR(TIMER2_COMP_vect)




/**
 * \brief Main function
 *
 * Initialization of functions\n
 * Show content of i_fb_data in infinite loop\n
 *
 * \return \b 0 if all right, but this should never happened
 */
int main(void)
{
  // Initialize communication module
  communication_init();

  // Initialize framebuffer and framebuffer counter (Timer0)
  init_framebuffer();

  // Initialize PWM driver. If PWM support enabled then use Timer3 for PWM
  init_pwm_rgb();

  // Init i_stat_cfg_anime structure
  i_stat_cfg_anime.anim_scheduler_stop = 0;
  i_stat_cfg_anime.anim_cnt = 0;
  i_stat_cfg_anime.anim_play_in_loop = 0;
  i_stat_cfg_anime.anim_random = 0;
  i_stat_cfg_anime.anim_read_FLASH = 0;
  i_stat_cfg_anime.anim_run = 0;
  i_stat_cfg_anime.read_end_of_anim = 0;

  // Initialize Timer2 for animation process and prepare first animation
  init_animation();

  // Initialize keyboard
  init_keyboard_6x5();

  // Initialize system timer (RTC for other functions)
  init_sys_timer();

  while(1)
  {// Run framebuffer in infinite loop
    framebuffer( &i_fb_data );   // Give pointer to frame buffer
  }

  // This should never, ever happen. If it happen, then it is end of time....
  return(42);
}


//---------------------------------------------------------------------------//

/**
 * \brief Simple scheduler for animations
 *
 * This function is responsible for changing whole animations. It can read\n
 * animations from flash memory and maybe in future also from SRAM (but there\n
 * is not enough RAM probably....).
 *
 */
void animation_scheduler(void)
{
  /* Variable that is set to 1, when all animations were already played and
   * user do not want play animations in loop
   */
  uint8_t i_stop_playing = 0;

  // Keep information if user stop scheduler
  static uint8_t i_scheduler_stopped = 0;

  /* If for some reason we do not want to play animation (because another
   * process want to do it own way) we can do it this way
   */
  if( i_stat_cfg_anime.anim_scheduler_stop != 0 )
  {
    // Stop playing animations
    i_stat_cfg_anime.anim_run = 0;
    i_scheduler_stopped = 1;
    return;
  }

  // Check if we are "wake up" from postpone state
  if(i_scheduler_stopped != 0)
  {
    // So scheduller was stopped, animation was stopped.... Let's run animation
    i_stat_cfg_anime.anim_run = 1;
    i_scheduler_stopped = 0;
  }

  // Test if data are in flash - else play nothing
  if( i_stat_cfg_anime.anim_read_FLASH != 0 )
  {// Animation is in FLASH
    switch (i_anime_scheduler_animation_id)
    {
      /*---------------------------------------------------------------------*/
#ifdef anime0
    case 0:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime1
          p_anime = anime1;                   // Change pointer
#endif
#ifndef anime1
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime0;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime1
    case 1:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime2
          p_anime = anime2;                   // Change pointer
#endif
#ifndef anime2
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime1;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime2
    case 2:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime3
          p_anime = anime3;                   // Change pointer
#endif
#ifndef anime3
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime2;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime3
    case 3:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime4
          p_anime = anime4;                   // Change pointer
#endif
#ifndef anime4
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime3;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime4
    case 4:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime5
          p_anime = anime5;                   // Change pointer
#endif
#ifndef anime5
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime4;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime5
    case 5:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime6
          p_anime = anime6;                   // Change pointer
#endif
#ifndef anime6
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime5;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime6
    case 6:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime7
          p_anime = anime7;                   // Change pointer
#endif
#ifndef anime7
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime6;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime7
    case 7:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime8
          p_anime = anime8;                   // Change pointer
#endif
#ifndef anime8
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime7;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime8
    case 8:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime9
          p_anime = anime9;                   // Change pointer
#endif
#ifndef anime9
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime8;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime9
    case 9:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime10
          p_anime = anime10;                   // Change pointer
#endif
#ifndef anime10
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime9;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime10
    case 10:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime11
          p_anime = anime11;                   // Change pointer
#endif
#ifndef anime11
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime10;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime11
    case 11:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime12
          p_anime = anime12;                   // Change pointer
#endif
#ifndef anime12
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime11;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime12
    case 12:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime13
          p_anime = anime13;                   // Change pointer
#endif
#ifndef anime13
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime12;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime13
    case 13:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime14
          p_anime = anime14;                   // Change pointer
#endif
#ifndef anime14
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime13;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime14
    case 14:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime15
          p_anime = anime15;                   // Change pointer
#endif
#ifndef anime15
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime14;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime15
    case 15:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime16
          p_anime = anime16;                   // Change pointer
#endif
#ifndef anime16
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime15;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime16
    case 16:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime17
          p_anime = anime17;                   // Change pointer
#endif
#ifndef anime17
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime16;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime17
    case 17:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime18
          p_anime = anime18;                   // Change pointer
#endif
#ifndef anime18
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime17;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime18
    case 18:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime19
          p_anime = anime19;                   // Change pointer
#endif
#ifndef anime19
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime18;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/

#ifdef anime19
    case 19:
      /* Check if animation run or not. If not -> change pointer on following
       * animation
       */
      if( i_stat_cfg_anime.anim_run == 0)
      {
        // Now read last word and check for "read_end_of_anim" flag
        check_for_read_end_of_anim( &i_anime_repeat_counter );

        // Test if animation should be played once again or not
        if( i_anime_repeat_counter == 0 )
        {// If 0 -> Counted out -> play next animation
          // Clear "read_end_of_anim" flag
          i_stat_cfg_anime.read_end_of_anim=0;
#ifdef anime20
          p_anime = anime20;                   // Change pointer
#endif
#ifndef anime20
          // If next animation is not defined -> do not set "anim_run" flag
          i_stop_playing = 1;
#endif
          i_anime_scheduler_animation_id++;   // Increase ID to next animation
        }
        else
        {
          p_anime = anime19;
          i_anime_repeat_counter--;
        }
      }
      break;
#endif
      /*---------------------------------------------------------------------*/




    // Else we are out of animations -> run all again or just stop playing
    default:
      // Test if user want play animations in loop
      if( (i_stat_cfg_anime.anim_run == 0) &&
          (i_stat_cfg_anime.anim_play_in_loop != 0) )
      {// Play in loop
        // Set pointer to first animation
        p_anime = anime0;
        // And set ID to value 0
        i_anime_scheduler_animation_id = 0;
      }
      else
      {// Do not play in loop -> stop playing
        i_stop_playing = 1;
      }
      break;
    }


    // Set play flag if should play
    if( (i_stat_cfg_anime.anim_run == 0) &&
        (i_stop_playing == 0) )
    {
      i_stat_cfg_anime.anim_run=1;      // Play animation (set flag)
    }
  }//if( i_stat_cfg_anime.anim_read_FLASH != 0 )
}



//---------------------------------------------------------------------------//

/**
 * \brief Check if "read_end_of_anim" is not set. If true load last value in\n
 * flash.
 *
 * If flag is not set, then write last value (mask 0x00FF) from flash to\n
 * variable witch pointer points.
 *
 * @param p_anime_repeat_counter
 */
inline void check_for_read_end_of_anim(volatile uint8_t *p_anime_repeat_counter)
{
  if( i_stat_cfg_anime.read_end_of_anim == 0)
  {// Flag not set -> read value and save it and save the flag

    /* Load just value (Low 8 bits). Because pointer is always
     * incremented, for reading last value we must decrement pointer.
     */
    *p_anime_repeat_counter = (pgm_read_word( --p_anime )) & 0xFF;
    p_anime++;    // And set pointer to origin value
    i_stat_cfg_anime.read_end_of_anim = 1;
  }
}



//---------------------------------------------------------------------------//

/**
 * \brief Initialize Timer2 for interrupt, witch use animation function
 *
 * Set timer2 to CTC mode and set interrupt time to approximately 10 ms
 */
void init_animation(void)
{
  // normal pin mode ; CTC mode ; prescaller 1024 -> 64us / 1 cycle
  TCCR2 = (0<<COM21)|(0<<COM20)|\
          (1<<WGM21)|(0<<WGM20)|\
          (1<<CS22) |(0<<CS21) |(1<<CS20);

  // Fill OCR2 -> approx 10ms
  OCR2 = animation_delay_timer;

  // Enable interrupt if counter counts to value in OCR2
  TIMSK |= (1<<OCIE2);

  // Set pointer to first animation in flash memory
  p_anime = anime0;

  // And set status and configure variable for animation interrupt process
  i_stat_cfg_anime.anim_read_FLASH = 1;
  i_stat_cfg_anime.anim_run = 1;
  // If is set default playing animations in loop
#if default_play_animations_in_loop == 1
  i_stat_cfg_anime.anim_play_in_loop = 1;
#else
  i_stat_cfg_anime.anim_play_in_loop = 0;
#endif
}

//---------------------------------------------------------------------------//

/**
 * \brief Process loaded word to "i_tmp" and if needed save data to\n
 * framebuffer, or proceed special commands
 *
 *
 * @param i_tmp Loaded word (16 bit) from memory (Flash or SRAM)
 *
 * @param i_framebuffer_cnt Counter for framebuffer. It counts how many words\n
 * were written to framebuffer
 *
 * \param p_do_not_wait When there could be time delay, we set this variable
 */
void animation_process_word(uint16_t i_tmp,
                            uint8_t *p_framebuffer_cnt,
                            volatile uint8_t *p_do_not_wait)
{
  // In default is zero - only if needed then changed
  *p_do_not_wait = 0;

  // Pointer for character (needed for cmd_draw_character)
  const uint16_t *p_character = 0;

  // Temporary variable for one 2D frame
  uint16_t i_tmp_2D_frame[5];

  // Test for special functions -> first bit is set
  if( ( i_tmp & 0x8000 ) != 0 )
  {/* Special function -> i_tmp contains 2 parts: command number and value ->
    * -> let's split them -> easier for AVR to work with 8bit variables than
    * 16bit
    */
    uint8_t i_cmd_number = (i_tmp & 0x7F00)>>8;   // Save command number
    uint8_t i_cmd_value  = (i_tmp & 0x00FF);      // And save value


    switch( i_cmd_number )
    {
    // Command animation start
    case (cmd_anim_start>>8)& 0x7F :    // From 16 bit number makes cmd number
      anime_slowdown_factor = i_cmd_value;        // Save slowdown factor
      // Set PWM to max
      pwm_set_r(255);
      pwm_set_g(255);
      pwm_set_b(255);
      // Clear framebuffer
      framebuffer_wallX_void(0);
      framebuffer_wallX_void(1);
      framebuffer_wallX_void(2);
      framebuffer_wallX_void(3);
      framebuffer_wallX_void(4);
      break;

    // Command animation stop
    case (cmd_end_of_anim>>8)& 0x7F :
      // Set animation to "stop"
      i_stat_cfg_anime.anim_run = 0;     // Anime stop

      *p_framebuffer_cnt = 25;  /* And set counter to 25 -> stop loading data
                                 * to framebuffer.
                                 */
      *p_do_not_wait = 1;
      break;


    // Command Set PWM for RED channel
    case (cmd_set_pwm_r>>8)& 0x7F :
      pwm_set_r( i_cmd_value ); break;

    // Command Set PWM for GREEN channel
    case (cmd_set_pwm_g>>8)& 0x7F :
      pwm_set_g( i_cmd_value ); break;

    // Command Set PWM for BLUE channel
    case (cmd_set_pwm_b>>8)& 0x7F :
      pwm_set_b( i_cmd_value ); break;



    // Parameters for 2D frame (active wall)
    case (cmd_param_2D_frame>>8)& 0x7F :
      // Decide according option
      switch( i_cmd_value )
      {
      case param_2D_frame_void: // Void 2D frame
        if(*p_framebuffer_cnt < 5 )
        {
          // Power off all LED in WALL0
          framebuffer_wallX_void(0);
          // And set counter on following wall
          *p_framebuffer_cnt = 5;
        }
        else if(*p_framebuffer_cnt < 10)
        {
          // Power off all LED in WALL1
          framebuffer_wallX_void(1);
          // And set counter on following wall
          *p_framebuffer_cnt = 10;
        }
        else if(*p_framebuffer_cnt < 15)
        {
          // Power off all LED in WALL2
          framebuffer_wallX_void(2);
          // And set counter on following wall
          *p_framebuffer_cnt = 15;
        }
        else if(*p_framebuffer_cnt < 20)
        {
          // Power off all LED in WALL3
          framebuffer_wallX_void(3);
          // And set counter on following wall
          *p_framebuffer_cnt = 20;
        }
        else
        {
          // Power off all LED in WALL4
          framebuffer_wallX_void(4);
          // And set counter on following wall
          *p_framebuffer_cnt = 25;
        }
        break;  // param_2D_frame_void
      case param_2D_frame_same: // Same 2D frame as previous
        // Just copy data and move on
        if(*p_framebuffer_cnt < 5)
        {// Error - WALL0 has no reference -> clear all data
          framebuffer_wallX_void(0);
          // And set counter on following wall
          *p_framebuffer_cnt = 5;
        }
        else if(*p_framebuffer_cnt < 10)
        {// Active WALL1 -> copy from WALL0
          framebuffer_copy_wallX_to_wallY(0,1);
          // And set counter on following wall
          *p_framebuffer_cnt = 10;
        }
        else if(*p_framebuffer_cnt < 15)
        {// Active WALL2 -> copy from WALL1
          framebuffer_copy_wallX_to_wallY(1,2);
          // And set counter on following wall
          *p_framebuffer_cnt = 15;
        }
        else if(*p_framebuffer_cnt < 20 )
        {// Active WALL3 -> copy from WALL2
          framebuffer_copy_wallX_to_wallY(2,3);
          // And set counter on following wall
          *p_framebuffer_cnt = 20;
        }
        else
        {// Active WALL4 -> copy from WALL3
          framebuffer_copy_wallX_to_wallY(3,4);
          // And set counter on following wall
          *p_framebuffer_cnt = 25;
        }
        break;  // param_2D_frame_same
      case param_2D_frame_ones:        // Whole 2D frame is on
      if(*p_framebuffer_cnt < 5 )
      {
        // Turn on all LEDs in WALL0
        framebuffer_wallX_ones(0);
        // And set counter on following wall
        *p_framebuffer_cnt = 5;
      }
      else if(*p_framebuffer_cnt < 10)
      {
        // Turn on all LEDs in WALL1
        framebuffer_wallX_ones(1);
        // And set counter on following wall
        *p_framebuffer_cnt = 10;
      }
      else if(*p_framebuffer_cnt < 15)
      {
        // Turn on all LEDs in WALL2
        framebuffer_wallX_ones(2);
        // And set counter on following wall
        *p_framebuffer_cnt = 15;
      }
      else if(*p_framebuffer_cnt < 20)
      {
        // Turn on all LEDs in WALL3
        framebuffer_wallX_ones(3);
        // And set counter on following wall
        *p_framebuffer_cnt = 20;
      }
      else
      {
        // Turn on all LEDs in WALL4
        framebuffer_wallX_ones(4);
        // And set counter on following wall
        *p_framebuffer_cnt = 25;
      }
        break;  // param_2D_frame_ones
      }
    break;// For case: cmd_param_2D_frame



    // Parameter for 3D frame
    case (cmd_param_3D_frame>>8)& 0x7F :
    // Ok, there will be some options
      switch(i_cmd_value)
      {
      case param_3D_frame_void: // Set all segments to 0
        framebuffer_wallX_void(0);
        framebuffer_wallX_void(1);
        framebuffer_wallX_void(2);
        framebuffer_wallX_void(3);
        framebuffer_wallX_void(4);
        break;  // param_3D_frame_void

      case param_3D_frame_same: /* Framebuffer same -> do not change
                                 * framebuffer -> just change p_framebuffer_cnt
                                 */
        *p_framebuffer_cnt = 25;
        break;    // param_3D_frame_same
      case param_3D_frame_ones: // Turn on all LEDs
        framebuffer_wallX_ones(0);
        framebuffer_wallX_ones(1);
        framebuffer_wallX_ones(2);
        framebuffer_wallX_ones(3);
        framebuffer_wallX_ones(4);
        break;    // param_3D_frame_ones
      }
    break;      // cmd_param_3D_frame



    // Draw character
    case (cmd_draw_character>>8)& 0x7F :
      // Now figure out, witch character program should load
      switch(i_cmd_value)
      {
      case '0': p_character = font_5x5_0; break;
      case '1': p_character = font_5x5_1; break;
      case '2': p_character = font_5x5_2; break;
      case '3': p_character = font_5x5_3; break;
      case '4': p_character = font_5x5_4; break;
      case '5': p_character = font_5x5_5; break;
      case '6': p_character = font_5x5_6; break;
      case '7': p_character = font_5x5_7; break;
      case '8': p_character = font_5x5_8; break;
      case '9': p_character = font_5x5_9; break;
      case 'A': p_character = font_5x5_A; break;
      case 'B': p_character = font_5x5_B; break;
      case 'C': p_character = font_5x5_C; break;
      case 'D': p_character = font_5x5_D; break;
      case 'E': p_character = font_5x5_E; break;
      case 'F': p_character = font_5x5_F; break;
      case 'G': p_character = font_5x5_G; break;
      case 'H': p_character = font_5x5_H; break;
      case 'I': p_character = font_5x5_I; break;
      case 'J': p_character = font_5x5_J; break;
      case 'K': p_character = font_5x5_K; break;
      case 'L': p_character = font_5x5_L; break;
      case 'M': p_character = font_5x5_M; break;
      case 'N': p_character = font_5x5_N; break;
      case 'O': p_character = font_5x5_O; break;
      case 'P': p_character = font_5x5_P; break;
      case 'Q': p_character = font_5x5_Q; break;
      case 'R': p_character = font_5x5_R; break;
      case 'S': p_character = font_5x5_S; break;
      case 'T': p_character = font_5x5_T; break;
      case 'U': p_character = font_5x5_U; break;
      case 'V': p_character = font_5x5_V; break;
      case 'W': p_character = font_5x5_W; break;
      case 'X': p_character = font_5x5_X; break;
      case 'Y': p_character = font_5x5_Y; break;
      case 'Z': p_character = font_5x5_Z; break;
      case 'a': p_character = font_5x5_a; break;
      case 'b': p_character = font_5x5_b; break;
      case 'c': p_character = font_5x5_c; break;
      case 'd': p_character = font_5x5_d; break;
      case 'e': p_character = font_5x5_e; break;
      case 'f': p_character = font_5x5_f; break;
      case 'g': p_character = font_5x5_g; break;
      case 'h': p_character = font_5x5_h; break;
      case 'i': p_character = font_5x5_i; break;
      case 'j': p_character = font_5x5_j; break;
      case 'k': p_character = font_5x5_k; break;
      case 'l': p_character = font_5x5_l; break;
      case 'm': p_character = font_5x5_m; break;
      case 'n': p_character = font_5x5_n; break;
      case 'o': p_character = font_5x5_o; break;
      case 'p': p_character = font_5x5_p; break;
      case 'q': p_character = font_5x5_q; break;
      case 'r': p_character = font_5x5_r; break;
      case 's': p_character = font_5x5_s; break;
      case 't': p_character = font_5x5_t; break;
      case 'u': p_character = font_5x5_u; break;
      case 'v': p_character = font_5x5_v; break;
      case 'w': p_character = font_5x5_w; break;
      case 'x': p_character = font_5x5_x; break;
      case 'y': p_character = font_5x5_y; break;
      case 'z': p_character = font_5x5_z; break;

      // If character is not supported
      default : p_character = font_5x5_quest;
      }

    // Now load data to framebuffer to default position
    i_fb_data[0][0] = pgm_read_word(p_character++);
    i_fb_data[0][1] = pgm_read_word(p_character++);
    i_fb_data[0][2] = pgm_read_word(p_character++);
    i_fb_data[0][3] = pgm_read_word(p_character++);
    i_fb_data[0][4] = pgm_read_word(p_character++);

    break;      // cmd_draw_character


    // Framebuffer rotations
    case (cmd_rotate_fb>>8)& 0x7F :
      switch(i_cmd_value)
      {
      case param_rotate_fb_forward:
        framebuffer_backup_wallX(0);           // Backup front 2D frame
        framebuffer_copy_wallX_to_wallY(1,0);  // Copy data from WALL1 to WALL0
        framebuffer_copy_wallX_to_wallY(2,1);  // WALL2 -> WALL1
        framebuffer_copy_wallX_to_wallY(3,2);  // WALL3 -> WALL2
        framebuffer_copy_wallX_to_wallY(4,3);  // WALL4 -> WALL3
        framebuffer_restore_to_wallX(4);       // WALL0 (backup) -> WALL4
      break;    // param_rotate_fb_forward

      case param_rotate_fb_backward:
        framebuffer_backup_wallX(4);       // Backup back 2D frame
        framebuffer_copy_wallX_to_wallY(3,4);// WALL3 -> WALL4
        framebuffer_copy_wallX_to_wallY(2,3);// WALL2 -> WALL3
        framebuffer_copy_wallX_to_wallY(1,2);// WALL1 -> WALL2
        framebuffer_copy_wallX_to_wallY(0,1);// WALL0 -> WALL1
        framebuffer_restore_to_wallX(0);   // WALL4 (backup) -> WALL0
      break;

      case param_rotate_fb_left:
        framebuffer_backup_columnX(0);  // backup COLUMN0 (5x - 2D frame)
        framebuffer_copy_columnX_to_columnY(1,0);       // COL1 -> COL0
        framebuffer_copy_columnX_to_columnY(2,1);       // COL2 -> COL1
        framebuffer_copy_columnX_to_columnY(3,2);       // COL3 -> COL2
        framebuffer_copy_columnX_to_columnY(4,3);       // COL4 -> COL3
        framebuffer_restore_to_columnX(4);      // COL0 (backup) -> COL4
      break;

      case param_rotate_fb_right:
        framebuffer_backup_columnX(4);
        framebuffer_copy_columnX_to_columnY(3,4);
        framebuffer_copy_columnX_to_columnY(2,3);
        framebuffer_copy_columnX_to_columnY(1,2);
        framebuffer_copy_columnX_to_columnY(0,1);
        framebuffer_restore_to_columnX(0);
        break;
      case param_rotate_fb_up:
        framebuffer_rotate_up_wallX_collumnY(0,0);
        framebuffer_rotate_up_wallX_collumnY(0,1);
        framebuffer_rotate_up_wallX_collumnY(0,2);
        framebuffer_rotate_up_wallX_collumnY(0,3);
        framebuffer_rotate_up_wallX_collumnY(0,4);
        framebuffer_rotate_up_wallX_collumnY(1,0);
        framebuffer_rotate_up_wallX_collumnY(1,1);
        framebuffer_rotate_up_wallX_collumnY(1,2);
        framebuffer_rotate_up_wallX_collumnY(1,3);
        framebuffer_rotate_up_wallX_collumnY(1,4);
        framebuffer_rotate_up_wallX_collumnY(2,0);
        framebuffer_rotate_up_wallX_collumnY(2,1);
        framebuffer_rotate_up_wallX_collumnY(2,2);
        framebuffer_rotate_up_wallX_collumnY(2,3);
        framebuffer_rotate_up_wallX_collumnY(2,4);
        framebuffer_rotate_up_wallX_collumnY(3,0);
        framebuffer_rotate_up_wallX_collumnY(3,1);
        framebuffer_rotate_up_wallX_collumnY(3,2);
        framebuffer_rotate_up_wallX_collumnY(3,3);
        framebuffer_rotate_up_wallX_collumnY(3,4);
        framebuffer_rotate_up_wallX_collumnY(4,0);
        framebuffer_rotate_up_wallX_collumnY(4,1);
        framebuffer_rotate_up_wallX_collumnY(4,2);
        framebuffer_rotate_up_wallX_collumnY(4,3);
        framebuffer_rotate_up_wallX_collumnY(4,4);
        break;
      case param_rotate_fb_down:
        framebuffer_rotate_down_wallX_collumnY(0,0);
        framebuffer_rotate_down_wallX_collumnY(0,1);
        framebuffer_rotate_down_wallX_collumnY(0,2);
        framebuffer_rotate_down_wallX_collumnY(0,3);
        framebuffer_rotate_down_wallX_collumnY(0,4);
        framebuffer_rotate_down_wallX_collumnY(1,0);
        framebuffer_rotate_down_wallX_collumnY(1,1);
        framebuffer_rotate_down_wallX_collumnY(1,2);
        framebuffer_rotate_down_wallX_collumnY(1,3);
        framebuffer_rotate_down_wallX_collumnY(1,4);
        framebuffer_rotate_down_wallX_collumnY(2,0);
        framebuffer_rotate_down_wallX_collumnY(2,1);
        framebuffer_rotate_down_wallX_collumnY(2,2);
        framebuffer_rotate_down_wallX_collumnY(2,3);
        framebuffer_rotate_down_wallX_collumnY(2,4);
        framebuffer_rotate_down_wallX_collumnY(3,0);
        framebuffer_rotate_down_wallX_collumnY(3,1);
        framebuffer_rotate_down_wallX_collumnY(3,2);
        framebuffer_rotate_down_wallX_collumnY(3,3);
        framebuffer_rotate_down_wallX_collumnY(3,4);
        framebuffer_rotate_down_wallX_collumnY(4,0);
        framebuffer_rotate_down_wallX_collumnY(4,1);
        framebuffer_rotate_down_wallX_collumnY(4,2);
        framebuffer_rotate_down_wallX_collumnY(4,3);
        framebuffer_rotate_down_wallX_collumnY(4,4);
      }
    break;      // cmd_rotate_fb



    // cmd_end_of_3D_frame
    case (cmd_end_of_3D_frame>>8)& 0x7F :
      // "All data are already in framebuffer"
      *p_framebuffer_cnt = 25;
    break;      // cmd_end_of_3D_frame


    // cmd_rand_all_pwm
    case (cmd_rand_all_pwm>>8)& 0x7F :
      i_random_counter--;

      // Test if random flag is not set
      if( i_stat_cfg_anime.anim_random == 0 )
      {// If flag was not set -> set now
        // Set random flag
        i_stat_cfg_anime.anim_random = 1;
        // And load value to counter
        i_random_counter = (i_tmp & 0x00FF) ;
      }

      // Test if counter is equal to zero
      if( i_random_counter == 0 )
      {// then do not decrease pointer again (next will be read new command)
        // Clear random flag
        i_stat_cfg_anime.anim_random = 0;
      }
      else
      {// Else read this instruction again
        p_anime--;
      }

      // Get data from pointer and compare them in condition
      if( ((*p_random_addr) & 0x01 ) == 0 )
      {
        // Test if overflow can occur
        if( OCR3A >= (254-(rand_PWM_factor<<1)) )
        {// Overflow can occurs
          OCR3A = OCR3A - rand_PWM_factor;
        }
        else
        {// No overflow occurs
          OCR3A = OCR3A + rand_PWM_factor;
        }
      }
      else
      {
        // Again, test for overflow
        if( OCR3A <= (rand_PWM_factor<<2) )
        {// Overflow can occurs
          OCR3A = OCR3A + rand_PWM_factor;
        }
        else
        {// No overflow occurs
          OCR3A = OCR3A - rand_PWM_factor;
        }
      }

      // Load next random number
      move_random_pointer();
      if( ((*p_random_addr) & 0x02 ) == 0 )
      {
        // Test if overflow can occur
        if( OCR3B >= (254-(rand_PWM_factor<<1)) )
        {// Overflow can occurs
          OCR3B = OCR3B - rand_PWM_factor;
        }
        else
        {// No overflow occurs
          OCR3B = OCR3B + rand_PWM_factor;
        }
      }
      else
      {
        // Again, test for overflow
        if( OCR3B <= (rand_PWM_factor<<2) )
        {// Overflow can occurs
          OCR3B = OCR3B + rand_PWM_factor;
        }
        else
        {// No overflow occurs
          OCR3B = OCR3B - rand_PWM_factor;
        }
      }

      // Load next random number
      move_random_pointer();
      if( ((*p_random_addr) & 0x04 ) == 0 )
      {
        // Test if overflow can occur
        if( OCR3C >= (254-(rand_PWM_factor<<1)) )
        {// Overflow can occurs
          OCR3C = OCR3C - rand_PWM_factor;
        }
        else
        {// No overflow occurs
          OCR3C = OCR3C + rand_PWM_factor;
        }
      }
      else
      {
        // Again, test for overflow
        if( OCR3C <= (rand_PWM_factor<<2) )
        {// Overflow can occurs
          OCR3C = OCR3C + rand_PWM_factor;
        }
        else
        {// No overflow occurs
          OCR3C = OCR3C - rand_PWM_factor;
        }
      }

      // "All data are already in framebuffer"
      *p_framebuffer_cnt = 25;
    break;      // cmd_rand_all_pwm



    }






    /**
     * \todo Another special functions
     */
  }
  else
  {// Else ordinary data

    /* Depend on i_framebuffer_cnt store data on different position.
     * Used switch because of speed. Because framebuffer is 16 bit,
     * data must be written in "two cycles"
     */
    switch(*p_framebuffer_cnt)
    {
    case 0:  i_fb_data[0][0] = i_tmp; break;
    case 1:  i_fb_data[0][1] = i_tmp; break;
    case 2:  i_fb_data[0][2] = i_tmp; break;
    case 3:  i_fb_data[0][3] = i_tmp; break;
    case 4:  i_fb_data[0][4] = i_tmp; break;
    case 5:  i_fb_data[1][0] = i_tmp; break;
    case 6:  i_fb_data[1][1] = i_tmp; break;
    case 7:  i_fb_data[1][2] = i_tmp; break;
    case 8:  i_fb_data[1][3] = i_tmp; break;
    case 9:  i_fb_data[1][4] = i_tmp; break;
    case 10: i_fb_data[2][0] = i_tmp; break;
    case 11: i_fb_data[2][1] = i_tmp; break;
    case 12: i_fb_data[2][2] = i_tmp; break;
    case 13: i_fb_data[2][3] = i_tmp; break;
    case 14: i_fb_data[2][4] = i_tmp; break;
    case 15: i_fb_data[3][0] = i_tmp; break;
    case 16: i_fb_data[3][1] = i_tmp; break;
    case 17: i_fb_data[3][2] = i_tmp; break;
    case 18: i_fb_data[3][3] = i_tmp; break;
    case 19: i_fb_data[3][4] = i_tmp; break;
    case 20: i_fb_data[4][0] = i_tmp; break;
    case 21: i_fb_data[4][1] = i_tmp; break;
    case 22: i_fb_data[4][2] = i_tmp; break;
    case 23: i_fb_data[4][3] = i_tmp; break;
    case 24: i_fb_data[4][4] = i_tmp; break;
    }

    (*p_framebuffer_cnt)++;        // Increase counter by 1
  }

}

//---------------------------------------------------------------------------//

/**
 * \brief Increase random pointer by 1
 *
 * If overflow occurs, then pointer is set to start of user SRAM.\n
 * So far we calculate with AVR 8-bit, so pointer size is 16 bit.\n
 * If needed, just change data type.
 *
 */
inline void move_random_pointer(void)
{
  // Increase pointer
  p_random_addr++;
  // Test for overflow.
  // Explicitly say compiler, that we want work with numbers
  if( ((uint16_t)p_random_addr) < user_SRAM_begin)
  {
    p_random_addr = (uint16_t*)user_SRAM_begin;
  }
}
