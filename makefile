# target chip
device=atmega64

# -g - param for gcc -> add debug information
# optimizer settings for gcc
optimalize=Os

# fn - filename of "main" .c file
fn=rgb_led_cube_5x5x5

# dir_lib - user libraries folder
dir_lib=led_cube_lib

# dir_anim - user animations
dir_anim=animations

# Debug directory - for temporary compiled data.
dbg_dir=Debug

# avrdude command for upload program
avrdude_cmd=avrdude -c usbasp -U flash:w:$(fn).hex -p

# Message for "all ok"
ok_msg=echo "" ; echo "<<|| All_OK ||>>"

all: compile elf disasm hex mem_usage show_ok_msg

help:
	@echo "Parameters are:"
	@echo "		all - compile all"
	@echo "		program - compile all and program target chip"
	@echo "		clean - delete all files in \"$(dbg_dir)\" directory"
	@echo "Example: make program"
	@echo " This compile all project and program target chip"

show_ok_msg:
	@$(ok_msg)
	
compile:
	@echo "Compiling... this may take a second"
	@avr-gcc -Wall -$(optimalize) -std=c99 -mmcu=$(device) -c *.c \
	-I ./$(dir_lib)/ -I ./$(dir_anim)/ -I ./ \
	# Compile files in project root directory

	@avr-gcc -Wall -$(optimalize) -std=c99 -mmcu=$(device) -c \
	./$(dir_lib)/*.c -I ./$(dir_anim)/ -I ./ -I ./$(dir_lib) \
	# Compile files in directory dir_lib
	
	@avr-gcc -Wall -$(optimalize) -std=c99 -mmcu=$(device) -c \
	./$(dir_anim)/*.c -I ./$(dir_lib)/ -I ./ \
	# Compile animations

	@mv *.o $(dbg_dir)/ \
	# Move all *.o files to Debug directory

elf:
	@echo "Generating ELF file"
	@avr-gcc -Wall -mmcu=$(device) -o $(dbg_dir)/$(fn).elf $(dbg_dir)/*.o
	
disasm:
	@echo "Disassembling ELF"
	@avr-objdump -S $(dbg_dir)/$(fn).elf > $(dbg_dir)/$(fn).disasm

hex:
	@echo "Generating HEX file"
	@avr-objcopy -O ihex $(dbg_dir)/$(fn).elf $(dbg_dir)/$(fn).hex

	@cp $(dbg_dir)/$(fn).hex ./ \
	# And copy hex file

mem_usage:
	@avr-size $(dbg_dir)/$(fn).elf


clean: clean_remove_files show_ok_msg

clean_remove_files:
	@echo "Deleting files in Debug folder"
	@rm -rf Debug/*


# Compile source and upload program to target chip
program: compile elf disasm hex mem_usage test_4_chip show_ok_msg

test_4_chip:
# Test for ATmega64
ifeq ($(device),atmega64)
	@$(avrdude_cmd) m64
endif

#ifeq ($(device),atmega16)
#	@$(avrdude_cmd) m16
#endif


ifneq ($(device),atmega64)
#  ifneq ($(device),atmega16)
# if not found, then show this
	@echo "/----------------------------------------------------\\"
	@echo "| Target device not found in makefile database!      |"
	@echo "| Please upload program manually, or modify makefile |"
	@echo "\\----------------------------------------------------/"
	@false
#  endif
endif


