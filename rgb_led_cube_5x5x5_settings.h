/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Setting file for LED cube 5x5x5 project
 *
 * Feel free to modify this as far as you know/guess what are you doing
 *
 * Created:  2015/06/06\n
 * Modified: 2015/06/21
 *
 */

#ifndef _rgb_led_cube_5x5x5_settings
#define _rgb_led_cube_5x5x5_settings

/**
 * \defgroup animations Animations order
 * \brief Here user easy define order of animations
 *
 * User just type filename to include and #define anime#  bin_data_array\n
 * Example:\n
 *  #include "anim_snake.h"
 *  #define anime0 bin_anim_snake
 *
 *  #include "my_animation_collection.h"
 *  #define anime1 bin_anim_matrix
 *  #define anime2 some_crazy_data
 *  #define anime3 different_name_for_snake_animation
 *
 *  // And so on....
 *
 * \ingroup animations
 *
 * \{
 */
#include "anim_LED_cube_intro.h"
#define anime0          bin_anim_LED_cube_intro

#include "anim_snake.h"
#define anime1          bin_anim_snake

#include "anim_electron.h"
#define anime2          bin_anim_electron_slow
#define anime3          bin_anim_electron_fast
#define anime4          bin_anim_electron_end

#include "anim_arrow.h"
#define anime5          bin_anim_arrow

#include "anim_wave.h"
#define anime6          bin_anim_wave

#include "anim_rand_cube_full.h"
#define anime7          bin_anim_rand_cube_full

#include "anim_matrix.h"
#define anime8          bin_anim_matrix

#include "anim_rand_cube.h"
#define anime9          bin_anim_rand_cube

#include "anim_space_non_invaters.h"
#define anime10         bin_anim_space_non_invaters
/**
 * \}
 */

/**
 * \brief Define if in default animations will run in loop or just played once
 *
 * When is played last animation there are two options.\n
 * 1) Stop playing                      -> set disabled
 * 2) Play all animations again
 */
#define default_play_animations_in_loop         1

/**
 * \brief MCU speed
 *
 * Expected speed of oscillator for AVR.
 */
#ifndef F_CPU
#define F_CPU           16000000UL
#endif
/**
 * \brief Value for animation timer to get approximately 10ms delay
 *
 * Delay is calculated for prescaller in function init_animation.\n
 * For 16 MHz crystal and prescaller 1024 is this value 156.\n
 * 16 MHz -> 62.5 ns -> 62.5 ns * 1024 -> 64 us /1 timer cycle ->\n
 * -> 10 ms / 64us = 156.25 -> 156
 */
#define animation_delay_timer   156


/**
 * \brief Enable/Disable PWM support
 *
 * If project will be ported on different AVR, there can be problem that\n
 * three PWM channel will not be on same counter. If PWM is disabled, then\n
 * outputs PWM_R*, PWM_G* and PWM_B* will be set to 1. Anyway it is good idea\n
 * to change PCB (when is changed AVR), so it is easier connect "R_GND",\n
 * "G_GND" and "B_GND" direct to GND. Then any code with PWM can be removed.
 * \b Options: \b enabled, \b disabled
 */
#define pwm_support     1


/**
 * \}
 */

/**
 * \defgroup io_names I/O symbolic names
 *
 * \brief Symbolic port and pin names
 *
 * To every pin is there defined symbolic name. If this symbolic name is\n
 * changed here, then is changed by preprocessor "everywhere"
 * \{
 */

/**
 * \ingroup io_names
 * \defgroup data_red Data for red color
 * \defgroup data_green Data for green color
 * \defgroup data_blue Data for blue color
 * \defgroup wall Pins for select wall
 * \defgroup column Pins for select columns
 * \defgroup pwm_rgb Pins connected to hardware PWM on AVR chip
 *  Do not change unless You really know what are You doing!
 * \defgroup keyboard Pins connected to matrix keyboard
 * \defgroup uart UART pins
 * Do not change unless You really know what are You doing!
 */

/**
 * \ingroup data_red
 * \{
 */
#define D_R0_PORT       G
#define D_R0_pin        4

#define D_R1_PORT       G
#define D_R1_pin        3

#define D_R2_PORT       B
#define D_R2_pin        7

#define D_R3_PORT       B
#define D_R3_pin        6

#define D_R4_PORT       B
#define D_R4_pin        5
/**
 * \}
 * \ingroup data_green
 * \{
 */
#define D_G0_PORT       B
#define D_G0_pin        4

#define D_G1_PORT       B
#define D_G1_pin        3

#define D_G2_PORT       B
#define D_G2_pin        2

#define D_G3_PORT       B
#define D_G3_pin        1

#define D_G4_PORT       B
#define D_G4_pin        0
/**
 * \}
 * \ingroup data_blue
 * \{
 */
#define D_B0_PORT       E
#define D_B0_pin        7

#define D_B1_PORT       E
#define D_B1_pin        6

#define D_B2_PORT       A
#define D_B2_pin        5

#define D_B3_PORT       A
#define D_B3_pin        4

#define D_B4_PORT       A
#define D_B4_pin        3
/**
 * \}
 *
 * \ingroup wall
 * \{
 */
#define WALL0_PORT      A
#define WALL0_pin       6

#define WALL1_PORT      A
#define WALL1_pin       7

#define WALL2_PORT      C
#define WALL2_pin       7

#define WALL3_PORT      C
#define WALL3_pin       5

#define WALL4_PORT      C
#define WALL4_pin       3
/**
 * \}
 *
 * \ingroup column
 * \{
 */
#define COLUMN0_PORT    D
#define COLUMN0_pin     5

#define COLUMN1_PORT    D
#define COLUMN1_pin     7

#define COLUMN2_PORT    G
#define COLUMN2_pin     0

#define COLUMN3_PORT    G
#define COLUMN3_pin     1

#define COLUMN4_PORT    C
#define COLUMN4_pin     1
/**
 * \}
 *
 * \ingroup pwm_rgb
 * \{
 */
#define PWM_R_PORT      E
#define PWM_R_pin       3

#define PWM_G_PORT      E
#define PWM_G_pin       4

#define PWM_B_PORT      E
#define PWM_B_pin       5
/**
 * \}
 *
 * \ingroup uart
 * \{
 */
// NOTE: This is only for quick reference. Actually not used in project
#define RX_PC_PORT      D
#define RX_PC_pin       3

#define TX_PC_PORT      D
#define TX_PC_pin       2
/**
 * \}
 */


/**
 * \}
 */


/**
 * \brief PWM step for random change
 *
 * When changing randomly PWM value, this define basic step. Higher value\n
 * means faster change, but less continual. Value 3 is OK.
 */
#define rand_PWM_factor         3


/**
 * \brief Maximum read address when reading from memory in "random" mode
 *
 * This value should be same as size program itself.\n
 * You can see program size when uploading program to chip, or you can use\n
 * readlef command to figure it out. Or read it from HEX file. Choice is up\n
 * to yours.
 */
#define anim_random_max_address_flash   32000


/**
 * \brief Define begin of user SRAM
 *
 * Used mainly by random commands
 */
#define user_SRAM_begin         0x0100

#endif
